#!/usr/bin/env python
import sys                                                                      # needed to allow system commands
import re

print '\t Program starting...'


########################## Reading coordinate file #############################

if len( sys.argv ) > 1 :
    coord_file = sys.argv[1]                                                    # inputting box xvg file as an argument
else:
    print '\t Error(1): No file name entered, please enter a file name.'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\t Reading file "' + coord_file + '"...'

try :                                                                           # error handeling
    filin = open( coord_file )
except IOError :
    print '\t Error(2): Unable to open "' + coord_file + '" file, please check file name or file location.'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving content of file in variable
    filin.close()
    print '\t File "' + coord_file + '" read successfully.'

################################################################################


############################ Preparing output file #############################

filout = open( 'tilt_angles_fixed.xvg', 'w' )                                   # preparing output file
filout.write( '#header\n@header\n' )                                            # writing header

print '\t Preparing output file...'

################################################################################


########################## Parsing content of file #############################

j = 0
print '\t Skipping header...'

while ( '#' in lines[j] ) :                                                     # skipping header part 1
    j = j + 1
while ( '@' in lines[j] ) :                                                     # skipping header part 2
    j = j + 1

k = 0                                                                           # counter variable


print '\t Outputting the concatenated angle file...'

for i in range( j, len( lines ) ) :                                             # for each couple of lines line
    k = k + 1                                                                   # increment k
    split_line = lines[i].split()                                               # spliting the line into columns

    split_line[0] = complex( split_line[0] ) + 682000                           # adding 682000 to every time
    split_line[0] = str ( split_line[0] )                                       # converting to string
    split_line[0] = re.sub("[^0-9]", "", split_line[0])                         # removing non numerical characters
    split_line[0] = int ( split_line[0] )                                       # converting to int
    split_line[0] = split_line[0] / 10                                          # removing the extra zero

    # writing angle on output file
    filout.write( str( split_line[0] ) + '\t' + split_line[1] + '\t' + split_line[2] + '\t' + split_line[3] + '\n' )

filout.close()                                                                  # closing file

print '\t Output file "tilt_angles_fixed.xvg" written successfully.'
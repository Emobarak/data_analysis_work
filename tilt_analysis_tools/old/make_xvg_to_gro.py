#!/usr/bin/env python
import sys                                                                      # needed to allow system commands

print '\t Program starting...'


########################## Reading coordinate file #############################

if len ( sys.argv ) > 1 :
    coord_file = sys.argv[1]                                                    # inputting box xvg file as an argument
else:
    print '\t Error(1): No file name entered, please enter a file name.'
    sys.exit()                                                                  # program exit if no file name entered

print '\t Reading file "' + coord_file + '"...'

try :                                                                           # error handeling
    filin = open( coord_file )
except IOError :
    print '\t Error(2): Unable to open "' + coord_file + '" file, please check file name or file location.'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving content of file in variable
    filin.close()
    print '\t File "' + coord_file + '" read successfully.'

################################################################################

filout = open( 'com_coords.gro', 'w' )                                          # preparing output file
print '\t Preparing output file...'

########################## Parsing content of file #############################

j = 0
print '\t Skipping header...'

while ( '#' in lines[j] ) :                                                     # skipping header part 1
    j = j + 1
while ( '@' in lines[j] ) :                                                     # skipping header part 2
    j = j + 1

k = 0                                                                           # counter variable

print '\t Outputting center of mass coordinates...'
for i in range( j, len( lines ) ) :                                             # for each couple of lines line
    k = k + 1                                                                   # increment k

    split_line = lines[i].split()                                               # spliting the line into columns
    filout.write( 'Center of mass coordinates ' + str(k) + '\n6\n' )            # writing .gro file header
    filout.write( '    ' + '1COM      C    1 ' + split_line[1] + ' ' + split_line[2] + ' ' + split_line[3] + '\n' )               # writing center of mass coordinates 1
    filout.write( '    ' + '1COM      C    2 ' + split_line[4] + ' ' + split_line[5] + ' ' + split_line[6] + '\n' )               # writing center of mass coordinates 2
    filout.write( '    ' + '1COM      C    3 ' + split_line[7] + ' ' + split_line[8] + ' ' + split_line[9] + '\n' )               # writing center of mass coordinates 3
    filout.write( '    ' + '1COM      C    4 ' + split_line[10] + ' ' + split_line[11] + ' ' + split_line[12] + '\n' )               # writing center of mass coordinates 4
    filout.write( '    ' + '1COM      C    5 ' + split_line[13] + ' ' + split_line[14] + ' ' + split_line[15] + '\n' )               # writing center of mass coordinates 5
    filout.write( '    ' + '1COM      C    6 ' + split_line[16] + ' ' + split_line[17] + ' ' + split_line[18] + '\n' )               # writing center of mass coordinates 6
    filout.write( '3\t3\t8\n' )

filout.close()                                                                  # closing file

print '\t Output file com_coords.gro written successfully.'

################################################################################
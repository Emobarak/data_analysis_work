#!/usr/bin/env python
import sys                                                                      # needed to allow system commands
import math                                                                     # needed to enable math functions

print '\t Program starting...'


########################## Reading coordinate file #############################

if len( sys.argv ) > 1 :
    coord_file = sys.argv[1]                                                    # inputting box xvg file as an argument
else:
    print '\t Error(1): No file name entered, please enter a file name.'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\t Reading file "' + coord_file + '"...'

try :                                                                           # error handeling
    filin = open( coord_file )
except IOError :
    print '\t Error(2): Unable to open "' + coord_file + '" file, please check file name or file location.'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving content of file in variable
    filin.close()
    print '\t File "' + coord_file + '" read successfully.'

################################################################################


############################ Preparing output file #############################

filout = open( 'averaged_2d_tilt_angles.xvg', 'w' )                             # preparing output file
filout.write( '#header\n@header\n' )                                            # writing header

print '\t Preparing output file...'

################################################################################


########################## Parsing content of file #############################

j = 0
print '\t Skipping header...'

while ( '#' in lines[j] ) :                                                     # skipping header part 1
    j = j + 1
while ( '@' in lines[j] ) :                                                     # skipping header part 2
    j = j + 1

k = 0                                                                           # counter variable

print '\t Outputting the average between the xz and zy plane angles...'

for i in range( j, len( lines ) ) :                                             # for each couple of lines line
    k = k + 1                                                                   # increment k
    split_line = lines[i].split()                                               # spliting the line into columns

    split_line[1] = ( float( split_line[2] ) + float( split_line[3] ) ) / 2

    # writing angle on output file
    filout.write( split_line[0] + '\t' + str( split_line[1] ) +'\n' )

filout.close()                                                                  # closing file

print '\t Output file "averaged_2d_tilt_angles.xvg" written successfully.'
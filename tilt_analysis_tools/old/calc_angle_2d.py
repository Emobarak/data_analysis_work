#!/usr/bin/env python
import sys                                                                      # needed to allow system commands
import math                                                                     # needed to enable math functions

print '\t Program starting...'


########################## Reading coordinate file #############################

if len( sys.argv ) > 1 :
    coord_file = sys.argv[1]                                                    # inputting box xvg file as an argument
else:
    print '\t Error(1): No file name entered, please enter a file name.'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\t Reading file "' + coord_file + '"...'

try :                                                                           # error handeling
    filin = open( coord_file )
except IOError :
    print '\t Error(2): Unable to open "' + coord_file + '" file, please check file name or file location.'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving content of file in variable
    filin.close()
    print '\t File "' + coord_file + '" read successfully.'

################################################################################


############################# Defining functions ###############################

def dotproduct( v1, v2 ) :                                                      # declaring vector dot product function
    return sum( ( a * b ) for a, b in zip( v1, v2 ) )

def length( v ) :                                                               # declaring vector length function
    return math.sqrt( dotproduct( v, v ) )

def angle( v1, v2 ) :                                                           # declaring vector angle function
    return math.acos( dotproduct( v1, v2 ) / ( length( v1 ) * length( v2 ) ) )

def crossproduct ( v1, v2 ) :                                                   # declarning vector cross product function
    v3 = []                                                                     # declaring the resulting vector

    # calculating coords of resulting vector
    v3.append( ( v1[1] * v2[2] ) - ( v1[2] * v2[1] ) )
    v3.append( ( v1[2] * v2[0] ) - ( v1[0] * v2[2] ) )
    v3.append( ( v1[0] * v2[1] ) - ( v1[1] * v2[0] ) )

    return v3

def projection ( v, plane_normal ) :                                            # calculating the 2D projection on a plane
    proj = []                                                                   # declaring projection vector
    perp_comp = []                                                              # declaring perpendicular component vector

    # calculating coords of perpendicular component
    perp_comp.append( ( dotproduct( v, plane_normal ) / dotproduct( plane_normal, plane_normal ) ) * plane_normal[0] )
    perp_comp.append( ( dotproduct( v, plane_normal ) / dotproduct( plane_normal, plane_normal ) ) * plane_normal[1] )
    perp_comp.append( ( dotproduct( v, plane_normal ) / dotproduct( plane_normal, plane_normal ) ) * plane_normal[2] )

    # calculating coords of projection
    proj.append( v[0] - perp_comp[0] )
    proj.append( v[1] - perp_comp[1] )    
    proj.append( v[2] - perp_comp[2] )

    return proj

################################################################################


############################ Preparing output file #############################

filout = open( 'tilt_angles_2d.xvg', 'w' )                                      # preparing output file
filout.write( '#header\n@header\n' )                                            # writing header

print '\t Preparing output file...'

################################################################################


########################## Parsing content of file #############################

j = 0
print '\t Skipping header...'

while ( '#' in lines[j] ) :                                                     # skipping header part 1
    j = j + 1
while ( '@' in lines[j] ) :                                                     # skipping header part 2
    j = j + 1

k = 0                                                                           # counter variable

# calculating plane normal vectors
print '\t Calculating plane normal vectors...'

axis_x = [1, 0, 0]                                                              # declaring the x axis vector
axis_y = [0, 1, 0]                                                              # declaring the y axis vector
axis_z = [0, 0, 1]                                                              # declaring the z axis vector

xy_normal = crossproduct( axis_x, axis_y )                                      # calculating the normal vector of the xy plane
xz_normal = crossproduct( axis_x, axis_z )                                      # calculating the normal vector of the xz plane
yz_normal = crossproduct( axis_y, axis_z )                                      # calculating the normal vector of the yz plane

print '\t Outputting the angle between 2D projected vectors...'

for i in range( j, len( lines ) ) :                                             # for each couple of lines line
    k = k + 1                                                                   # increment k
    split_line = lines[i].split()                                               # spliting the line into columns

    # preparing the two vectors
    vector_1 = []                                                               # declare vector 1 array
    vector_1.append ( float( split_line[1] ) - float( split_line[4] ) )         # calculate x coord
    vector_1.append ( float( split_line[2] ) - float( split_line[5] ) )         # calculate y coord
    vector_1.append ( float( split_line[3] ) - float( split_line[6] ) )         # calculate z coord

    vector_2 = []                                                               # declare vector 2 array
    vector_2.append ( float( split_line[7] ) - float( split_line[10] ) )        # calculate x coord
    vector_2.append ( float( split_line[8] ) - float( split_line[11] ) )        # calculate y coord
    vector_2.append ( float( split_line[9] ) - float( split_line[12] ) )        # calculate z coord

    # projecting the vectors in a 2D plane
    # for plane xy
    proj1_xy = projection( vector_1, xy_normal )
    proj2_xy = projection( vector_2, xy_normal )

    # for plane xz
    proj1_xz = projection( vector_1, xz_normal )
    proj2_xz = projection( vector_2, xz_normal )

    # for plane yz
    proj1_yz = projection( vector_1, yz_normal )
    proj2_yz = projection( vector_2, yz_normal )

    # calculating the angle between the two 2D vectors
    # for plane xy
    angle_xy = angle ( proj1_xy, proj2_xy )
    angle_xy = ( angle_xy * 180 ) / math.pi                                     # convert from rad to degrees

    # for plane xz
    angle_xz = angle ( proj1_xz, proj2_xz )
    angle_xz = ( angle_xz * 180 ) / math.pi                                     # convert from rad to degrees

    # for plane xy
    angle_yz = angle ( proj1_yz, proj2_yz )
    angle_yz = ( angle_yz * 180 ) / math.pi                                     # convert from rad to degrees

    # writing angle on output file
    filout.write( split_line[0] + '\t' + str( angle_xy ) + '\t' + str( angle_xz ) + '\t' + str( angle_yz ) +'\n' )

filout.close()                                                                  # closing file

print '\t Output file "tilt_angles_2d.xvg" written successfully.'
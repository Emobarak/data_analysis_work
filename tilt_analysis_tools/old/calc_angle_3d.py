#!/usr/bin/env python
import sys                                                                      # needed to allow system commands
import math                                                                     # needed to enable math functions

print '\t Program starting...'


########################## Reading coordinate file #############################

if len( sys.argv ) > 1 :
    coord_file = sys.argv[1]                                                    # inputting box xvg file as an argument
else:
    print '\t Error(1): No file name entered, please enter a file name.'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\t Reading file "' + coord_file + '"...'

try :                                                                           # error handeling
    filin = open( coord_file )
except IOError :
    print '\t Error(2): Unable to open "' + coord_file + '" file, please check file name or file location.'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving content of file in variable
    filin.close()
    print '\t File "' + coord_file + '" read successfully.'

################################################################################

filout = open( 'tilt_angles.xvg', 'w' )                                         # preparing output file
filout.write( '#header\n@header\n' )                                            # writing header

print '\t Preparing output file...'

########################## Parsing content of file #############################

j = 0
print '\t Skipping header...'

while ( '#' in lines[j] ) :                                                     # skipping header part 1
    j = j + 1
while ( '@' in lines[j] ) :                                                     # skipping header part 2
    j = j + 1

k = 0                                                                           # counter variable

print '\t Outputting the angle between vectors...'

for i in range( j, len( lines ) ) :                                             # for each couple of lines line
    k = k + 1                                                                   # increment k
    split_line = lines[i].split()                                               # spliting the line into columns

    # preparing the two vectors
    vector_1 = []                                                               # declare vector 1 array
    vector_1.append ( float( split_line[1] ) - float( split_line[4] ) )         # calculate x coord
    vector_1.append ( float( split_line[2] ) - float( split_line[5] ) )         # calculate y coord
    vector_1.append ( float( split_line[3] ) - float( split_line[6] ) )         # calculate z coord

    vector_2 = []                                                               # declare vector 2 array
    vector_2.append ( float( split_line[7] ) - float( split_line[10] ) )        # calculate x coord
    vector_2.append ( float( split_line[8] ) - float( split_line[11] ) )        # calculate y coord
    vector_2.append ( float( split_line[9] ) - float( split_line[12] ) )        # calculate z coord

    # calculating the vector lengths
    v1_length = math.sqrt( vector_1[0] * vector_1[0] + vector_1[1] * vector_1[1] + vector_1[2] * vector_1[2] )
    v2_length = math.sqrt( vector_2[0] * vector_2[0] + vector_2[1] * vector_2[1] + vector_2[2] * vector_2[2] )

    # calculating the angle between the two vectors
    angle = math.acos( ( vector_1[0] * vector_2[0] + vector_1[1] * vector_2[1] + vector_1[2] * vector_2[2] ) / ( v1_length * v2_length ) )
    angle = ( angle * 180 ) / math.pi                                           # convert from rad to degrees

    filout.write( split_line[0] + '\t' + str( angle ) + '\n' )                  # writing angle on output file

filout.close()                                                                  # closing file

print '\t Output file "tilt_angles.xvg" written successfully.'

#def dotproduct(v1, v2):
#  return sum((a*b) for a, b in zip(v1, v2))

#def length(v):
#  return math.sqrt(dotproduct(v, v))

#def angle(v1, v2):
#  return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))
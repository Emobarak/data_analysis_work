#!/usr/bin/env python
import sys                                                                      # system library
import os                                                                       # bash commands library
import shutil                                                                   # more bash commands library

distribution_array_original = []                                                # array for all lipids file for original simulations
distribution_array_repeat_1 = []                                                # array for all lipids file for repeat 1 simulations
distribution_array_repeat_2 = []                                                # array for all lipids file for repeat 2 simulations
distribution_array_repeat_3 = []                                                # array for all lipids file for repeat 3 simulations
distribution_average_array = []                                                 # array for all lipids averaged

current_file_array = []                                                         # array for the current file


# original - all systems ########################################################
os.chdir('/wrk/projects/tlr4/analysis/tilt/original')

for i in range( 2, 5 ) :                                                        # for every original system
    current_file_array = []                                                     # reset current file array

    file_name = 'system_' + str(i) + '/bun_tilt.xvg'                          # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    distribution_array_original.append(current_file_array)                      # append the file array to the all_lipids array

# repeat 1 - all systems ########################################################
os.chdir('/wrk/projects/tlr4/analysis/tilt/repeat_1')

for i in range( 2, 5 ) :                                                        # for every repeat_1 system

    current_file_array = []                                                     # reset current file array

    file_name = 'system_' + str(i) + '/bun_tilt.xvg'                                                  # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    distribution_array_repeat_1.append(current_file_array)                      # append the file array to the all_lipids array

# repeat 2 - all systems ########################################################
os.chdir('/wrk/projects/tlr4/analysis/tilt/repeat_2')

for i in range( 2, 5 ) :                                                        # for every repeat_2 system

    current_file_array = []                                                     # reset current file array

    file_name = 'system_' + str(i) + '/bun_tilt.xvg'                                                  # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    distribution_array_repeat_2.append(current_file_array)                      # append the file array to the all_lipids array

# repeat 3 - all systems ########################################################
os.chdir('/wrk/projects/tlr4/analysis/tilt/repeat_3')

for i in range( 3, 5 ) :                                                        # for every repeat_3 system

    current_file_array = []                                                     # reset current file array

    file_name = 'system_' + str(i) + '/bun_tilt.xvg'                                                  # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    distribution_array_repeat_3.append(current_file_array)                      # append the file array to the all_lipids array

# average - all systems #########################################################

sys2_average_array = []                                                         # declare sys2 average array
sys3_average_array = []                                                         # declare sys3 average array
sys4_average_array = []                                                         # declare sys4 average array

print len(distribution_array_original[2])
print len(distribution_array_repeat_1[2])
print len(distribution_array_repeat_2)
print len(distribution_array_repeat_3)

for i in range( 0, len(distribution_array_original) ) :                         # for every file
    #print i
    for j in range( 0, len(distribution_array_repeat_1[2]) ) :                  # for every line of every file
#        print j

        if i == 0 :                                                             # if we are at the 2nd system
            average_0 = (float(distribution_array_original[i][j][1]) + float(distribution_array_repeat_1[i][j][1]) + float(distribution_array_repeat_2[i][j][1])) / 3
            sys2_average_array.append([distribution_array_original[i][j][0], round(average_0, 4)])                      # save them in the current file array

        elif i == 1 :                                                           # if we are at the 3rd or 4th system
            average_0 = (float(distribution_array_repeat_1[i][j][1]) + float(distribution_array_repeat_2[i][j][1]) + float(distribution_array_repeat_3[i-1][j][1])) / 3
            sys3_average_array.append([distribution_array_original[i][j][0], round(average_0, 4)])                      # save them in the current file array

        elif i == 2 :                                                           # if we are at the 3rd or 4th system
            average_0 = (float(distribution_array_repeat_1[i][j][1]) + float(distribution_array_repeat_2[i][j][1]) + float(distribution_array_repeat_3[i-1][j][1])) / 3
            sys4_average_array.append([distribution_array_original[i][j][0], round(average_0, 4)])                      # save them in the current file array

#################################################################################

os.chdir('/wrk/projects/tlr4/analysis/tilt')

if os.path.exists( './averaged_new' ) :                                             # if the old directory exists
    shutil.rmtree( './averaged_new' )                                               # delete it
os.mkdir( './averaged_new' )                                                        # create it again

# output all lipids files #######################################################

file_name = '2_tilt_fixed_averaged.xvg'                                         # set the file name
filout = open( file_name, 'w' )                                                 # open the file

for j in range ( 0, len(sys2_average_array) ) :                                 # for every line of the file
    filout.write( '%4s' % sys2_average_array[j][0] )                            # write atom number
    filout.write( '%10s' % sys2_average_array[j][1] )                           # write averaged order parameter
    filout.write( '\n' )                                                        # jump line

filout.close()                                                                  # close the file
os.rename ( './' + file_name, './averaged/' + file_name )                       # move the file

#################################################################################

file_name = '3_tilt_fixed_averaged.xvg'                                         # set the file name
filout = open( file_name, 'w' )                                                 # open the file

for j in range ( 0, len(sys3_average_array) ) :                                 # for every line of the file
    filout.write( '%4s' % sys3_average_array[j][0] )                            # write atom number
    filout.write( '%10s' % sys3_average_array[j][1] )                           # write averaged order parameter
    filout.write( '\n' )                                                        # jump line

filout.close()                                                                  # close the file
os.rename ( './' + file_name, './averaged/' + file_name )                       # move the file

#################################################################################

file_name = '4_tilt_fixed_averaged.xvg'                                         # set the file name
filout = open( file_name, 'w' )                                                 # open the file

for j in range ( 0, len(sys4_average_array) ) :                                 # for every line of the file
    filout.write( '%4s' % sys4_average_array[j][0] )                            # write atom number
    filout.write( '%10s' % sys4_average_array[j][1] )                           # write averaged order parameter
    filout.write( '\n' )                                                        # jump line

filout.close()                                                                  # close the file
os.rename ( './' + file_name, './averaged_new/' + file_name )                       # move the file

#################################################################################
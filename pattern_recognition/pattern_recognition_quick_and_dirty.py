#!/usr/bin/env python3
import re

################################################################################
#                                    README                                    #
################################################################################
#                                                                              #
# "Quick and dirty" heuristic                                                  #
#  --------------------------                                                  #
# - Very quick method to find simple patterns in simple entries of numbers and #
#   letters that might be separated into sections by special characters        #
# - Only derives a quick and simple pattern from the first entry of the input  #
# - Does not compare mutliple entries with each other, so is not able to find  #
#   more precise patterns                                                      #
# - The idea is that this very quick and simple heuristic does a pretty decent #
#   job at finding patterns and identifying erroneous entries from simple data #
#   such as dates or order IDs                                                 #
#                                                                              #
#                                                                              #
# How to start the heuristic                                                   #
# --------------------------                                                   #
# - Call the function defined below with one of the input lists as argument    #
# - You can also make your own lists to challenge the heuristic                #
# - Either: type "python3 'file_name'.py" in the terminal                      #
# - OR: make the file executable, and type "./'file_name'.py" in the terminal  #
# - The program will return the derived pattern and the erroneous entries in   #
#   the terminal                                                               #
#                                                                              #
#                                                                              #
# Assumptions                                                                  #
# -----------                                                                  #
# 1) The information content is always presented in alphanumerical characters  #
# 2) Special characters are only used as section separators                    #
#       > you can have more than one separator character in a row ($#)         #
#       > do not use the "\" as a separators (I have not found a way to neuter #
#         it )                                                                 #
# 3) Correct information entries are always the same length                    #
# 4) The first entry (always used as a template) is never erroneous            #
#                                                                              #
# Therefore, an entry is considered erroneous if:                              #
# 1) It's longer/shorter (extra/missing characters)                            #
# 2) And/or has characters (letter, number or special characters) where they   #
#    should not below                                                          #
#                                                                              #
#                                                                              #
# What this heuristic can't find                                               #
# ------------------------------                                               #
# 1) Patterns of single digit / lower case / upper case characters             #
# 2) Precise character patterns (such as repeated [aa12])                      #
# 3) "Sections" that are not separated by special characters                   #
#                                                                              #
################################################################################



################################################################################
# Input examples                                                               #
################################################################################

#example 1 (no errors, no separator, numerical characters)
input_01 = ["201811",
            "201910",
            "199113",
            "200110",
            "201910"]

#example 2 (no errors, no separator, alphabetical characters)
input_02 = ["gdsfsad",
            "fnsaknd",
            "gsaefda",
            "jlmflmq",
            "sdanklq"]

#example 3 (no errors, no separator, alphanumerical characters)
input_03 = ["201aa11",
            "201bs10",
            "199as13",
            "200gr10",
            "201as10"]

#example 4 (no errors, 1 separator, numerical characters)
input_04 = ["2018-11",
            "2019-10",
            "1991-13",
            "2000-10",
            "2019-10"]

#example 5 (no errors, 1 separator, alphanumerical characters)
input_05 = ["A-101a3311",
            "A-12c11412",
            "A-123sa411",
            "A-1233b412",
            "A-123c1411"]

#example 6 (no errors, 2 same separators, alphanumerical characters)
input_06 = ["2c18-01-aa",
            "2a19-10-ab",
            "19a1-10-ad",
            "20v0-01-qa",
            "2019-10-ds"]

#example 7 (no errors, 2 different separators, alphanumerical characters)
input_07 = ["A-1012331/1",
            "A-1231141/2",
            "A-1231141/1",
            "A-1233441/2",
            "A-1231141/1"]

#example 8 (no errors, 4 different separators, alphanumerical characters)
input_08 = ["A-10123 31/1@4ds",
            "A-12311 41/2@fd2",
            "A-12311 41/1@as1",
            "A-12334 41/2@1g6",
            "A-12311 41/1@a56"]

#example 9 (4 errors, 8 different separators, alphanumerical characters)
input_09 = ["1-A-101.23 31 1-/1@4ds 21gh",
            "2-A-123.11 41 4-/2@fd2 21sd",
            "2-B-132.1 31 4-/2@fd2 2gsa",                                       # erroneous (missing char)
            "4-A-123.11 41 3-/1@as1 21f5",
            "A-1-132.11 23 2-/3@213 21fs",                                      # erroneous (wrong char at wrong place)
            "6-A-123.34 41 2-/2@1g6 dqs2",
            "3-2wead.21 31.2-/2@1w6 d13a",                                      # erroneous (wrong char at wrong place)
            "2-1-adf.21 31.5-/1@5rs 213sdags",                                  # erroneous (too long)
            "1-A-123.11 41 1-/1@a56 321s"]

#example 10 (experiment)
input_10 = ["13.05/{1988}/edm",
            "28.06/{1990}/ptm",
            "04.08/{1971}/???",                                                 # erroneous (missing data)
            "14.11/{1985}/crl",
            "--.04/{2004}/blt",                                                 # erroneous (missing data)
            "01.01/{1991}/mrg",
            "02.01/{19x1}/mcd",                                                 # erroneous (bad character)
            "23.03/{2010}/xrc",
            "04.07/{2002}/bla",
            "21.09/{1974}/ten"]

# example 11 (experiment)
input_11 = ["13.05/{1988}/edm",
            "28.06/{1990}/ptm",
            "04.08/{1971}/???",
            "14.11/{1985}/crl",
            "01.01/{1991}/mrg",
            "02.01/{19x1}/mcd",
            "23.03/{2010}/xrc",
            "04.07/{2002}/bla",
            "21.09/{1974}/ten",
            "13.12/{2005}/biz",
            "20.02/{1987}/put",
            "01.01/{1990}/abc",
            "02.02/{1989}/bvd",
            "03.11/{1988}/cde",
            "04.12/{1982}/drf",
            "05.10/{1981}/efg",
            "06.02/{1991}/fgh",
            "07.12/{1972}/ghi",
            "08.01/{1977}/hij",
            "09.11/{1997}/ijk",
            "10.05/{1978}/jkl",
            "11.06/{1990}/klm",
            "12.07/{1974}/lmn",
            "13.08/{1992}/mno",
            "14.08/{1970}/nop",
            "19.07/{2010}/ops",
            "15.07/{1971}/opq",
            "16.09/{1977}/pqf",
            "17.10/{1983}/qrs",
            "18.12/{1985}/rsq",
            "fhdiknfjajdilsmka",
            "20.12/{1986}/tua",
            "21.11/{1988}/uvc",
            "15.07/{1971}/opq",
            "22.08/{1994}/vwe",
            "23.07/{19_9}/wxe",
            "24.05/{1995}/xyg",
            "25.03/{1990}/yhz",
            "26.02/{1982}/z__",
            "27.12_{1985}/ddq",
            "28.10/{1981}/sdf",
            "09.11/{1997}/ijk",
            "10.05/{19x8}/jdl",
            "11.06/{1990}/klm",
            "12.07/{1974}/lmn",
            "13.08/{1992}/mno",
            "14.08/{1970}/nop",
            "19.07/{2010}/ops",
            "15.07/{1971}/opq",
            "16.09/{1977}/pqf",
            "17.10/{1983}/qrs",
            "18.12/{1985}/rsq",
            "19.12/{1990}/sty",
            "14.08/{1970}/nop",
            "19.07/{2010}/ops",
            "15.07/{1971}/o1q",
            "16.09/{1977}/pqf",
            "17.10/{1983}/qrs",
            "18.12/{1985}/rsq",
            "19.12/{1990}/sty",
            "20.12/{1986}/tua",
            "21.11/{1988}/uvc",
            "15.07/{1971}/opq",
            "22.08/{1994}/vwe",
            "23.07/{1989}/wxe",
            "24.05/{1995}/xyg",
            "25.03/{1990}/yhz",
            "aa.09/{1974}/ten",
            "13.12/{2005}/biz",
            "20.02/{1987}/put",
            "01.01/{1990}/abc",
            "02.02/{1989}/bvd",
            "03.11/{1988}/cde",
            "04.12/{1982}/drf",
            "05.10/{1981}/efg",
            "06.02/{1991}/fgh_",
            "07.12/{1972}/ghi",
            "08.01/{1977}/hij",
            "09.11/{1997}/ijk",
            "10.0v/{1978}/jkl",
            "11.06/{1990}/klm",
            "12.07/{1974}/lmn",
            "13.08/{1992}/mno",
            "14.08/{1970}/nop",
            "19.07/{2010}/ops",
            "15.07/{1971}/opq",
            "26.02/{1982}/zeq",
            "27.12/{1985}/ddq",
            "28.10/{1981}/sdf",
            "19.07/{2010}/ops",
            "15.07/{1971}/opq",
            "16.09/{1977}/pqf",
            "17.10/{1983}/qrs",
            "01.01/{1990}/abc",
            "02.02/{1989}/bvd",
            "03.11/{1988}/cde",
            "04.12/{1982}/drf",
            "22.08/{1994}/vwe",
            "23.07/{1989}/wxe",
            "24.05/{1995}/xyg"]

################################################################################


################################################################################
# quick_pattern function                                                       #
################################################################################

def quick_pattern( input_list ):                                                # define the quick_pattern function

    num_info = re.compile(r'^[0-9]+$')                                          # numerical characters compiler
    alpha_info = re.compile(r'^[a-zA-Z]+$')                                     # alphabetical characters compiler
    alphanum_info = re.compile(r'\w+')                                          # alphanumerical characters compiler
    sep = re.compile(r'\W+')                                                    # special characters (separator) compiler

    pattern_template = input_list[0]                                            # use first entry of input list as pattern template
    pat_tem_len = len(pattern_template)                                         # get length of pattern template

    print ("\t>> using entry 1: " + pattern_template + " as template")          # printing template

    find_pattern = (alphanum_info.findall(pattern_template))                    # check if there are special characters (separators) in the template
                                                                                # if the length of find_pattern is 1
    if len(find_pattern) == 1:                                                  # the template is not separated by special characters

        find_pattern = (num_info.findall(pattern_template))                     # check if it's only numerical characters
                                                                                # if the length of find_pattern is 1
        if len(find_pattern) == 1:                                              # the template is only numerical characters
            answer_check = "^\d{" + str(pat_tem_len) + "}$"                     # save the regular expression answer
            print ("\t>> found pattern: r'^\d{" + str(pat_tem_len) + "}$'")     # print the answer

                                                                                # if the length of find_pattern is 0
        elif len(find_pattern) == 0:                                            # the template is only alphabetical characters
            answer_check = "^[a-zA-Z]{" + str(pat_tem_len) + "}$"               # save the regular expression answer
            print ("\t>> found pattern: r'^[a-zA-Z]{" + str(pat_tem_len)
            + "}$'")                                                            # print the answer

        else:                                                                   # or else it's a mix of alphanumerical characters
            answer_check = "^[a-zA-Z0-9]{" + str(pat_tem_len) + "}$"            # save the regular expression answer
            print ("\t>> found pattern: r'^[a-zA-Z0-9]{" + str(pat_tem_len)
            + "}$'")                                                            # print the answer

        error_check = re.compile(answer_check)                                  # use the answer as a regular expression to check the rest of the entries

        k = 0                                                                   # error index

        for i in input_list:                                                    # for every line in the input
            k = k + 1                                                           # increment the error index
            if not error_check.match(i):                                        # if our new regular expression template does not match the entry
                print ("\t\t>>> entry " + str(k) + ": '" + i
                 + "' is erroneous")                                            # print the erroneous entry

                                                                                # else, the length of find_pattern is over 1
    else:                                                                       # so the template is separated by special characters

        subpattern_re = []                                                      # list to store the individual regular expression answer terms
        problematic_characters = ["*", "$", "^", "(", ")", "?", "[",
        "]", "{", "}", ".", "|", "+"]                                           # list to store the special characters that might confuse the regular expression
        separators = (sep.findall(pattern_template))                            # get the separators

        for i in range (0, len(separators)):                                    # for every separator
            for j in range (0, len(problematic_characters)):                    # for every problematic character
                if problematic_characters[j] in separators[i]:                  # if a problematic character is detected
                    separators[i] = separators[i].replace(
                        problematic_characters[j], "\\"
                        + problematic_characters[j])                            # fix it so it will not confuse the regular expression

        for i in find_pattern :                                                 # for every subtemplate
            find_subpattern = (num_info.findall(i))                             # check if it's only numerical characters

            if len(find_subpattern) == 1 and len(find_subpattern[0]) == len(i): # if the subtemplate is only numerical characters and is the same length as the whole pattern
                subpattern_re.append("\d{" + str(len(i)) + "}")                 # save the regular expression answer

            elif len(find_subpattern) == 0 :                                    # if the subtemplate is only alphabetical characters
                subpattern_re.append("[a-zA-Z]{" + str(len(i)) + "}")           # save the regular expression answer

            else :                                                              # if it's a mix of alphanumerical characters
                subpattern_re.append("[a-zA-Z0-9]{" + str(len(i)) + "}")        # save the regular expression answer

        answer = "r'^"                                                          # answer variable used to display the answer on the terminal
        answer_check = "^"                                                      # answer check variable used to check for errors
        j = 0                                                                   # separator incrementor

        if sep.match(pattern_template[0]):                                      # if the first character of the template is a special character
            answer = answer + str(separators[j])                                # add the first separator to the answer
            answer_check = answer_check + str(separators[j])                    # add the separator to the answer check
            j = j + 1                                                           # increment the separator variable

        for i in range (0, len(find_pattern)):                                  # for every separated element
            answer = answer + str(subpattern_re[i])                             # concatenate the answer to the answer variable
            answer_check = answer_check + str(subpattern_re[i])                 # concatenate the answer to the answer check variable

            if i < len(find_pattern) - 1:                                       # if we are not considering the last subtemplate element
                answer = answer + str(separators[j])                            # add the separator to the answer
                answer_check = answer_check + str(separators[j])                # add the separator to the answer check
                j = j + 1                                                       # increment the separator variable

        answer = answer + "$'"                                                  # cap off the answer
        answer_check = answer_check + "$"                                       # cap off the answer check

        print ("\t>> found pattern " + answer)                                  # print the answer

        error_check = re.compile(answer_check)                                  # compile the answer check as a regular expression

        k = 0                                                                   # error index

        for i in input_list:                                                    # for every line in the input
            k = k + 1                                                           # increment the error index
            if not error_check.match(i):                                        # if our new regular expression template does not match the entry
                print ("\t\t>>> entry " + str(k) + ": '" + i
                + "' is erroneous")                                             # print the erroneous entry

    return;                                                                     # close main


################################################################################
# program start                                                                #
################################################################################

def main():                                                                     # define main function
    print ("\n> program start")                                                 # verbose

    quick_pattern( input_11 )                                                   # call function with different input lists as argument

    print("> done\n")                                                           # print end of program

    return;                                                                     # close main

# call main function ###########################################################

if __name__ == "__main__":                                                      # call main
    main()

################################################################################
#                                     EOF!                                     #
################################################################################
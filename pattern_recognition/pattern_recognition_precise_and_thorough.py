#!/usr/bin/env python3
import sys
import re
import random

################################################################################
#                                    README                                    #
################################################################################
#                                                                              #
# "Thorough and precise" algorithm                                             #
#  -------------------------------                                             #
# - More complex method to find more precise patterns in simple entries of     #
#   numbers and letters that might be separated into sections by special       #
#   characters                                                                 #
# - Derives a more complex pattern by taking a set number of data entries as   #
#   template, which is decided by the user as an argument                      #
# - The idea is that by taking more data entries as templates, the heuristic   #
#   is able to derive a more and more precise pattern, and is better at seeing #
#   and ignoring the erroneous entries                                         #
# - For a quicker and more simple solution, I have also provided a second less #
#   complex algorithm that finds less in-depth patterns from the data entries  #
#                                                                              #
#                                                                              #
# How to start the algorithm                                                   #
# --------------------------                                                   #
# - Have a data list file prepared, with one data entry per line               #
# - Ideally, we want a list of 100 entries with 2 of them erroneous            #
# - An example file has been provided (data_list.txt)                          #
# - You can also make your own lists to challenge the heuristic                #
# - You will also need to enter a precision value, that needs to be between 1  #
#   and the number of data entries                                             #
# - The higher the precision, the better the derived regular expressions       #
# - Either:                                                                    #
#   Type "python3 'file_name'.py 'data_file.txt' [precision]" in the terminal  #
# - OR: make the file executable, and type                                     #
#   "./'file_name'.py" 'data_file.txt' [precision]"                            #
# - The program will return the derived pattern and the erroneous entries in   #
#   the terminal                                                               #
# - If you use the option "-h" when calling the program, you will call the     #
#   help function                                                              #
#                                                                              #
#                                                                              #
# Assumptions                                                                  #
# -----------                                                                  #
# 1) The information content is always presented in alphanumerical characters  #
# 2) Special characters are only used as section separators                    #
#       > you can have more than one separator character in a row ($#)         #
#       > do not use the "\" as a separators                                   #
# 3) Correct information entries are always the same length                    #
#                                                                              #
# Therefore, an entry is considered erroneous if:                              #
# 1) It's longer/shorter (extra/missing characters)                            #
# 2) And/or has characters (letter, number or special characters) where they   #
#    should not be                                                             #
#                                                                              #
#                                                                              #
# What this heuristic can't do                                                 #
# ----------------------------                                                 #
# It should be able to find everything, but it doesn't simplify repeated       #
# patterns such as [aabb]{2}, it will just find it as: aabbaabb                #
#                                                                              #
#                                                                              #
# The other solution I have provided is a quicker and more simple method that  #
# is able to find simple patterns, as it only uses the first data entry as     #
# a template                                                                   #
#                                                                              #
################################################################################


################################################################################
# defining functions                                                           #
################################################################################

def help():                                                                     # defining help function

    print ('\n\t *************************************************************')
    print ('\t ******** pattern_recognition_precise_and_thorough.py ********')
    print ('\t *************************************************************')
    print ('\n\t Help Menu:')
    print ('\t ----------')
    print ('\t > This program takes as input a file containing a list of data')
    print ('\t entries (1 data entry per line), and will look for patterns in')
    print ('\t the data in order to generate a regular expression that will be')
    print ('\t able to match each correct data entry, while also excluding the')
    print ('\t erroneous ones\n')
    print ('\t > The program will also require a [Precision] value: an integer')
    print ('\t between 1 and the max number of entries in the list\n')
    print ('\t > The higher the [Precision], the more random entries will be')
    print ('\t considered in order to generate the regular expression\n')
    print ('\t > The data file provided has 100 entries, 2 of them erroneous')
    print ('\t (2% errors) which is why the minimum recommended [Precision]')
    print ('\t entered should be at least 5 (more than twice the number of')
    print ('\t erroneous entries), as a lower precision will give variable')
    print ('\t results depending on how lucky you get with the templates\n')
    print ('\t > The program starts by taking [Precision] random data entries')
    print ('\t and will generate a regular expression more precise than the')
    print ('\t one generated by the other provided solution\n')
    print ('\t > This more precise regular expression will then be applied to')
    print ('\t the entire list as a check to see how many data entries it can')
    print ('\t match, in which case the more the better\n')
    print ('\t > To use the program, either type:')
    print ('\t\t "python3 file_name.py data_file.txt [Precision]"')
    print ('\t > Or make the file executable, and instead type:')
    print ('\t\t "./file_name.py data_file.txt [Precision]"')
    print ('\t ----------\n')
    print ('\t Heuristic written by Edouard Mobarak')
    print ('\t Version 1.0')
    print ('\n\t *************************************************************')
    print ('\t ************* Thank you for using this program! *************')
    print ('\t *************************************************************\n')

    sys.exit(1)                                                                 # program exits

################################################################################

def refine_pattern(random_templates):                                           # define the refine_pattern function that will refine the previous quick pattern

    sep = re.compile(r'\W')                                                     # special characters (separator) compiler
    num_info = re.compile(r'^[0-9]+$')                                          # numerical characters compiler
    alpha_info = re.compile(r'^[a-zA-Z]+$')                                     # alphabetical characters compiler
    problematic_characters = ["*", "$", "^", "(", ")", "?", "[",
    "]", "{", "}", ".", "|", "+"]                                               # array to store the special characters that might confuse the regular expression
    char_compare_list = [[] for _ in range(len(random_templates[0]))]           # character comparison list
    answer = "^"                                                                # prepare the answer variable
    ans_nomatch = ''                                                            # prepare the ans_nomatch variable


    for i in range(0, len(char_compare_list)):                                  # fill up the character comparison list
        for j in range (0, len(random_templates)):
            char_compare_list[i].append(random_templates[j][i])                 # this will insert every character from our template in a node

    for i in range(0, len(char_compare_list)):                                  # for every character in the template character list
        for j in range(0, len(char_compare_list[i])):
            if char_compare_list[i][j] in problematic_characters:               # if we find a problematic character
                char_compare_list[i][j] = "\\" + char_compare_list[i][j]        # fix it

    for i in range (0, len(char_compare_list)):                                 # for every character in the character compare list

        if (len(set(char_compare_list[i])) == 1):                               # if all the character in the sublist are the same

            if len(ans_nomatch) == 0:                                           # if ans_nomatch is empty
                answer = answer + char_compare_list[i][0]                       # add the character to the answer

            else:                                                               # if ans_nomatch is not empty

                if num_info.match(ans_nomatch):                                 # if only numerical characters are in ans_nomatch
                    answer = answer + "\d{" + str(len(ans_nomatch)) + "}"       # update the answer with the numerical chars
                    ans_nomatch = ''                                            # flush ans_nomatch

                elif alpha_info.match(ans_nomatch):                             # if only alphabetical characters are in ans_nomatch
                    answer = answer + "[a-zA-Z]{" + str(len(ans_nomatch)) + "}" # update the answer with the alphabetical chars
                    ans_nomatch = ''                                            # flush ans_nomatch

                else:                                                           # if alphanumerical characters are in ans_nomatch
                    answer = answer + "[a-zA-Z0-9]{" +str(len(ans_nomatch))+"}" # update the answer with the alphanumerical chars
                    ans_nomatch = ''                                            # flush ans_nomatch

                answer = answer + char_compare_list[i][0]                       # add the character to the answer

        else:                                                                   # if all the characters in the sublist are not the same
            correct_char = max(char_compare_list[i],
            key=char_compare_list[i].count)                                     # count the most common character

            if sep.match(correct_char):                                         # if the most common character is a separator

                if len(ans_nomatch) == 0:                                       # if ans_nomatch is empty
                    answer = answer + correct_char                              # add the separator to the answer

                else:                                                           # if ans_nomatch is not empty
                    if num_info.match(ans_nomatch):                             # if only numerical characters are in ans_nomatch
                        answer = answer + "\d{" + str(len(ans_nomatch)) + "}"   # update the answer with numerical chars
                        ans_nomatch = ''                                        # flush ans_nomatch

                    elif alpha_info.match(ans_nomatch):                         # if only alphabetical characters are in ans_nomatch
                        answer = answer + "[a-zA-Z]{"+str(len(ans_nomatch))+"}" # update the answer with alphabetical chars
                        ans_nomatch = ''                                        # flush ans_nomatch

                    else:                                                       # if alphanumerical characters are in ans_nomatch
                        answer=answer+"[a-zA-Z0-9]{"+str(len(ans_nomatch))+"}"  # update the answer with alphanumerical chars
                        ans_nomatch = ''                                        # flush ans_nomatch

                    answer = answer + correct_char                              # add the separator to the answer

            else:                                                               # if the most commmon character is not a separator
                num_char_list = []                                              # declare a numerical list
                alpha_char_list = []                                            # declare an alphabetical list

                for j in char_compare_list[i]:                                  # for every occurence of the character
                    if num_info.match(j):                                       # if j match a numerical character
                        num_char_list.append(j)                                 # save it to the numerical list
                    elif alpha_info.match(j):                                   # if j match an alphabetical character
                        alpha_char_list.append(j)                               # add it to the alphabetical list

                if len(num_char_list) >= len(alpha_char_list):                  # if there are more numbers than letters
                    ans_nomatch = ans_nomatch + num_char_list[0]                # save a number to ans_nomatch
                else:                                                           # or else
                    ans_nomatch = ans_nomatch + alpha_char_list[0]              # save a letter to ans_nomatch

    if len(ans_nomatch) > 0:                                                    # if at the end there are still saved characters
        if num_info.match(ans_nomatch):                                         # if only numerical characters are in ans_nomatch
            answer = answer + "\d{" + str(len(ans_nomatch)) + "}"               # update the answer
            ans_nomatch = ''                                                    # flush ans_nomatch

        elif alpha_info.match(ans_nomatch):                                     # if only alphabetical characters are in ans_nomatch
            answer = answer + "[a-zA-Z]{" + str(len(ans_nomatch)) + "}"         # update the answer
            ans_nomatch = ''                                                    # flush ans_nomatch

        else:                                                                   # if alphanumerical characters are in ans_nomatch
            answer = answer + "[a-zA-Z0-9]{" + str(len(ans_nomatch)) + "}"      # update the answer
            ans_nomatch = ''                                                    # flush ans_nomatch

    answer = answer + "$"                                                       # cap off the answer

    return answer                                                               # return the answer

################################################################################


################################################################################
# program start                                                                #
################################################################################

def main():                                                                     # define main function
    print ("\n> program start")                                                 # verbose


    # argument handeling #######################################################

    if len(sys.argv) > 1 :                                                      # if at least one file has been entered as argument

        for i in range(1 ,len(sys.argv)) :                                      # for every argument entered
            if sys.argv[i] == '-h' :                                            # if the '-h' flag is used anywhere
                help()                                                          # start the help section

        if len(sys.argv) < 3 :                                                  # if less than two arguments have been entered
            print ('\n> ERROR(2): Less than two argument have been entered, '
            + 'please enter a data file and a "Precision" value')
            print ('> use the "-h" flag to get more help.\n')                   # verbose
            sys.exit(1)                                                         # error = program exits

        if len(sys.argv) > 3 :                                                  # if more than two arguments have been entered
            print ('\n> ERROR(3): More than two argument have been entered, '
            + 'please enter a data file and a "Precision" value')
            print ('> use the "-h" flag to get more help\n')                    # verbose
            sys.exit(1)                                                         # error = program exits

    else :
        print ('\n> ERROR(1): No argument entered, please enter a data '        # if no file name entered
        + 'file and a "Precision" value')
        print ('> use the "-h" flag to get more help.\n')                       # verbose
        sys.exit(1)                                                             # error = program exit


    # open and read dataset file ###############################################

    print ('\t>> reading file "' + sys.argv[1] + '"...')                        # verbose

    try :                                                                       # error handeling
        filin = open(sys.argv[1])                                               # open the file entered as first argument
    except IOError :
        print ('\n> ERROR(4): unable to read file "'
        + sys.argv[1] + '", please check file name or file location\n')         # verbose
        sys.exit(1)                                                             # program exits if file is missing or the name is entered wrong
    else :
        lines = filin.readlines()                                               # saving every line of every file in the file array
        filin.close()                                                           # close the file
        print ('\t\t>>> file "' + sys.argv[1] + '" read successfully')          # verbose


    # find precise pattern #####################################################

    precision = int(sys.argv[2])                                                # save precision variable
    random_data_entries = []                                                    # list to save the random data entries chosen
    entry_len = []                                                              # list to save the length of data entries
    correct_data_entries = []                                                   # list to save the correct data entries
    matched = []                                                                # matched list
    not_matched = []                                                            # not matched list

    if precision == 0:                                                          # if the precision is 0
        print ('\n> ERROR(5): The "Precision" value needs to be higher'
        + ' than 0!')
        print ('> use the "-h" flag to get more help\n')                        # verbose
        sys.exit(1)                                                             # error = program exits

    if precision > len(lines):                                                  # if the precision is higher than the number of entries
        print ('\n> ERROR(6): The "Precision" value needs to be lower'
        + ' than the number of data entries (here: ' + str(len(lines)) + ')!')
        print ('> use the "-h" flag to get more help\n')                        # verbose
        sys.exit(1)                                                             # error = program exits

    for i in range(0, len(lines)):                                              # for ever data line read
        lines[i] = lines[i].strip('\n')                                         # strip the '\n' character

    print ("\t>> looking for precise patterns using "
        + str(precision) + " random data entries...")                           # verbose

    data_entries = lines.copy()                                                 # copy the data entry list

    for i in range (0, int(precision)):                                         # for every precision increment (the more precision, the more increments)
        random_entry = random.randint(0, len(data_entries)-1)                   # generate a random number
        random_data_entries.append(data_entries[random_entry])                  # use a random entry of input list as pattern template
        data_entries.remove(data_entries[random_entry])                         # remove the chosen entry from the list

    print ("\t\t>> finding template entries that are too short or too long...") # verbose

    for i in random_data_entries:                                               # for every random data entry
        entry_len.append(len([i][0]))                                           # count their length

    correct_len = max(entry_len, key=entry_len.count)                           # calculate the most common length

    for i in random_data_entries:                                               # for every random data entry
        if len([i][0]) == correct_len:                                          # if the length is correct
            correct_data_entries.append(i)                                      # append the data to the correct list

    eliminated = len(random_data_entries) - len(correct_data_entries)           # calculates the number of incorrect entries removed already
    print ("\t\t\t>>> " + str(eliminated) + " incorrect entry(ies) eliminated") # verbose

    final_pattern = refine_pattern(correct_data_entries)                        # call the refine_pattern function
    final_re_test = re.compile(final_pattern)                                   # compile the final template
    print ("\t\t>>> found precise pattern: " + final_pattern)                   # verbose


    # test precise pattern #####################################################

    print ("\t>> testing pattern on data...")                                   # verbose

    for i in lines:                                                             # try it on every line of the input
        if final_re_test.match(i):                                              # if it matches
            matched.append(i)                                                   # save the matched
        else:                                                                   # if not
            not_matched.append(i)                                               # save the not matched

    print ("\t\t>>> with a precision of [" + str(precision) + "] found "
    + str(len(matched)) + " matches")                                           # verbose
    print ("\t\t>>> excluded " + str(len(not_matched)) + " entries")            # verbose

    for i in not_matched:
        print ("\t\t\t>>>> " + i)                                               # verbose

    print("> done\n")                                                           # verbose

    return;                                                                     # close main function


# call main function ###########################################################

if __name__ == "__main__":                                                      # call main
    main()

################################################################################
#                                     EOF!                                     #
################################################################################
#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library
import collections                                                              # sorted dictionary library

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\t ************************************************************************'
    print '\t ***************************** Help section *****************************'
    print '\t ************************************************************************\n'
    print '\t > This program takes 1 Manually sorted .gro file and 1 Automatically'
    print '\t sorted .gro file (in this order) and outputs a reordered Man .gro file'
    print '\t which has the Auto .gro file order, but keeps the Man atom names.\n'
    print '\t > The goal of the program is to check which Atoms are missing / added in'
    print '\t the Man .gro file (compared to the Auto .gro file), and to later be able'
    print '\t to compare their respective topology .top files once they have been'
    print '\t generated at a later step.\n'
    print '\t > Example line:\n'
    print '\t\t ./reorder_gro_coord.py [man_file.gro] [auto_file.gro] > log.txt'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


############################### Starting program ###############################

print '\n\t ************************************************************************'
print '\t ************************** reorder_gro.py v1.0 *************************'
print '\t ************************************************************************'
print '\n\t This program was written by Edouard Mobarak:'
print '\t - Doctoral Student at the University of Helsinki'
print '\t - Biological Physics and Soft Matter group'
print '\t - Supervised by Dr. Tomasz Rog and Prof. Ilpo Vattulainen'
print '\n\t Version v1.0 - 22/03/2018'
print '\t Requires Python v2.7\n'
print '\t ************************************************************************\n'

################################################################################


################################# Reading files ################################

if len( sys.argv ) > 1 :                                                        # if at least one file has been entered as argument

    arg_arr = []                                                                # create an array that stores the arguments

    for i in range( 1 ,len( sys.argv ) ) :                                      # preliminary check: for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()                                                          # start the help section

    if len( sys.argv ) < 3 :                                                    # if not enough files have been entered
        print '\n ERROR(2): Less than 2 files has been entered, please enter a man_ordered .gro file and an auto_ordered .gro file.'
        print ' > Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if len( sys.argv ) > 3 :                                                    # if not enough files have been entered
        print '\n ERROR(3): More than 2 files has been entered, please enter a man_ordered .gro file and an auto_ordered .gro file.'
        print ' > Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if not sys.argv[1].endswith('.gro') :                                       # checks if first file entered is a .gro file
        print '\n ERROR(4): First file entered "' + sys.argv[1] + '" is not a .gro file, please enter a man_ordered .gro file and an auto_ordered .gro file.'
        print ' > Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a gro file
    else :
        arg_arr.append( sys.argv[1] )                                           # if it is, append it to the argument array

    if not sys.argv[2].endswith('.gro') :                                       # checks if second file entered is a .gro file
        print '\n ERROR(5): Second file entered "' + sys.argv[2] + '" is not a .gro file, please enter a man_ordered .gro file and an auto_ordered .gro file.'
        print ' > Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the second file is not a gro file
    else :
        arg_arr.append( sys.argv[2] )                                           # if it is, append it to the argument array

else :
    print '\n ERROR(1): No file name entered, please enter a man_ordered .gro file and an auto_ordered .gro file.'
    print ' > Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\n\t Starting program...'
print '\t ' + str( len( sys.argv )-1 ) + ' files entered...\n'

file_array = []                                                                 # create an array that stores all lines of all files

for i in range( 0 ,len( sys.argv )-1 ) :
    try :                                                                       # error handeling
        filin = open( arg_arr[i] )
    except IOError :
        print '\n ERROR(6): Unable to open file "' + arg_arr[i] + '", please check file name or file location.'
        print ' > Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if file is missing or the name is entered wrong
    else :
        print '\t Reading file "' + arg_arr[i] + '"...'
        lines = filin.readlines()                                               # saving every line of every file in the file array
        file_array.append( lines )
        filin.close()
        print '\t\t File "' + arg_arr[i] + '" read successfully.'

################################################################################


########################## sorting data in dictionary ##########################

print '\n\t Saving file content in data structure...'

man_dict = {}                                                                   # dictionary for the man order .gro file
man_dict = collections.OrderedDict(man_dict)                                    # turn it into a sorted dictionary
residue = file_array[0][2].split()[0]                                           # current residue initialised at first file residue
residue_array = []                                                              # array for unique residues

for i in range ( 2 ,len(file_array[0])-1 ) :                                    # for the man order .gro file
    current_line = file_array[0][i].split()                                     # split the current line

    if ( residue != current_line[0] ) :                                         # if we are in a new residue
        man_dict[residue] = residue_array                                       # save the residue array in the dictionary
        residue = current_line[0]                                               # switch to the new residue
        residue_array = []                                                      # reset the residue array

    residue_array.append(file_array[0][i])                                      # append the line in the residue array
man_dict[residue] = residue_array                                               # save the residue array in the dictionary

auto_dict = {}                                                                  # dictionary for auto order .gro file
auto_dict = collections.OrderedDict(auto_dict)                                  # turn it into a sorted dictionary
residue = file_array[1][2].split()[0]                                           # current residue initialised at first file residue
residue_array = []                                                              # array for unique residues

for i in range ( 2 ,len(file_array[1])-1 ) :                                    # for the auto order .gro file
    current_line = file_array[1][i].split()                                     # split the current line

    if ( residue != current_line[0] ) :                                         # if we are in a new residue
        auto_dict[residue] = residue_array                                      # save the residue array in the dictionary
        residue = current_line[0]                                               # switch to the new residue
        residue_array = []                                                      # reset the residue array

    residue_array.append(file_array[1][i])                                      # append the line in the residue array
auto_dict[residue] = residue_array                                              # save the residue array in the dictionary

print '\t\t Done!\n'

################################################################################


########################### Parsing content of files ###########################

print '\n\t Processing file content...'
print '\t ==========================='

notes = 0                                                                       # setting the number of notes to 0
notes_arr = []                                                                  # array to save the residues who had notes
warnings = 0                                                                    # setting the number of warnings to 0
warning_arr = []                                                                # array to save the residues who had warnings
uneven_res_arr = []                                                             # declaring array for residues with a different number of atoms

filout = open( 'man_reordered.gro', 'w' )                                       # open output file

for key in man_dict.iteritems() :                                               # for every unique residue in the dictionary

    print "\n\t\t * For residue " + str(key[0]) + ":"

    man_res = man_dict[key[0]]                                                  # current residue from man order .gro file variable
    auto_res = auto_dict[key[0]]                                                # current residue from auto order .gro file variable
    is_uneven = 0                                                               # flag that indicates if the residue is uneven

    if len(man_res) != len(auto_res) :                                          # if residues in each file have a different length
        uneven_res_arr.append(man_res[0].split()[0])                            # save the residue name in the uneven array
        notes_arr.append(man_res[0].split()[0])                                 # save the residue name in the notes array
        is_uneven = 1                                                           # set the is_uneven flag to 1
        notes = notes+1                                                         # increment the number of notes
        print "\t\t\t NOTE(" + str(notes) + "): Uneven residue found!"
        print "\t\t\t\t\t   Length of man residue = " + str(len(man_res))
        print "\t\t\t\t\t   Length of auto residue = " + str(len(auto_res))

    for i in range (0, len(auto_res) ) :                                        # for every Auto atom
        auto_res_line = auto_res[i].split()                                     # split the line
        print "\t\t\t > For Auto atom " + str(auto_res_line[1]) + ":"
        for j in range (0, len(man_res) ) :                                     # for every man atom
            found = 0                                                           # set the found flag to 0
            x=0                                                                 # set the x flag to 0
            y=0                                                                 # set the y flag to 0
            z=0                                                                 # set the z flag to 0
            man_res_line = man_res[j].split()                                   #split the line
            print "\t\t\t\t Checking Man atom " + man_res_line[1]

            AutoX = float(auto_res_line[3])                                     # save the AutoX coord
            AutoY = float(auto_res_line[4])                                     # save the AutoY coord
            AutoZ = float(auto_res_line[5])                                     # save the AutoZ coord
            ManX = float(man_res_line[3])                                       # save the ManX coord
            ManY = float(man_res_line[4])                                       # save the ManY coord
            ManZ = float(man_res_line[5])                                       # save the ManZ coord

            cutoff = 0.08                                                       # set the first cutoff to 0.08

            if ( AutoX == ManX ) :                                              # if the xcoord of both Auto atom and Man atom are the same
                print "\t\t\t\t - Xcoord Auto = " + str(AutoX) + " OK with Xcoord Man = " + str(ManX)
                x=1                                                             # set x to 1
            if ( AutoY == ManY ) :                                              # if the ycoord of both Auto atom and Man atom are the same
                print "\t\t\t\t - Ycoord Auto = " + str(AutoY) + " OK with Ycoord Man = " + str(ManY)
                y=1                                                             # set y to 1
            if ( AutoZ == ManZ ) :                                              # if the zcoord of both Auto atom and Man atom are the same
                z=1                                                             # set z to 1
                print "\t\t\t\t - Zcoord Auto = " + str(AutoZ) + " OK with Zcoord Man = " + str(ManZ)

            if x+y+z == 3 :                                                     # if all 3 coords are the same
                filout.write(man_res[j])                                        # output the Man atom
                found = 1                                                       # set the found flag to 1
                man_res.remove(man_res[j])                                      # remove the current Man atom from the Man residue
                print "\t\t\t\t\t = Paired " + auto_res_line[1] + " with " + man_res_line[1]
                if man_res_line[1] in ["OXT", "HXT"] :                          # if the Man atom is a known terminal atom
                    notes=notes+1                                               # increment the notes variable
                    if man_res_line[0] not in notes_arr :                       # if the residue is already not saved
                        notes_arr.append(man_res_line[0])                       # save the residue name
                    print "\t\t\t = NOTE(" + str(notes) + "): Terminal atom " + man_res_line[0] + " found."
                break                                                           # leave the loop

            # if all 3 coords are NOT the same
            elif abs( AutoX - ManX ) <= cutoff :                                # check if X coord difference is within the cutoff
                print "\t\t\t\t\t Substraction = " + str(abs( AutoX - ManX ))
                print "\t\t\t\t - Xcoord Auto = " + str(AutoX) + " OK with Xcoord Man = " + str(ManX) + " after cutoff"
                if abs( AutoY - ManY ) <= cutoff :
                    print "\t\t\t\t\t Substraction = " + str(abs( AutoY - ManY )) # check if Y coord difference is within the cutoff
                    print "\t\t\t\t - Ycoord Auto = " + str(AutoY) + " OK with Ycoord Man = " + str(ManY) + " after cutoff"
                    if abs( AutoZ - ManZ ) <= cutoff :
                        print "\t\t\t\t\t Substraction = " + str(abs( AutoZ - ManZ )) # check if Z coord difference is within the cutoff
                        print "\t\t\t\t - Zcoord Auto = " + str(AutoZ) + " OK with Zcoord Man = " + str(ManZ) + " after cutoff"
                        filout.write(man_res[j])                                # if all 3 are within the cutoff, output the Man atom
                        found = 1                                               # set the found flag to 1
                        man_res.remove(man_res[j])                              # remove the current Man atom from the Man residue
                        print "\t\t\t\t\t = Paired " + auto_res_line[1] + " with " + man_res_line[1]
                        if man_res_line[1] in ["OXT", "HXT"] :                  # if the Man atom is a known terminal atom
                            notes=notes+1                                       # increment the notes variable
                            if man_res_line[0] not in notes_arr :               # if the residue is already not saved
                                notes_arr.append(man_res_line[0])               # save the residue name
                            print "\t\t\t = NOTE(" + str(notes) + "): Terminal atom " + man_res_line[1] + " found."
                        break                                                   # leave the loop

        if found == 0 :                                                         # if no Man atom corresponding to the Auto atom was found
            print "\t\t\t Residue " + auto_res_line[1] + " not found."
            print "\t\t\t Trying again with bigger cutoff."

            for j in range (0, len(man_res) ) :                                 # try again with the Man atoms that were NOT removed from the residue
                found = 0
                x=0
                y=0
                z=0
                man_res_line = man_res[j].split()
                print "\t\t\t\t Checking Man atom " + man_res_line[1]

                AutoX = float(auto_res_line[3])
                AutoY = float(auto_res_line[4])
                AutoZ = float(auto_res_line[5])
                ManX = float(man_res_line[3])
                ManY = float(man_res_line[4])
                ManZ = float(man_res_line[5])

                cutoff = 0.2                                                    # this time with a bigger cutoff

                if abs( AutoX - ManX ) <= cutoff :
                    print "\t\t\t\t\t Substraction = " + str(abs( AutoX - ManX ))
                    print "\t\t\t\t - Xcoord Auto = " + str(AutoX) + " OK with Xcoord Man = " + str(ManX) + " after cutoff"
                    if abs( AutoY - ManY ) <= cutoff :
                        print "\t\t\t\t\t Substraction = " + str(abs( AutoY - ManY ))
                        print "\t\t\t\t - Ycoord Auto = " + str(AutoY) + " OK with Ycoord Man = " + str(ManY) + " after cutoff"
                        if abs( AutoZ - ManZ ) <= cutoff :
                            print "\t\t\t\t\t Substraction = " + str(abs( AutoZ - ManZ ))
                            print "\t\t\t\t - Zcoord Auto = " + str(AutoZ) + " OK with Zcoord Man = " + str(ManZ) + " after cutoff"
                            filout.write(man_res[j])
                            found = 1
                            man_res.remove(man_res[j])
                            print "\t\t\t\t\t = Paired " + auto_res_line[1] + " with " + man_res_line[1]
                            if man_res_line[1] in ["OXT", "HXT"] :              # if the Man atom is a known terminal atom
                                notes=notes+1                                   # increment the notes variable
                                if man_res_line[0] not in notes_arr :           # if the residue is already not saved
                                    notes_arr.append(man_res_line[0])           # save the residue name
                                print "\t\t\t = NOTE(" + str(notes) + "): Terminal atom " + man_res_line[0] + " found."
                            break

        if found == 0 :                                                         # if a corresponding atom is still not found
            filout.write("CHECK: residue protonation states.\n")                # output a note
            notes = notes+1                                                     # increment the notes variable
            if man_res_line[0] not in notes_arr :                               # if the residue is already not saved
                notes_arr.append(man_res_line[0])                               # save the residue name
            print "\t\t\t = NOTE(" + str(notes) + "): Residue " + auto_res_line[1] + " not found in the Man file."

    if ( is_uneven == 1 ) :                                                     # if the current residues had an uneven number of atoms
            filout.write("CHECK: atom count.\n")                                # output a warning
            warnings = warnings+1                                               # increment the warnings variable
            if man_res_line[0] not in warning_arr :                             # if the residue is already not saved
                warning_arr.append(man_res_line[0].split()[0])                  # save the residue name
            print "\t\t\t WARNING(" + str(warnings) + "): Atom count uneven."

print "\n\t Finished with (" + str(notes) + ") NOTES"
if ( len(notes_arr) > 0 ) :                                                     # If NOTES appeared in the log
    print "\t\t Please check residues " + str(notes_arr) + " to see which atoms are problematic."

print "\n\t Finished with (" + str(warnings) + ") WARNINGS"
if ( warnings > 0 ) :                                                           # If WARNINGS appeared in the log
    print "\t\t Please check residues " + str(warning_arr) + " to see why the atom count is different."

print "\n\t Done!"

print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'

################################################################################












################################################################################

#ignh = 0                                                                        # set the ignore h option off
#excn = 0                                                                        # set the ignore exception names option off
#terat = 0                                                                       # set the terminal atom option off

#    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
#        is_option = sys.argv[i].split()                                         # split the argument
#        if is_option[0][0] == "-" :                                             # if the argument is an option/flag
#            if sys.argv[i] in ["-ignh", "-excn" , "-ter"] :                     # if the option exists
#                if sys.argv[i] == '-ignh' :                                     # if the '-ignh' flag is used
#                    print "\t * Ignoring hydrogen names."
#                    sys.argv.remove(sys.argv[i])                                # remove the ignh entry from the sys array to avoid errors
#                    ignh = 1                                                    # set the ignh flag to 1
#                    break                                                       # get out of the current loop
#            else :                                                              # if it does not exist
#                print '\n ERROR(7): Option ' + sys.argv[i] + ' not recognized, please use one of the available options.'
#                print ' > Use the "-h" flag to get more help.\n'
#                sys.exit(1)
#
#    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
#        is_option = sys.argv[i].split()                                         # split the argument
#        if is_option[0][0] == "-" :                                             # if the argument is an option/flag
#            if sys.argv[i] in ["-ignh", "-excn" , "-ter"] :                     # if the option exists
#                if sys.argv[i] == '-excn' :                                     # if the '-excn' flag is used
#                    print "\t * Ignoring exceptional atom names."
#                    sys.argv.remove(sys.argv[i])                                # remove the excn entry from the sys array to avoid errors
#                    excn = 1                                                    # set the excn flag to 1
#                    break                                                       # get out of the current loop
#            else :                                                              # if it does not exist
#                print '\n ERROR(7): Option ' + sys.argv[i] + ' not recognized, please use one of the available options.'
#                print ' > Use the "-h" flag to get more help.\n'
#                sys.exit(1)
#
#    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
#        is_option = sys.argv[i].split()                                         # split the argument
#        if is_option[0][0] == "-" :                                             # if the argument is an option/flag
#            if sys.argv[i] in ["-ignh", "-excn" , "-ter"] :                     # if the option exists
#                if sys.argv[i] == '-ter' :                                      # if the '-ter' flag is used
#                    print "\t * Taking into account terminal atoms."
#                    sys.argv.remove(sys.argv[i])                                # remove the ter entry from the sys array to avoid errors
#                    terat = 1                                                   # set the terat flag to 1
#                    break                                                       # get out of the current loop
#            else :                                                              # if it does not exist
#                print '\n ERROR(7): Option ' + sys.argv[i] + ' not recognized, please use one of the available options.'
#                print ' > Use the "-h" flag to get more help.\n'
#                sys.exit(1)


#                man_atom_no_int = ''.join([i for i in man_atom if not i.isdigit()]) # man atom name stripped of numbers
#                auto_atom_no_int = ''.join([i for i in auto_atom if not i.isdigit()]) # auto atom name stripped of numbers

#import numpy as np
#from decimal import Decimal, ROUND_DOWN
#import math


            #AutoX = round(float(auto_res_line[3]), 2)
            #AutoY = round(float(auto_res_line[4]), 2)
            #AutoZ = round(float(auto_res_line[5]), 2)
            #ManX = round(float(man_res_line[3]), 2)
            #ManY = round(float(man_res_line[4]), 2)
            #ManZ = round(float(man_res_line[5]), 2)

            #AutoX = math.floor(float(auto_res_line[3]) * 100)/100.0
            #AutoY = math.floor(float(auto_res_line[4]) * 100)/100.0
            #AutoZ = math.floor(float(auto_res_line[5]) * 100)/100.0
            #ManX = math.floor(float(man_res_line[3]) * 100)/100.0
            #ManY = math.floor(float(man_res_line[4]) * 100)/100.0
            #ManZ = math.floor(float(man_res_line[5]) * 100)/100.0

            #AutoX = "{0:.2f}".format(float(auto_res_line[3]))
            #AutoY = "{0:.2f}".format(float(auto_res_line[4]))
            #AutoZ = "{0:.2f}".format(float(auto_res_line[5]))
            #ManX = "{0:.2f}".format(float(man_res_line[3]))
            #ManY = "{0:.2f}".format(float(man_res_line[4]))
            #ManZ = "{0:.2f}".format(float(man_res_line[5]))

            #AutoX = truncate(Decimal(auto_res_line[3]), 2)
            #AutoY = truncate(Decimal(auto_res_line[4]), 2)
            #AutoZ = truncate(Decimal(auto_res_line[5]), 2)
            #ManX = truncate(Decimal(man_res_line[3]), 2)
            #ManY = truncate(Decimal(man_res_line[4]), 2)
            #ManZ = truncate(Decimal(man_res_line[5]), 2)

#def truncate(d, places):
#    """Truncate Decimal d to the given number of places.
#
#    >>> truncate_decimal(Decimal('1.234567'), 4)
#    Decimal('1.2345')
#    >>> truncate_decimal(Decimal('-0.999'), 1)
#    Decimal('-0.9')
#    """
#    return d.quantize(Decimal(10) ** -places, rounding=ROUND_DOWN)
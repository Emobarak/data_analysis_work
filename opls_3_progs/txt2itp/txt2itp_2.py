#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library
import shutil                                                                   # more bash commands library

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\n\t Help Menu:'
    print '\t ##########'
    print '\t This program takes as input a list.txt file filled with molecule'
    print '\t parameters and a complementary_information.itp file filled with'
    print '\t any other important parameter, and will output .itp files for'
    print '\t each described molecule, and as a single "ffnonbonded.itp" file'
    print '\t filld with non-bonded forcefield parameters, all for OPLS3.'
    print '\t ##########\n'
    print '\t Script written by Edouard Mobarak'
    print '\t Parameter conversion done by Fabio Lolicato.\n'
    print '\t Version 1.2'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


############################### Starting program ###############################

print '\n\t ************************************************************************'
print '\t ****************************** txt2itp.py ******************************'
print '\t ************************************************************************'

################################################################################


################################# Reading files ################################

# argument handeling ############################################################
if len( sys.argv ) > 1 :                                                        # if at least one file has been entered as argument

    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()                                                          # start the help section

    if len( sys.argv ) < 3 :                                                    # if less than two arguments have been entered
        print '\n ERROR(2): Less than two argument have been entered, please enter one molecule_list .txt file, and one additional info .itp file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if len( sys.argv ) > 3 :                                                    # if more than two arguments have been entered
        print '\n ERROR(3): More than two argument have been entered, please enter one molecule_list .txt file, and one additional info .itp file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)

    if not sys.argv[1].endswith('.txt') :                                       # checks if first argument entered is a .txt file
        print '\n ERROR(4): Argument ' + sys.argv[i] + ' is not a .txt file, please enter a text .txt file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a txt file

    if not sys.argv[2].endswith('.itp') :                                       # checks if second argument entered is an .itp file
        print '\n ERROR(5): Argument ' + sys.argv[i] + ' is not an .itp file, please enter a text .itp file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a txt file

else :
    print '\n ERROR(1): No argument entered, please enter a text .txt file and an additional info .itp file.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\n\t Starting program...'                                                # verbose

# open file 1 ###################################################################
try :                                                                           # error handeling
    filin = open( sys.argv[1] )
except IOError :
    print '\n ERROR(6): Unable to open "' + sys.argv[1] + '" file, please check file name or file location.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exits if file is missing or the name is entered wrong
else :
    print '\n\t Reading file "' + sys.argv[1] + '"...'                          # verbose
    lines = filin.readlines()                                                   # saving every line of every file in the file array
    filin.close()                                                               # close the file
    print '\t\t file "' + sys.argv[1] + '" read successfully.'                  # verbose

# open file 2 ###################################################################
try :                                                                           # error handeling
    filin2 = open( sys.argv[2] )
except IOError :
    print '\n ERROR(7): Unable to open "' + sys.argv[2] + '" file, please check file name or file location.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exits if file is missing or the name is entered wrong
else :
    print '\n\t Reading file "' + sys.argv[2] + '"...'                          # verbose
    lines2 = filin2.readlines()                                                 # saving every line of every file in the file array
    filin2.close()                                                              # close the file
    print '\t\t file "' + sys.argv[2] + '" read successfully.'                  # verbose

################################################################################


########################## Preparing files processing ##########################

# define regex patterns #########################################################
pattern1 = re.compile( 'Title' )                                                # start of molecule pattern
pattern2 = re.compile( '#######' )                                              # end of molecule pattern
pattern3 = re.compile( 'Atom' )                                                 # start of atom description section pattern
pattern4 = re.compile( 'Stretch' )                                              # start of atom description section pattern
pattern5 = re.compile( 'Bend_angle' )                                           # start of atom description section pattern
pattern6 = re.compile( 'Proper_torsion' )                                       # start of atom description section pattern
pattern7 = re.compile( 'Improper_torsion' )                                     # start of atom description section pattern
pattern8 = re.compile( 'chloro' )                                               # type of molecule pattern
pattern9 = re.compile( 'bromo' )                                                # type of molecule pattern
pattern10 = re.compile( 'iodo' )                                                # type of molecule pattern
pattern11 = re.compile( 'name:' )                                               # start of molecule in complinfo pattern
pattern12 = re.compile( 'pairs')                                                # start of pair section in complinfo pattern

# define variables ##############################################################
openfile = 0                                                                    # openfile variable set to 0
file_total = 0                                                                  # file_total variable set to 0
section = 'none'                                                                # section variable set to 'none'
title = []                                                                      # declare a title array
virtual_array = []                                                              # create a virtual array
complinfo = dict()                                                              # creates a complementary info dictionary
pairinfo = dict()                                                               # creates a pair info dictionary
title_code = '000'                                                              # start a 3 digit code for every title
atom_number = 0                                                                 # atom number variable set to 0

# handle file directories #######################################################
if os.path.exists( './TOP' ) :                                                  # if the old directory exists
    shutil.rmtree( './TOP' )                                                    # delete old directory
if os.path.exists( './TOP_nonbonded' ) :                                        # if the old directory exists
    shutil.rmtree( './TOP_nonbonded' )                                          # delete old directory 2
os.mkdir( './TOP' )                                                             # create a directory to output files
os.mkdir( './TOP_nonbonded' )                                                   # create a directory to output files 2

################################################################################


######################### Processing complementary data ########################

print '\n\t Parsing ' + sys.argv[2] + ' complementary file...'                  # verbose

# Fill up the complementary info dictionary #####################################
for i in range( 0, len(lines2) ) :                                              # for every line in the complementary info file
    if ( pattern11.search(lines2[i]) ) :                                        # if we arrive at a new molecule
        title = lines2[i].split()                                               # set new current title
        complinfo[title[2]] = []                                                # add title as a key in the first dictionary
        pairinfo[title[2]] = []                                                 # add title as a key in the second dictionary
        section = 'none'                                                        # set section to none
        continue                                                                # continue

    if section == 'none' :                                                      # if we are not in the [ pairs ] section
        if 'at.num' in lines2[i] :                                                   # if a read line is comented
            continue                                                            # skip it

        if lines2[i] == '\n' or lines2[i].isspace() :                           # if a read line is empty
            continue                                                            # skip it

        if ( pattern12.search(lines2[i]) ) :                                    # if we get to the pairs section
            section = 'pairs'                                                   # set the section to pair
            continue                                                            # continue

        current_line = lines2[i].split()                                        # split the current line
        complinfo[title[2]].append((current_line[0], current_line[1], current_line[2])) # add the data in the correct dictionary section

    else :                                                                      # if we are at the pairs section
        if lines2[i] == '\n' or lines2[i].isspace() :                           # if a read line is empty
            continue                                                            # skip it

        if 'fn' in lines2[i] :                                                  # if a read line is comented
            continue                                                            # skip it

        current_line = lines2[i].split()                                        # split the current line
        pairinfo[title[2]].append((current_line[0], current_line[1], current_line[2])) # add the data in the correct dictionary section

print '\t\t Finished processing ' + sys.argv[2] + ' file!'                      # verbose

################################################################################


################################ Writing files #################################

print '\n\t Writing files...'                                                     # verbose

# read the .txt file ############################################################
for i in range( 18, len(lines) ) :                                              # for every line read from the file

    if( pattern1.search(lines[i]) ) :                                           # if a molecule title is found

        title = lines[i].split()                                                # set new current title
        title[1] = re.sub('[",]', '', title[1])                                 # strip useless characters from the title
        title[len(title)-1] = re.sub('[",]', '', title[len(title)-1])           # strip useless characters from the title
        title_code = '%03d' % (int(title_code) + 1)                             # increment the 3 digit code
        atom_number = 0                                                         # set the atom_number to 0

        # create the fixed titles ###############################################
        if title[1] == title[len(title)-1] :
            fixed_title_1 = title_code + '_' + title[1] + '.itp'
            fixed_title_2 = title_code + '_' + title[1] + '_ffnonbonded.itp'
        else :
            fixed_title_1 = title_code + '_' + title[1] + '_' + title[len(title)-1] + '.itp'
            fixed_title_2 = title_code + '_' + title[1] + '_' + title[len(title)-1] + '_ffnonbonded.itp'

        # open the files ########################################################
        filout_1 = open( title_code + '.itp', 'w' )                             # create a new .itp file
        filout_2 = open( title_code + '_ffnonbonded.itp', 'w' )                 # create a second .itp file
        openfile = 1                                                            # set openfile to 1
        virtual_array = []                                                      # reset the virtual array

        # write .itp header #####################################################
        filout_1.write('; #####################################################\n')
        filout_1.write('; Topology file generated by the txt2itp.py script\n')
        filout_1.write('; Script written by Edouard Mobarak\n')
        filout_1.write('; Parameter conversion done by Fabio Lolicato\n')
        filout_1.write('; Version 1.2\n')
        filout_1.write('; #####################################################\n\n')
        filout_1.write('; ' + title[1] + '\n\n')
        filout_1.write('[ moleculetype ]\n')
        filout_1.write('; Name        nrexcl\n')
        filout_1.write('; MOL         3\n')

    if( openfile == 1 ) :                                                       # if a file is open

        if lines[i] == '\n' or lines[i].isspace() :                             # if a read line is empty
            continue                                                            # skip it

        # atom section header ###################################################
        if( pattern3.search(lines[i]) ) :                                       # if an Atom title is found
            filout_1.write('\n[ atoms ]\n')                                     # write atom description
            filout_1.write(';   nr    type   resnr  residu    atom    cgnr  charge      mass\n')
            filout_2.write('[ atomtypes ]\n')                                   # write atomtypes description in file 2
            filout_2.write('; name     at_num    mass    charge   ptype          sigma      epsilon\n')
            section = 'atom'                                                    # set the section variable to 'atom'

        elif( pattern4.search(lines[i]) ) :                                     # if a Stretch title is found
            # virtual atom section writing ######################################
            if len(virtual_array) > 0 :                                         # if the virtual array is not empty
                # type LC2 ######################################################
                if virtual_array[0][5] == 'LC2' :                               # if its a LC2 virtual atom
                    filout_1.write('\n[ virtual_sites2 ]\n')                    # write virtual atom description
                    filout_1.write(';   nr  from         funct               a\n')

                    for j in range( 0, len(virtual_array) ) :                   # parse the virtual array
                        filout_1.write('%6s' % virtual_array[j][0])
                        filout_1.write('%6s' % virtual_array[j][7])
                        filout_1.write('%6s' % virtual_array[j][8])
                        filout_1.write('%8s' % 1)
                        if pattern8.search(fixed_title_1) :                     # if it's a chloro atom
                            filout_1.write('%16s' % -0.927468753 + '\n')        # print the correct value
                        elif pattern9.search(fixed_title_1) :                   # if it's a bromo atom
                            filout_1.write('%16s' % -0.841023233 + '\n')        # print the correct value
                        else :                                                  # if it's an iodo atom
                            filout_1.write('%16s' % -0.861074194 + '\n')        # print the correct value

                # type LC3 ######################################################
                else :
                    filout_1.write('\n[ virtual_sites3 ]\n')                    # write virtual atom description
                    filout_1.write(';   nr  from               funct               a               b\n')

                    for j in range( 0, len(virtual_array) ) :                   # parse the virtual array
                        filout_1.write('%6s' % virtual_array[j][0])
                        filout_1.write('%6s' % virtual_array[j][7])
                        filout_1.write('%6s' % virtual_array[j][8])
                        filout_1.write('%6s' % virtual_array[j][9])
                        filout_1.write('%8s' % 1)
                        filout_1.write('%16s' % -0.292627)
                        filout_1.write('%16s' % -0.292627 + '\n')

            filout_1.write('\n[ bonds ]\n')                                     # write bond description
            filout_1.write(';  ai    aj funct           c1           c2\n')
            section = 'bond'                                                    # set the section variable to 'bond'

        # pairs section header ##################################################
        elif( pattern5.search(lines[i]) ) :                                     # if a Bend_angle title is found
            filout_1.write('\n[ pairs ]\n')                                     # write pairs description
            filout_1.write(';  ai    aj funct           c1           c2\n')

            # pairs section writing #############################################
            for j in range ( 0, len(pairinfo[title_code]) ) :                   # for every pair in the dictionary
                filout_1.write('%5s' % pairinfo[title_code][j][0])              # write it in the file
                filout_1.write('%6s' % pairinfo[title_code][j][1])
                filout_1.write('%6s' % pairinfo[title_code][j][2] + '\n')

        # angle section header ##################################################
            filout_1.write('\n[ angles ]\n')                                    # write angle description
            filout_1.write(';  ai    aj    ak funct           c1           c2\n')
            section = 'angle'                                                   # set the section variable to 'angle'

        # dihedral section header ###############################################
        elif( pattern6.search(lines[i]) ) :                                     # if an Proper_torsion title is found
            filout_1.write('\n[ dihedrals ]\n')                                 # write dihedrals description
            filout_1.write(';  ai    aj    ak    al funct           c1           c2           c3          c4\n')
            section = 'dihedral'                                                # set the section variable to 'dihedral'

        # impropers section header###############################################
        elif( pattern7.search(lines[i]) ) :                                     # if an Improper_torsion title is found
            filout_1.write('\n[ dihedrals ]\n')                                 # write improper description
            filout_1.write(';  ai    aj    ak    al funct           c1           c2    (impropers)\n')
            section = 'improper'                                                # set the section variable to 'improper'

        # move finished files in folder #########################################
        if( pattern2.search(lines[i]) ) :                                       # if a molecule end chain is found
            filout_1.close()                                                    # close that .itp file
            filout_2.close()                                                    # close the other file
            openfile = 0                                                        # reset openfile variable to 0
            section = 'none'                                                    # reset section variable to 'none'
            os.rename ( './' + title_code + '.itp', './TOP/' + title_code + '.itp') # move the file in the created directory
            os.rename ( './' + title_code + '_ffnonbonded.itp', './TOP_nonbonded/' + title_code + '_ffnonbonded.itp' ) # move the file in the created directory
            file_total = file_total + 2                                         # increment file total
            print '\t\t ' + title_code + '.itp file written successfully.'      # verbose
            print '\t\t ' + title_code + '_ffnonbonded.itp file written successfully.'  # verbose

        # atom section writing ##################################################
        if section == 'atom' and not pattern3.search(lines[i]) :                # when we are in the 'atom' section
            current_line = lines[i].split()                                     # split the line
            fixed_line_1 = float(current_line[2]) / 10
            fixed_line_2 = float(current_line[3]) * 4.184
            filout_1.write('%6s' % current_line[0])                             # write the information in the new file

            if fixed_line_1 == 0 and fixed_line_2 == 0 :                        # if the atom is virtual
                filout_1.write('%8s' % current_line[5])
                filout_1.write('%8s' % '1')
                filout_1.write('%8s' % title[len(title)-1])
                filout_1.write('%16s' % current_line[0])
                filout_1.write('%8s' % current_line[1])
                filout_1.write('%10s' % 0.0 +'\n')                             # set the mass to zero

                # gather LC2  data ##############################################
                if current_line[5] == 'LC2' :                                   # if it's an LC2 virtual atom
                    virtual_array.append([current_line[0], current_line[1],
                    current_line[2], current_line[3], current_line[4], current_line[5],
                    current_line[6],current_line[7], current_line[8]])          # get the info
                # gather LC3  data ##############################################
                else :                                                          # if it's an LC3
                    virtual_array.append([current_line[0], current_line[1],
                    current_line[2], current_line[3], current_line[4], current_line[5],
                    current_line[6],current_line[7], current_line[8], current_line[9]]) # get the info

            else :
                filout_1.write('%16s' % '1')
                filout_1.write('%8s' % title[len(title)-1])
                filout_1.write('%16s' % current_line[0])
                filout_1.write('%8s' % current_line[1] + '\n')

            if fixed_line_1 == 0.0 and fixed_line_2 == 0.0 :                    # if the atom is virtual
                filout_2.write('%6s' % current_line[5])                         # write the information in the second file
                filout_2.write('%11s' % 0)
                filout_2.write('%8s' % 0)
                filout_2.write('%10s' % current_line[1])
                filout_2.write('%23s' % fixed_line_1)                           # fixed line
                filout_2.write('%13s' % fixed_line_2 + '\n')                    # fixed line
            else :                                                              # if it is not
                filout_2.write('%6s' % complinfo[title_code][atom_number][0])   # write the information in the second file
                filout_2.write('%11s' % complinfo[title_code][atom_number][1])
                filout_2.write('%8s' % complinfo[title_code][atom_number][2])
                filout_2.write('%10s' % current_line[1])
                filout_2.write('%23s' % fixed_line_1)                           # fixed line
                filout_2.write('%13s' % fixed_line_2 + '\n')                    # fixed line
                atom_number = atom_number + 1                                   # increment the atom number

        # bond section writing ##################################################
        elif section == 'bond' and not pattern4.search(lines[i]) :              # when we are in the 'bond' section
            current_line = lines[i].split()                                     # split the line
            fixed_line_1 = float(current_line[3]) / 10                          # convert line
            fixed_line_2 = float(current_line[2]) * 418.4                       # convert line
            filout_1.write('%5s' % current_line[0])                             # write the information in the new file
            filout_1.write('%6s' % current_line[1])
            filout_1.write('%6s' % 1)
            filout_1.write('%13s' % str(fixed_line_1))
            filout_1.write('%13s' % str(fixed_line_2) + '\n')                   # fixed line

        # angle section writing #################################################
        elif section == 'angle' and not pattern5.search(lines[i]) :             # when we are in the 'angle' section
            current_line = lines[i].split()                                     # split the line
            fixed_line = float(current_line[3]) * 4.184                         # convert line
            filout_1.write('%5s' % current_line[0])                             # write the information in the new file
            filout_1.write('%6s' % current_line[1])
            filout_1.write('%6s' % current_line[2])
            filout_1.write('%6s' % 1)
            filout_1.write('%13s' % current_line[4])
            filout_1.write('%13s' % str(fixed_line) + '\n')                     # fixed line

        # dihedral section writing ##############################################
        elif section == 'dihedral' and not pattern6.search(lines[i]) :          # when we are in the 'dihedral' section
            current_line = lines[i].split()                                     # split the line
            fixed_line_1 = float(current_line[4]) * 4.184                       # convert line
            fixed_line_2 = float(current_line[5]) * 4.184                       # convert line
            fixed_line_3 = float(current_line[6]) * 4.184                       # convert line
            fixed_line_4 = float(current_line[7]) * 4.184                       # convert line
            filout_1.write('%5s' % current_line[0])                             # write the information in the new file
            filout_1.write('%6s' % current_line[1])
            filout_1.write('%6s' % current_line[2])
            filout_1.write('%6s' % current_line[3])
            filout_1.write('%6s' % 5)
            filout_1.write('%13s' % str(fixed_line_1))                          # fixed line
            filout_1.write('%13s' % str(fixed_line_2))                          # fixed line
            filout_1.write('%13s' % str(fixed_line_3))                          # fixed line
            filout_1.write('%12s' % str(fixed_line_4) + '\n')                   # fixed line

        # improper section writing ##############################################
        elif section == 'improper' and not pattern7.search(lines[i]) :          # when we are in the 'improper' section
            current_line = lines[i].split()                                     # split the line
            fixed_line = float(current_line[4]) * 4.184                         # convert line
            filout_1.write('%5s' % current_line[0])                             # write the information in the new file
            filout_1.write('%6s' % current_line[1])
            filout_1.write('%6s' % current_line[2])
            filout_1.write('%6s' % current_line[3])
            filout_1.write('%6s' % 1)
            filout_1.write('%13s' % fixed_line + '\n')                          # fixed line

print '\n\t ' + str(file_total) + ' output files written successfully.'         # verbose

################################################################################


################################ Ending program ################################

print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'

################################################################################


# TO DO!!!
# remove the file names, and only put the 3 digit code (for pdb, nonbonded.itp and .itp files)
# change the molecule name in the .itp files to just MOL
#




################################### old code ###################################

# sort and remove duplicates from pair array ####################################
# for j in range ( 0, len(pairs_array) ) :                                      # for all pairs in the array
#     if int(pairs_array[j][0]) > int(pairs_array[j][1]) :                      # sort each pair units individually
#         temp = pairs_array[j][0]
#         pairs_array[j][0] = pairs_array[j][1]
#         pairs_array[j][1] = temp
#
# pairs_array = sorted(pairs_array)                                             # sort the pairs array in order
#
# pairs_array_fixed = []                                                        # declare a pairs array with no duplicates
#
# for j in range( 0, len(pairs_array) ) :                                       # remove duplicates from old pairs array
#     if pairs_array[j] not in pairs_array_fixed :
#         pairs_array_fixed.append(pairs_array[j])
#
# write pairs data ##############################################################
# for j in range ( 0, len(pairs_array_fixed) ) :                                # for every pair in the fixed array
#     filout_1.write('%5s' % pairs_array_fixed[j][0])                           # write it in the file
#     filout_1.write('%6s' % pairs_array_fixed[j][1])
#     filout_1.write('%6s' % 1 + '\n')
#
################################################################################
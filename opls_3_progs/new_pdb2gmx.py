#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library
import collections                                                              # sorted dictionary library
import numpy as np                                                              # math library

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\t ***************************** Help section *****************************'
    print '\t ************************************************************************\n'
    print '\t > This program calls the GROMACS pdb2gmx command to generate a topology'
    print '\t .top file based on a .gro or .pdb structure file, and then processes.'
    print '\t the .top file and do a few corrections based on the OPLS3 force field.\n'
    print '\t > Example line:\n'
    print '\t\t ./new_pdb2gmx.py > new_pdb2gmx_log.log'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


############################### Starting program ###############################

print '\n\t ************************************************************************'
print '\t ************************** new_pdb2gmx.py v1.0 ************************'
print '\t ************************************************************************'
print '\n\t This program was written by Edouard Mobarak:'
print '\t - Doctoral Student at the University of Helsinki'
print '\t - Biological Physics and Soft Matter group'
print '\t - Supervised by Dr. Tomasz Rog and Prof. Ilpo Vattulainen'
print '\n\t Version v1.0 - 13/06/2018'
print '\t Requires Python v2.7\n'
print '\t ************************************************************************'

################################################################################


################################# Reading files ################################

for i in range( 1 ,len( sys.argv ) ) :                                          # preliminary check: for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()

file_name = "MCL1_top_fixed.top"

try :                                                                           # error handeling
    filin = open( file_name )
except IOError :
    print '\n ERROR(1): Unable to open file "' + file_name + '", please check file name or file location.'
    print ' > Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exits if file is missing or the name is entered wrong
else :
    print '\n\t Reading file "' + file_name + '"...'
    lines = filin.readlines()                                                   # saving every line of every file in the file array
    filin.close()
    print '\t\t File "' + file_name + '" read successfully.'

################################################################################


####################### Sorting data in data structures ########################

print '\n\t Saving file content in data structure...'

# for the pdb2gmx topology .top file
atom_section_top = 0                                                            # get to the atom section
while "[ atoms ]" not in lines[atom_section_top] :
    atom_section_top = atom_section_top + 1
atom_section_top = atom_section_top + 1

bond_section_top = atom_section_top                                             # get to the bond section
while "[ bonds ]" not in lines[bond_section_top] :
    bond_section_top = bond_section_top + 1
bond_section_top = bond_section_top + 1

pairs_section_top = bond_section_top                                            # get to the pairs section
while "[ pairs ]" not in lines[pairs_section_top] :
    pairs_section_top = pairs_section_top + 1
pairs_section_top = pairs_section_top + 1

angle_section_top = pairs_section_top                                           # get to the angle section
while "[ angles ]" not in lines[angle_section_top] :
    angle_section_top = angle_section_top + 1
angle_section_top = angle_section_top + 1

torsion_section_top = angle_section_top                                         # get to the dihedral section
while "[ dihedrals ]" not in lines[torsion_section_top] :
    torsion_section_top = torsion_section_top + 1
torsion_section_top = torsion_section_top + 1

improper_section_top = torsion_section_top                                      # get to the improper section
while "[ dihedrals ]" not in lines[improper_section_top] :
    improper_section_top = improper_section_top + 1
improper_section_top = improper_section_top + 1

top_atom_arr = []                                                               # array for the sections of the .top file

for i in range ( atom_section_top, bond_section_top-2 ) :                       # for every atom in the .top file
    if not lines[i].startswith(";") :                                           # if it does not start with ";"
        splitline = lines[i].split()                                            # split the line
        top_atom_arr.append(splitline)                                          # append it to the array
#        print splitline

################################################################################


################################# process file #################################

print '\n\t Processing file...'

filout = open( file_name + "_fixed", "w" )                                      # open output file

for i in range(0, angle_section_top+1) :                                        # write everything until the angle section
    filout.write(lines[i])

print '\t\t Correcting angles...'

for i in range(angle_section_top+1, torsion_section_top-1) :                    # for the angle section

    atom_1 = 0                                                                  # reset the atoms 1 2 and 3 variables
    atom_2 = 0
    atom_3 = 0
    next_is_pro = 0                                                             # reset the proline flag to 0

    if lines[i] != '\n':                                                        # if the line is not empty
        current_line = lines[i].split()                                         # split it

        for j in range(0, len(top_atom_arr)) :                                  # for every atom
            if current_line[0] == top_atom_arr[j][0]:                           # if the first atom of the angle is found
                if top_atom_arr[j][4] == "CA":                                  # if the first atom is a CA
                    atom_1 = 1                                                  # set atom flag to 1

            if current_line[1] == top_atom_arr[j][0]:                           # if the second atom of the angle is found
                if top_atom_arr[j][4] == "C":                                   # if the second atom is a C
                    atom_2 = 1                                                  # set atom flag to 1

            if current_line[2] == top_atom_arr[j][0]:                           # if the third atom of the angle is found
                if top_atom_arr[j][4] == "N":                                   # if the third atom is a N
                    if top_atom_arr[j][3] == "PRO":                             # if the next residue is a proline
                        next_is_pro = 1                                         # set pro flag to 1
                        atom_3 = 1                                              # set atom flag to 1

            if atom_1 + atom_2 + atom_3 == 3:                                   # if all flags are set to 1
                break                                                           # leave the loop

        # if all flags are set to 1 and the next atom is a proline
        if atom_1 + atom_2 + atom_3 == 3 and next_is_pro == 1 and current_line[4] == "114.6":
            print "\t\t\t fixing CA C N normal case: " + current_line[0] + " " + current_line[1] + " " + current_line[2]
            filout.write('%5s' % current_line[0])
            filout.write('%6s' % current_line[1])
            filout.write('%6s' % current_line[2])
            filout.write('%6s' % current_line[3])
            filout.write('%9s' % "112.200")
            filout.write('%12s' % "292.880")
            filout.write('\n')
        else :                                                                  # or else
            filout.write(lines[i])                                              # just output the line

print '\t\t Correcting torsion...'


filout.write("\n[ dihedrals ]\n")                                                 # writing missing headers
filout.write(";  ai    aj    ak    al funct            c0            c1            c2            c3            c4            c5\n")


for i in range(torsion_section_top+1, improper_section_top-1) :                 # for the torsion section

    atom_1 = 0                                                                  # reset the atoms 1 2 and 3 variables
    atom_2 = 0
    atom_3 = 0
    atom_4 = 0
    next_is_pro = 0                                                             # reset the proline flag to 0
    current_is_pro = 0                                                          # reset the current proline flag to 0
    current_is_gly = 0                                                          # reset the current glycine flag to 0

    if lines[i] != '\n':                                                        # if the line is not empty
        current_line = lines[i].split()                                         # split it

        for j in range(0, len(top_atom_arr)) :                                  # for every atom
            if current_line[0] == top_atom_arr[j][0]:                           # if the first atom of the angle is found
                if top_atom_arr[j][4] == "N":                                   # if the first atom is a N
                    atom_1 = 1                                                  # set atom flag to 1

                elif top_atom_arr[j][4] == "CB":                                # else if the first atom is a CB
                    atom_1 = 1                                                  # set atom flag to 1

            if current_line[1] == top_atom_arr[j][0]:                           # if the second atom of the angle is found
                if top_atom_arr[j][4] == "CA":                                  # if the second atom is a CA
                    atom_2 = 1                                                  # set atom flag to 1

                    if top_atom_arr[j][3] == "PRO":                             # if the current residue is a proline
                        current_is_pro = 1                                      # set pro flag to 1

                    elif top_atom_arr[j][3] == "GLY":                           # if the current residue is a proline
                        current_is_gly = 1                                      # set gly flag to 1

            if current_line[2] == top_atom_arr[j][0]:                           # if the third atom of the angle is found
                if top_atom_arr[j][4] == "C":                                   # if the third atom is a C
                    atom_3 = 1                                                  # set atom flag to 1

            if current_line[3] == top_atom_arr[j][0]:                           # if the fourth atom of the angle is found
                if top_atom_arr[j][4] == "N":                                   # if the fourth atom is a N
                    if top_atom_arr[j][3] == "PRO":                             # if the next residue is a proline
                        next_is_pro = 1                                         # set pro flag to 1
                        atom_4 = 1                                              # set atom flag to 1

            if atom_1 + atom_2 + atom_3 + atom_4 == 4:                          # if all flags are set to at least 1
                break                                                           # leave the loop

        # if all flags are set to 1 and the next atom is a proline
        if atom_1 + atom_2 + atom_3 + atom_4 == 4 and next_is_pro == 1:
            if current_line[5] == "2.49" :
                print "\t\t\t fixing N CA C N normal case: " + current_line[0] + " " + current_line[1] + " " + current_line[2] + " " + current_line[3]
                filout.write('%5s' % current_line[0])
                filout.write('%6s' % current_line[1])
                filout.write('%6s' % current_line[2])
                filout.write('%6s' % current_line[3])
                filout.write('%6s' % current_line[4])
                filout.write('%7s' % "-7.958")
                filout.write('%7s' % "17.154")
                filout.write('%7s' % "-2.489")
                filout.write('%7s' % "-0.828")
                filout.write('\n')
            elif current_line[5] == "-6.52" :
                print "\t\t\t fixing N CA C N PRO case: " + current_line[0] + " " + current_line[1] + " " + current_line[2] + " " + current_line[3]
                filout.write('%5s' % current_line[0])
                filout.write('%6s' % current_line[1])
                filout.write('%6s' % current_line[2])
                filout.write('%6s' % current_line[3])
                filout.write('%6s' % current_line[4])
                filout.write('%7s' % "-15.811")
                filout.write('%7s' % "17.226")
                filout.write('%7s' % "0.548")
                filout.write('%7s' % "2.088")
                filout.write('\n')
            elif current_line[5] == "3.4" :
                print "\t\t\t fixing N CA C N GLY case: " + current_line[0] + " " + current_line[1] + " " + current_line[2] + " " + current_line[3]
                filout.write('%5s' % current_line[0])
                filout.write('%6s' % current_line[1])
                filout.write('%6s' % current_line[2])
                filout.write('%6s' % current_line[3])
                filout.write('%6s' % current_line[4])
                filout.write('%7s' % "-20.359")
                filout.write('%7s' % "15.991")
                filout.write('%7s' % "0.155")
                filout.write('%7s' % "-2.552")
                filout.write('\n')

            elif current_line[5] == "5.26" :
                print "\t\t\t fixing CB CA C N normal case: " + current_line[0] + " " + current_line[1] + " " + current_line[2] + " " + current_line[3]
                filout.write('%5s' % current_line[0])
                filout.write('%6s' % current_line[1])
                filout.write('%6s' % current_line[2])
                filout.write('%6s' % current_line[3])
                filout.write('%6s' % current_line[4])
                filout.write('%7s' % "8.242")
                filout.write('%7s' % "11.954")
                filout.write('%7s' % "-3.485")
                filout.write('%7s' % "-3.368")
                filout.write('\n')
            elif current_line[5] == "7.53" :
                print "\t\t\t fixing CB CA C N PRO case: " + current_line[0] + " " + current_line[1] + " " + current_line[2] + " " + current_line[3]
                filout.write('%5s' % current_line[0])
                filout.write('%6s' % current_line[1])
                filout.write('%6s' % current_line[2])
                filout.write('%6s' % current_line[3])
                filout.write('%6s' % current_line[4])
                filout.write('%7s' % "2.377")
                filout.write('%7s' % "28.405")
                filout.write('%7s' % "-3.561")
                filout.write('%7s' % "4.184")
                filout.write('\n')
        else :                                                                  # or else
            filout.write(lines[i])                                              # just output the line

for i in range(improper_section_top-2, len(lines)) :                            # write everything until the end
    filout.write(lines[i])

filout.close()

print '\t Done!'
print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'
################################################################################

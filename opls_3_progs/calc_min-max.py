#!/usr/bin/env python
import sys                                                                      # system library
import numpy as np

data = np.loadtxt(sys.argv[1], comments=["#","@"])
a=data[:,1]

maximum = max(a)
minimum = min(a)

print (maximum-minimum)*10
#!/usr/bin/env python
import sys                                                                      # system library
import os                                                                       # bash commands library
import shutil                                                                   # more bash commands library

density_array_original = []                                                     # array for all lipids file for original simulations
density_array_repeat_1 = []                                                     # array for all lipids file for repeat 1 simulations
density_array_repeat_2 = []                                                     # array for all lipids file for repeat 2 simulations
density_average_array = []                                                      # array for all lipids averaged

current_file_array = []                                                         # array for the current file


# original - all lipids #########################################################
os.chdir('/wrk/projects/dyes/analysis_results/density_profiles/original')

for i in range( 1, 15 ) :                                                       # for every original system

    current_file_array = []                                                     # reset current file array

    file_name = 'dens_sys' + str(i) + '_art.xvg'                                # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    density_array_original.append(current_file_array)                           # append the file array to the all_lipids array



# repeat 1 - all lipids #########################################################
os.chdir('/wrk/projects/dyes/analysis_results/density_profiles/repeat_1')

for i in range( 1, 15 ) :                                                       # for every repeat_1 system

    current_file_array = []                                                     # reset current file array

    file_name = 'sys' +  str(i) + '_rep1_den.xvg'                               # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    density_array_repeat_1.append(current_file_array)                           # append the file array to the all_lipids array



# repeat 2 - all lipids #########################################################
os.chdir('/wrk/projects/dyes/analysis_results/density_profiles/repeat_2')

for i in range( 1, 15 ) :                                                       # for every repeat_2 system

    current_file_array = []                                                     # reset current file array

    file_name = 'sys' +  str(i) + '_rep2_den.xvg'                               # save the file name

    filin = open( file_name, 'r' )                                              # open the file
    lines = filin.readlines()                                                   # read the file
    filin.close()                                                               # close the file

    for j in range( 0, len(lines) ) :                                           # for every line of the file
        current_line = lines[j].split()                                         # split the line

        if '#' not in current_line[0] and '@' not in current_line[0] :          # if it's not part of the header
            current_file_array.append(current_line)                             # save it in the current file array

    density_array_repeat_2.append(current_file_array)                           # append the file array to the all_lipids array



# average - all lipids ##########################################################

for i in range( 0, len(density_array_original) ) :                              # for every file

    current_file_array = []                                                     # reset current file array
    error_array = []                                                            # reset error array

    for j in range( 0, len(density_array_original[i]) ) :                       # for every line of every file
        if i == 12 :                                                            # if we are at the 13th file
            average_0 = (float(density_array_original[i][j][0]) + float(density_array_repeat_2[i][j][0]))
            average_1 = (float(density_array_original[i][j][1]) + float(density_array_repeat_2[i][j][1]))
            average_2 = (float(density_array_original[i][j][2]) + float(density_array_repeat_2[i][j][2]))
            average_3 = (float(density_array_original[i][j][3]) + float(density_array_repeat_2[i][j][3]))
            average_4 = (float(density_array_original[i][j][4]) + float(density_array_repeat_2[i][j][4]))
        else :
            # average the values
            average_0 = (float(density_array_original[i][j][0]) + float(density_array_repeat_1[i][j][0]) + float(density_array_repeat_2[i][j][0])) / 3
            average_1 = (float(density_array_original[i][j][1]) + float(density_array_repeat_1[i][j][1]) + float(density_array_repeat_2[i][j][1])) / 3
            average_2 = (float(density_array_original[i][j][2]) + float(density_array_repeat_1[i][j][2]) + float(density_array_repeat_2[i][j][2])) / 3
            average_3 = (float(density_array_original[i][j][3]) + float(density_array_repeat_1[i][j][3]) + float(density_array_repeat_2[i][j][3])) / 3
            average_4 = (float(density_array_original[i][j][4]) + float(density_array_repeat_1[i][j][4]) + float(density_array_repeat_2[i][j][4])) / 3
        # save them in the current file array
        current_file_array.append([round(average_0, 6), round(average_1, 6), round(average_2, 6), round(average_3, 6), round(average_4, 6)])

    density_average_array.append(current_file_array)                            # save the averages in the all_lipids_average array

#print density_average_array[0]

#################################################################################

os.chdir('/wrk/projects/dyes/analysis_results/density_profiles')

if os.path.exists( './averaged' ) :                                             # if the old directory exists
    shutil.rmtree( './averaged' )                                               # delete it
os.mkdir( './averaged' )                                                        # create it again

# output all lipids files #######################################################

for i in range ( 0, len(density_average_array) ) :                              # for every file

    file_name = str(i+1) + '_density_averaged.xvg'                              # set the file name
    filout = open( file_name, 'w' )                                             # open the file

    for j in range ( 0, len(density_average_array[i]) ) :                       # for every line of the file
        filout.write( '%12s' % density_average_array[i][j][0] )                 # write atom number
        filout.write( '%15s' % density_average_array[i][j][1] )                 # write averaged order parameter
        filout.write( '%15s' % density_average_array[i][j][2] )                 # write averaged order parameter
        filout.write( '%15s' % density_average_array[i][j][3] )                 # write averaged order parameter
        filout.write( '%15s' % density_average_array[i][j][4] )                 # write averaged order parameter
        filout.write( '\n' )                                                    # jump line

    filout.close()                                                              # close the file
    os.rename ( './' + file_name, './averaged/' + file_name )                   # move the file

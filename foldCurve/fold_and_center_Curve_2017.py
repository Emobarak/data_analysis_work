#!/usr/bin/env python
import sys
import os


try :                                                                           # error handeling
    filin = open( sys.argv[1] )
except IOError :                                                                # error if file not found
    print 'Error 1: unable to open "' + sys.argv[1] + '" file !'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving all lines where they can be accessed like an array
    filin.close()                                                               # closing the file

# preliminary processing ########################################################

filout_name_old = os.path.splitext(sys.argv[1])                                 # stripping extension
filout_name_new = filout_name_old[0] + '_folded.xvg'                            # fixing file name

filout = open( filout_name_new, 'w' )                                           # opening write-only file

# skip header
j = 0                                                                           # line number variable
while ( '#' in lines[j] ) :                                                     # while we are not at the right section
    j = j + 1                                                                   # incrementing variable
while ( '@' in lines[j] ) :                                                     # while we are in the right header section
    filout.write( lines[j] )                                                    # writing header
    j = j + 1

# save all proper data in the column list array
columns = len( lines[j].split() )                                               # number of columns in the file
column_list = [ [] for n in xrange( columns ) ]                                 # list that will carry lists (1 for each column)

for i in range( j, len( lines ) ) :                                             # for each relavant line
    split_line = lines[i].split()                                               # splitting it

    for k in range( 0, columns ) :                                              # for each column in the file
        if '&' in split_line[k] :                                               # skip & characters
            break

        if '@' in split_line[k] :                                               # skip @ characters
            break

        column_list[k].append( float( split_line[k] ) )                         # saving all coords in different columns

# curve folding #################################################################

average_array = []                                                              # array that will store local averages
average_column_array = []                                                       # array that will store global averages

col_size = len(column_list[0])-1                                                # get the total length of a colum

for i in range ( 1, len(column_list) ) :                                        # for every y column

    average_array = []                                                          # reset the average array

    for j in range ( 0, col_size/2 + 1 ) :                                      # for every value in the columns

        average = ( float(column_list[i][j]) + float(column_list[i][col_size-j]) ) / 2  # average the values
        average = round(average, 6)                                             # round it to 6 decimals

        average_array.append(average)                                           # append the average to the local average array

    average_column_array.append(average_array)                                  # append the local average array to the global average array

#print average_column_array[3][0]


for i in range( 0, len(average_column_array) ) :                                # for every y column of the averaged array
    for j in range( 0, len(average_column_array[i]) ) :
        filout.write( '%12s' %  str ( column_list[0][len(column_list[0])-j-1] ) )
        filout.write( '%12s' %  str ( average_column_array[i][j] ) )
        filout.write( '\n' )
    filout.write( '&\n' )
filout.close()

################################################################################

#print '\nCurves have been successfully folded for the "' + filout_name_new + '" file !'
#print 'Please check the "folded.out" file for more details !\n'

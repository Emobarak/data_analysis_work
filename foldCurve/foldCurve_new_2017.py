#!/usr/bin/env python
import sys
import os


try :                                                                           # error handeling
    filin = open( sys.argv[1] )
except IOError :                                                                # error if file not found
    print 'Error 1: unable to open "' + sys.argv[1] + '" file !'
    sys.exit(1)
else :
    lines = filin.readlines()                                                   # saving all lines where they can be accessed like an array
    filin.close()                                                               # closing the file

# preliminary processing ########################################################

filout_name_old = os.path.splitext(sys.argv[1])                                 # stripping extension
filout_name_new = filout_name_old[0] + '_folded.xvg'                            # fixing file name

filout = open( filout_name_new, 'w' )                                           # opening write-only file

j = 0                                                                           # line number variable
while ( '#' in lines[j] ) :                                                     # while we are not at the right section
    j = j + 1                                                                   # incrementing variable
while ( '@' in lines[j] ) :                                                     # while we are in the right header section
    filout.write( lines[j] )                                                    # writing header
    j = j + 1

k = 0                                                                           # column number variable
columns = len( lines[j].split() )                                               # number of columns in the file
column_list = [ [] for n in xrange( columns ) ]                                 # list that will carry lists (1 for each column)

for i in range( j, len( lines ) ) :                                             # for each relavant line

    split_line = lines[i].split()                                               # splitting it

    for k in range( 0, columns ) :                                              # for each column in the file

        if '&' in split_line[k] :                                               # skip & characters
            break

        if '@' in split_line[k] :                                               # skip @ characters
            break

        column_list[k].append( float( split_line[k] ) )                         # saving all coords in different columns

# curve folding #################################################################

p_peak1 = 0                                                                     # initializing the first phosphorus peak value at 0
for i in range ( 0, len( column_list[3] )/2 ) :                                 # for the first half of the coord list
    if ( p_peak1 <= column_list[3][i] ) :                                       # if the old peak value is lower than the new value
        p_peak1 = column_list[3][i]                                             # save the new peak value

p_peak2 = 0                                                                     # initializing the second peak value at 0
for i in range ( len( column_list[3] )/2, len( column_list[3] ) ) :             # same thing for the second half of the list
    if ( p_peak2 <= column_list[3][i] ) :                                       # if the old peak value is lower than the new value
        p_peak2 = column_list[3][i]                                             # save the new peak value

peak_ind1 = column_list[3].index(p_peak1)                                       # finding their indexes
peak_ind2 = column_list[3].index(p_peak2)                                       # finding their indexes

print peak_ind1
print peak_ind2

for k in range ( 1, len( column_list ) ) :                                      # for all y columns

    j = peak_ind2                                                               # setting j to the second Phosphate peak index

    for i in range( peak_ind1, -1, -1 ) :                                       # from the first peak index to 0
        folded = ( column_list[k][i] + column_list[k][j] )/2                    # folding (averaging) corresponding indexes
        column_list[k][j] = folded                                              # filling up the folded list
        j = j + 1                                                               # inrementing j

    i = peak_ind1+1                                                             # setting i to the index after the first Phostphate peak
    j = peak_ind2-1                                                             # setting j to the index before the second Phostphate peak

    while ( i != j ) :                                                          # while i and j are different
        if ( i+1 == j ) :                                                       # if they are just next to each other
            folded = ( column_list[k][i] + column_list[k][j] )/2                # folding (averaging) corresponding indexes
            column_list[k][j] = folded                                          # filling up the folded list
            break                                                               # break the loop

        folded = ( column_list[k][i] + column_list[k][j] )/2                    # folding (averaging) corresponding indexes
        column_list[k][j] = folded                                              # filling up the folded list
        j = j - 1                                                               # decrementing j
        i = i + 1                                                               # incrementing i
    #folding curve inside the peaks

j = 0                                                                           # setting j to 0
for i in range( len( column_list[0] )/2, len( column_list[0] ) ) :              # for every coord of the second half
    filout.write( '%12s' %  str ( column_list[0][j] ) )                         # writing x coords
    j = j+1                                                                     # incrementing j
    for k in range( 1, columns ) :                                              # for every column
        filout.write( '%15s' % str( column_list[k][i] ) )                       # writing y coords
    filout.write( '\n' )
#writing coords

################################################################################
#ending program
filout.close()

print '\nCurves have been successfully folded for the "' + filout_name_new + '" file !'
print 'Please check the "folded.out" file for more details !\n'







#import heapq                                                                       #needed to find the two largest numbers
#heapq.nlargest( 2, column_list[3] )                                                #finding the largest values
#folded_list = [None]*len( 1000 )                                                   #initializing empty list with known size
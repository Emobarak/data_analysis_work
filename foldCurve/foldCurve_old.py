#!/usr/bin/env python

import sys																			#needed to allow system commands


print '\nWelcome to the fold curving program.'

################################################################################

#reading density file
splined_file = raw_input( 'Please specify splined .out file: ' )					#inputing density .xvg file

try :
	filin = open( splined_file, 'r' )												#opening the file
except IOError :																	#error if file not found
	print 'Error 1: unable to open "' + splined_file + '" file !'
	sys.exit(1)
else :
	lines = filin.readlines()														#saving all lines where they can be accessed like an array
	filin.close()																	#closing the file

################################################################################

#folding the graphs and writing new coordinates
filout = open( 'folded.out', 'w' )													#opening write-only file

j = 0																				#line number variable
while ( '#' in lines[j] ) :															#while we are not at the right section
	j = j + 1																		#incrementing variable
while ( '@' in lines[j] ) :															#while we are in the right header section
	filout.write( lines[j] )														#writing header
	j = j + 1

k = 0																				#column number variable
columns = len( lines[j].split() )													#number of columns in the file
column_list = [ [] for n in xrange( columns ) ]										#list that will carry lists (1 for each column)
for i in range( j, len( lines ) ) :													#for each relavant line
	split_line = lines[i].split()													#splitting it
	for k in range( 0, columns ) :													#for each column in the file
		column_list[k].append( float( split_line[k] ) )								#saving all coords in different lists
#splitting the columns and saving the values

################################################################################

for i in range( 0, len( column_list[0] )/2 ) :										#for every x coord
	j = len( column_list[0] )-i-1													#reverse index variable
	for k in range( 1, columns ) :													#for each y coord column
		temp = ( column_list[k][i] + column_list[k][j] )/2							#averaging y coords between the two folds
		column_list[k][i] = temp
		column_list[k][j] = temp
#folding the curves right in the middle and averaging

for i in range( len( column_list[0] )/2, len( column_list[0] ) ) :					#for every coord of the second half
	filout.write( '\t' + str ( column_list[0][i] ) )								#writing x coords
	for k in range( 1, columns ) :													#for every column
		filout.write( '\t\t\t' + str( column_list[k][i] ) )							#writing y coords
	filout.write( '\n' )
#writing coords

################################################################################
#ending program
filout.close()

print '\nCurves have been successfully folded for the "' + splined_file + '" file !'
print 'Please check the "folded.out" file for more details !\n'
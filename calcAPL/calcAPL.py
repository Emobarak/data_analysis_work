#!/usr/bin/env python

import sys																			#needed to allow system commands


lip_list = [ 'dppc', 'dlpc', 'dlpe', 'cho',
			'dlps', 'dopc', 'smpc',
			'dspc', 'dope', 'popc', 'pope' ]										#lipid list

print '\nWelcome to the APL (Area per lipid) calculator program.'

################################################################################

#reading topology file
top_file = raw_input( 'Please specify topology .top file: ' )						#inputing topology file

try :
	filin = open( top_file, 'r' )													#opening the file
except IOError :																	#error if file not found
	print 'Error 1: unable to open "' + top_file + '" file !'
	sys.exit(1)
else :
	lines = filin.readlines()														#saving all lines where they can be accessed like an array
	filin.close()																	#closing the file

################################################################################

#calculating lipid number per membrane phase
j = 0																				#loop variable set to 0
while '[ molecules ]' not in lines[j] :												#while we are not at the [ molecules ] section
	j = j + 1																		#incrementing j

lip_num = 0
print '\nEntities found in the system: '
for i in range( j+1, len( lines ) ) :												#for each line of the [ molecules ] section
	num = lines[i].split()															#getting its number
	sys.stdout.write( "\n" + num[0] )
	for lip in lip_list :															#for each lipid in the list
		if lip in lines[i].lower() :												#if a lipid exists in the top file
			sys.stdout.write( ' (selected)' )
			lip_num = lip_num + int( num[1] )										#summing it

lip_num = (lip_num/2)																	#dividing it by 2

print '\n\nThere are ' + str( lip_num ) + ' lipids per fold !\n'

################################################################################

#reading box xvg file
box_file = raw_input( 'Please specify box .xvg file: ' )							#inputting box xvg file

try :
	filin = open( box_file, 'r' )
except IOError :
	print 'Error 2: unable to open "' + box_file + '" file !'
	sys.exit(1)
else :
	lines = filin.readlines()
	filin.close()

################################################################################

#calculating and writing area per lipid
filout = open( 'apl_2.xvg', 'w' )														#opening write-only file

j = 0
while ( '#' in lines[j] ) :															#while we are not at the right section
	j = j + 1																		#incrementing variable
while ( '@' in lines[j] ) :															#while we are in the right header section
	if j < 19 :
		filout.write( lines[j] )													#writing header
	if j == 19 :
		filout.write( '@ s1 legend "Area Per Lipid"\n' )
	j = j + 1

for i in range( j, len( lines ) ) :													#for each relavant line
	split_line = lines[i].split()													#spliting the line into columns
	apl = float( split_line[1] ) * float( split_line[2] ) / lip_num					#calculating the area per lipid
	apl = round( apl, 6 )															#rounding up the number
	filout.write( '\t' + split_line[0] + '\t\t' + str( apl ) + '\n' )				#writing what is relevant

################################################################################

#ending program
filout.close()

print '\nAPL has been successfully calculated for the "' + box_file + '" file !'
print 'Please check the "apl.out" file for more details !\n'
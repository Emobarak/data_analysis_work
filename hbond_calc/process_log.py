#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library


############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\n\t Help Menu:'
    print '\t ##########'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


################################# Reading files ################################

if len( sys.argv ) > 1 :                                                        # if at least one file has been entered as argument

    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()                                                          # start the help section

    if len( sys.argv ) > 2 :                                                    # if only one file has been entered
        print '\n ERROR(2): More than one file has been entered, please enter only one .log file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if not sys.argv[1].endswith('.log') :                                       # checks if first file entered is a .txt file
        print '\n ERROR(3): First file entered is not a .log file, please enter one .log file generated by the "run_h_bonds_step.sh" script.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a txt file

else :
    print '\n ERROR(1): No file name entered, please enter a .log file generated by the "run_h_bonds_step.sh" script.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\n\t Starting program...\n'

file_array = []                                                                 # create an array that stores all lines of all files

try :                                                                       # error handeling
    filin = open( sys.argv[1] )
except IOError :
    print '\n ERROR(4): Unable to open "' + sys.argv[1] + '" file, please check file name or file location.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                             # program exits if file is missing or the name is entered wrong
else :
    print '\t Reading file "' + sys.argv[1] + '"...'
    lines = filin.readlines()                                               # saving every line of every file in the file array
    file_array.append( lines )
    filin.close()
    print '\t\t file "' + sys.argv[1] + '" read successfully.'

################################################################################


########################### Parsing content of files ###########################

print '\n\t Processing h_bonds for different systems...'

system_array = []                                                               # array to store systems
temp_array = []                                                                 # temporary array
system = 1                                                                      # system name variable
system_num_array = []                                                           # array to store system name

for i in range( 0, len( file_array[0] ) ) :                                     # for every line in the .log file

    if re.match( 'system', str( file_array[0][i] ) ) :                          # if the word "system" is found
        line = file_array[0][i].split()                                         # split the line
        if system != int( line[2] ) :                                           # if the system has not been encountered before
            system_num_array.append( system )                                   # save the system variable
            system = int( line[2] )                                             # change the system variable
            system_array.append( temp_array )                                   # append the temporary array to the system array
            temp_array = []                                                     # reset the temporary array

    else :                                                                      # or else
        if re.match ( 'Selected', str( file_array[0][i] ) ) :                   # if the word "Selected is found
            line = file_array[0][i].split()                                     # split the line
            group = line[2].replace("'", "")                                    # extract the group from the line (remove ' characters)
            temp_array.append( str( group ) )                                   # append the group to the temp array

        if re.match ( 'No Donors found', str( file_array[0][i] ) ) :            # if no h_bonds are possible
            temp_array.append( 'NA' )                                           # append 'NA' to the temporary array

        if re.match ( 'Average number of hbonds', str( file_array[0][i] ) ) :   # if h_bonds are possible
            line = file_array[0][i].split()                                     # split the line
            hbonds = line[6]                                                    # extract the number of hbonds
            temp_array.append( hbonds )                                         # append the hbonds to the temp array

system_array.append( temp_array )                                               # append the last system
system_num_array.append( system )                                               # append the last system name

################################################################################


########################### Preparing output file 1 ############################

filout = open( 'hbonds_out.txt', 'w' )                                          # preparing output file 1 = atom names
print '\n\t Preparing output hbonds file ...'

################################################################################


############################### Processing data ################################

print '\t Ignoring "NA" and "0" values...'
step = 1                                                                        # step variable set at 1

for i in range( 0, len( system_array ) ) :                                      # for every system

    for j in range( 0, len( system_array[i] ) ) :                               # for every entry in each system

        if step == 1 :                                                          # if the step is 1
            step = step + 1                                                     # increment the step
            filout.write(  '%-6s%-18s' % ( str( system_num_array[i] ), str( system_array[i][j] ) ) )
            # output the system name and group name 1
        elif step == 2 :
            step = step + 1                                                     # increment the step
            filout.write( '%-18s' % ( system_array[i][j] ) )                    # output the group name 2
        else :                                                                  # or else if the step is at 3
            step = 1                                                            # set the step back at 1
            hbonds = system_array[i][j]                                         # save the hbonds value
            # if hbonds != 'NA' and system_array[i][j] != '0.000':                # if the hbonds value is not 'NA' or 0
            #     hbonds = float( system_array[i][j] )/4                          # divide the value by 4 (for the number of dyes)
            filout.write( str( hbonds ) + '\n' )                                # output the hbonds number and jump line

filout.close()                                                                  # close the file

print '\n\t Done!\n'

################################################################################

# FILE FORMAT: system   group_1   group_2   hbonds
# find a way to normalise the hbonds by dividing the ones related to dyes by 4, and the ones related to lipids by the number of lipids
#!/usr/bin/env python

"""
Call from terminal:
python script.py inputfile.log outputfile.log

inputfile.log
    input file with hydrogen bond data got after
    using ndxrange_1_2.py and then process_log.py

outputfile.log
    output file that has all lines removed where
    the number of h bonds is 0.000 or NA

Program reads a .log file that contains information about
the hydrogen bonds (data got after using ndxrange_1_2.py
and then process_log.py)

It then removes all the lines where number of h bonds is
NA or 0.000 and outputs a new file with the remaining
datain the same format as the original file
"""

import sys                                                                      # import needed modules

infile_name = sys.argv[1]                                                       # read in the filenames
outfile_name = sys.argv[2]

print ' '                                                                       # print to terminal
print ' program:  ' +sys.argv[0]                                                # print to terminal
print ' input:    ' +infile_name                                                # print to terminal
print ' removes all pairs which have zero number of H bonds'                    # print to terminal

input1 = open(infile_name,'r')                                                  # open input file for reading

list_data = []                                                                  # initialize a new list

for line in input1:                                                             # read lines in input file
    linesplit = line.split()                                                    # split the line and take the last argument
    if linesplit[3] != 'NA' and linesplit[3] > '0.009':                         # then check that it is not 'NA' or '0.000'
        list_data.append(line)                                                  # append the line to a new list

input1.close                                                                    # close input

output1 = open(outfile_name,'w')                                                # open output file for writing

for line in list_data:                                                          # write to a new file line by line
    output1.write(line)

print ' output:   ' +outfile_name                                               # print to terminal
print ' '                                                                       # print to terminal

output1.close                                                                   # close output


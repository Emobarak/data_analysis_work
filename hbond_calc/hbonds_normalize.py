#!/usr/bin/env python

"""
Call from terminal:
python script.py inputfile.log outputfile.log

inputfile
    intput file that has all lines removed where
    the number of h bonds is 0.000 or NA

outputfile.log
    output file has hbonds values normalized
    (divided by the number of dyes (4 for sys 1_10 and 2 for sys 11_14)

"""

import sys                                                             # import needed modules

infile_name = sys.argv[1]                                              # read in the filenames
outfile_name = sys.argv[2]

print ' '                                                              # print to terminal
print ' program:  ' +sys.argv[0]                                       # print to terminal
print ' input:    ' +infile_name                                       # print to terminal
print ' normalizes hbonds values'                                      # print to terminal

input1 = open(infile_name,'r')                                         # open input file for reading

list_data = []                                                         # initialize a new list

for line in input1:                                                    # read lines in input file
    linesplit = line.split()                                           # split the line and take the last argument
    if int(linesplit[0]) <= 10 :                                       # if the system is 1_10
        linesplit[3] = float(linesplit[3])/4                           # divide the value by 4
#        print linesplit[3]
    else :                                                             # if the system is 11_14
        linesplit[3] = float(linesplit[3])/2                           # divide the value by 2

    list_data.append(linesplit)                                        # append the line to a new list

input1.close                                                           # close input

output1 = open(outfile_name,'w')                                       # open output file for writing

for line in list_data:                                                 # write to a new file line by line
    output1.write( '%-6s%-18s%-18s%-0s' % ( str( line[0] ), str( line[1] ), str( line[2] ), str( line[3] ) ) + '\n')

print ' output:   ' +outfile_name                                      # print to terminal
print ' '                                                              # print to terminal

output1.close                                                          # close output


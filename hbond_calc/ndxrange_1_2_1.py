#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\n\t Help Menu:'
    print '\t ##########'
    print '\t The program takes as input:'
    print '\t - One .txt list file with ranges of indexes'
    print '\t - One or more corresponding .gro files, depending on the .txt file'
    print '\n\t * The program will proceed to translate those indexes groups into groups '
    print '\t of atom names corresponding to the indexes in the list'
    print '\t * As of version 1.1, the program will also output an .ndx file for each '
    print '\t .gro file inputed, with the same groups as the ones in the .txt file'
    print '\t * As of version 1.2, the program will also call the "make_ndx" gromacs '
    print '\t function, and prepare proper .ndx files which will include the index '
    print '\t groups entered in the list .txt file'
    print '\t .gro file inputed, with the same groups as the ones in the .txt file'
    print '\n\t .txt file format:'
    print '\t -----------------'
    print '\t - Starts with a header corresponding to a .gro file: @ Atto647N-CPE-Head'
    print '\t   (the header always begins with an "@" symbol)'
    print '\t   For every header you enter this way, you will have to provide'
    print '\t   a .gro file later as argument, in the correct order'
    print '\t - Use a "#" symbol to comment lines'
    print '\t - Use a number to input an atom index: 12'
    print '\t - Every line is considered to be one group: 13 (one group)'
    print '\t                                             15 (another group)'
    print '\t   So jump lines to define different groups'
    print '\t - Use a "-" symbol to input mulitple indexes in one group: 12 - 14'
    print '\t - Use a "->" symbol to input a range of indexes in one group: 10 -> 20'
    print '\t - Use a combination of symbols to precisely define index groups:'
    print '\t   @ Atto647N-SM-Tail'
    print '\t   # index                                                     group'
    print '\t   32                                                          # 1'
    print '\t   33 - 35 - 40                                                # 2'
    print '\t   130 -> 133                                                  # 3'
    print '\t   99 -> 101 - 140 -> 143                                      # 4'
    print '\n\t Output file format:'
    print '\t -------------------'
    print '\t - The program will output a new .txt file with the correct atom names '
    print '\t   for the inputed indexes, as they are in the .gro files entered'
    print '\t - One atom name per index'
    print '\t - One line per group'
    print '\t - The appropriate header starting with an "@" symbol for each .gro file'
    print '\t - The corresponding output for the previous example will be:'
    print '\t   @ Atto 647N Head'
    print '\t   C22'
    print '\t   H23 C24 O27'
    print '\t   N34 H35 H36'
    print '\t   C20 H201 H202 C45 H451 H452'
    print '\t - An .ndx index file with the proper groups will also be outputed '
    print '\t   for each .gro file entered.'
    print '\n\t To use the program:'
    print '\t -------------------'
    print '\t\t\t python ndxrange list.txt gro1.gro gro2.gro'
    print '\n\t -h : displays the help menu'
    print '\n\t IMPORTANT:'
    print '\t ----------'
    print '\t - Make sure to input as many .gro files as define in the .txt file!'
    print '\t - Make sure to input the .gro files in the same order as in '
    print '\t   the .txt file!'
    print '\t - Make sure to input valid .gro files (format, atom order, etc.)!'
    print '\t - Please do NOT use .gro files of complete systems with a membrane and water'
    print '\t - ONLY use .gro files of the molecules you are working on.'
    print '\t - Spaces DO matter in the .txt file! (for now)'
    print '\t - Do not use any other symbols than "@, #, -, -> spaces, integers" in '
    print '\t   the .txt file!'
    print '\n\t Error codes:'
    print '\t ------------'
    print '\t - 1: No files inputed as argument'
    print '\t - 2: Only 1 file inputed as argument'
    print '\t - 3: First file inputed as argument not a .txt file'
    print '\t - 4: The first file inputed after the .txt file is not a .gro file'
    print '\t - 5: File(s) inputed as argument not found in the directory'
    print '\t - 6: Number of .gro files inputed is not the same as found '
    print '\t      in the .txt file.'
    print '\t - 7: Unrecognized character in the .txt file'
    print '\n\t Errors not handeled (yet):'
    print '\t --------------------------'
    print '\t - Improper .gro format (header missing, indentation problems, etc.)'
    print '\t - Non matching .gro file(s) (wrong order, missing indexes, etc.) '
    print '\n\t Future plans:'
    print '\t -------------'
    print '\t - New flags (verbose, input files, output name, other options, etc.)'
    print '\t - Optimisation'
    print '\t - Debug mode'
    print '\t - Additional functions and options'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


############################### Starting program ###############################

print '\n\t ************************************************************************'
print '\t ***************************** ndxrange v1.2 ****************************'
print '\t ************************************************************************'
print '\n\t This program was written by Edouard Mobarak:'
print '\t - Doctoral Student at the University of Helsinki'
print '\t - Biological Physics and Soft Matter group'
print '\t - Supervised by Dr. Tomasz Rog and Prof. Ilpo Vattulainen'
print '\n\t Version v1.2.1 - 03/08/2016'
print '\t Requires Python v2.7'

################################################################################


################################# Reading files ################################

if len( sys.argv ) > 1 :                                                        # if at least one file has been entered as argument

    arg_array = []                                                              # create an array that stores the arguments

    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()                                                          # start the help section

    if len( sys.argv ) == 2 :                                                   # if only one file has been entered
        print '\n ERROR(2): Only one file has been entered, please enter one list .txt file and at least one .gro file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if not sys.argv[1].endswith('.txt') :                                       # checks if first file entered is a .txt file
        print '\n ERROR(3): First file entered is not a .txt file, please enter one list .txt file and at least one .gro file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a txt file
    else :
        arg_array.append( sys.argv[1] )                                         # if it is, append it to the argument array

    for i in range( 2 ,len( sys.argv ) ) :                                      # checks if the later files are .gro files
        if not sys.argv[i].endswith( '.gro' ) :
            print '\n ERROR(4): file ' + sys.argv[i] + ' is not a .gro file, please enter one list .txt file and at least one .gro file.'
            print ' Use the "-h" flag to get more help.\n'
            sys.exit(1)                                                         # program exits if one of the later file is not a .gro file
        else :                                                                  # if the first file is a .txt file and the later files are .gro files
            arg_array.append( sys.argv[i] )                                     # save the files in the argument array

else :
    print '\n ERROR(1): No file name entered, please enter one list .txt file and at least one .gro file.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\n\t Starting program...'
print '\t ' + str( len( sys.argv )-1 ) + ' files entered...\n'

file_array = []                                                                 # create an array that stores all lines of all files

for i in range( 0 ,len( sys.argv )-1 ) :
    try :                                                                       # error handeling
        filin = open( arg_array[i] )
    except IOError :
        print '\n ERROR(5): Unable to open "' + arg_array[i] + '" file, please check file name or file location.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if file is missing or the name is entered wrong
    else :
        print '\t Reading file "' + arg_array[i] + '"...'
        lines = filin.readlines()                                               # saving every line of every file in the file array
        file_array.append( lines )
        filin.close()
        print '\t\t file "' + arg_array[i] + '" read successfully.'
#    print file_array[i][3]                                                     # verbose

################################################################################


########################### Parsing content of files ###########################

print '\n\t Processing index groups...'

k = 0                                                                           # .gro file counter

for i in range(0, len( file_array[0] ) ) :                                      # for every line in the .txt file
    if ( '@' in file_array[0][i] ) :                                            # if an '@' symbol is see in first position
        k = k + 1                                                               # increment the number of .gro files

if k != len( sys.argv )-2 :                                                     # checks if enough .gro files have been inputed
    print '\n ERROR(6): the ' + sys.argv[1] + ' file features ' + str(k) + ' .gro files, while ' + str( len( sys.argv )-2 ) + ' was (were) inputed to the program, please review your list .txt file and/or input the correct number of .gro files.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exits if the number of .gro files entered is different than the number in the .txt file

line_array = []                                                                 # create an array that will store individual words in individual lines
index_group_array = []                                                          # create an array that will store all the index groups individually for every .gro file
k = -1                                                                          # reset .gro file counter to -1

for i in range( 0, len( file_array[0] ) ) :                                     # for every line in the .txt list file
    line_array.append( file_array[0][i].split() )                               # split the line and store it in the line array
#    print file_array[0][i].split()                                             # verbose

pattern = re.compile( "^[0-9]+$" )                                              # pattern that only recognizes integers (for Error (7))

for i in range( 0, len( line_array ) ) :                                        # for every line of the .txt file

    temp_array = []                                                             # create a temporary array that will insude multiple indexes will be added to the same group

    for j in range( 0, len( line_array[i] ) ) :                                 # for every word of the current .txt file line
#        print line_array[i][j]                                                 # verbose
        if line_array[i][j] == '@' :                                            # if the first character is an '@'
            gro_file_array = []                                                 # create a .gro file array
            index_group_array.append( gro_file_array )                          # append it inside a case of the index group array
            k = k + 1                                                           # increment k

        if line_array[i][0] != '@' and line_array[i][0] != '#' and line_array[i][0] != ' ' :
                                                                                # skip lines starting with '@', '#' and ' '
            if line_array[i][1] == '#' :                                        # if the second character is an '#' (case 1)
                print 'case1 ' + str( line_array[i][0] )                       # verbose
                temp_array.append( str( line_array[i][0] ) )                    # add the only index to the temp array
                break                                                           # skip to next line

            if line_array[i][j] == '#' :                                        # if the program reaches the comment
                break                                                           # skip to next line

            if line_array[i][j] != '@' and line_array[i][j] != '#' and line_array[i][j] != '-' and line_array[i][j] != '->' and line_array[i][j] != ' ' and not pattern.match(str(line_array[i][j])) :
                                                                                # if any other characters than @, #, -, ->, ' ' and integers are used in the .txt file
                print '\n ERROR(7): Unrecognized symbol: "' + line_array[i][j] + '" at line ' + str(i+1) + ' in the ' + sys.argv[1] + ' file. Please do not use unrecognized characters.'
                print ' Use the "-h" flag to get more help.\n'
                sys.exit(1)                                                     # program exits if it finds an unautherized characted in the .txt file

            if line_array[i][j] == '->' :                                       # if a '->' symbol is found (case 2)
#                print 'case 2'                                                 # verbose
                if line_array[i][j-2] == '-' :                                  # if a '-' symbol is found before (case 2a)
#                    print 'case2a ' + str(range( int( line_array[i][j-1] )+1,  
#                        int( line_array[i][j+1] )+1 ) )                        # verbose
                    temp_array.extend( range( int( line_array[i][j-1] )+1,
                        int( line_array[i][j+1] )+1 ) )                         # add the indexes in the range except the first to the temporary array

                elif line_array[i][j+2] == '-' :                                # or else if a '-' symbol is found after (case 2b)
#                    print 'case2b ' + str( range( int( line_array[i][j-1] ),
#                        int( line_array[i][j+1] )+1 ) )                        # verbose
                    temp_array.extend( range( int( line_array[i][j-1] ),
                        int( line_array[i][j+1] )+1 ) )                         # add the indexes in the range to the temporary array

                else :                                                          # or else (case 2c)
#                    print 'case2c ' +str( range( int( line_array[i][j-1] ),
#                        int( line_array[i][j+1] )+1 ) )                        # verbose
                    index_group_array[k].append( range( int( line_array[i][j-1] ),
                    int( line_array[i][j+1] )+1 ) )                             # add the indexes in the range to the group array

            if line_array[i][j] == '-' :                                        # if a '-' symbol is found (case 3)
#                print 'case 3'                                                 # verbose
                if line_array[i][j-2] == '->' :                                 # if a '->' was previously found (case 3a)
#                    print 'case3a ' + str( line_array[i][j+1] )                # verbose
                    temp_array.append( str( line_array[i][j+1] ) )              # add the next index to the temp array

                elif line_array[i][j-2] == '-' :                                # or if a '-' was previously found (case 3b)
#                    print 'case3b ' + str( line_array[i][j+1] )                # verbose
                    temp_array.append( str( line_array[i][j+1] ) )              # get the next index to the temp array

                elif line_array[i][j+2] == '-' :                                # or if a '-' is later found (case 3c)
#                    print 'case3c ' + str( line_array[i][j-1] ) + ' ' + str( line_array[i][j+1] ) # verbose
                    temp_array.extend( ( str( line_array[i][j-1] ),
                        str( line_array[i][j+1] ) ) )                           # add both indexes to the temp array
                else :                                                          # or else if there is nothing is found before or after (case 3d)
#                    print 'case3d ' + str( line_array[i][j-1] ) + ' ' + str( line_array[i][j+1] ) # verbose
                    temp_array.extend( ( str( line_array[i][j-1] ),
                        str( line_array[i][j+1] ) ) )                           # add both indexes to the temp array

    if temp_array != [] :                                                       # if the temporary array is not empty
        index_group_array[k].append( temp_array )                               # append the temp array to the index group array


print '\t\t Index groups processed successfully.'

################################################################################


########################### Preparing output file 1 ############################

filout1 = open( 'atom_name_list.txt', 'w' )                                     # preparing output file 1 = atom names
print '\n\t Preparing output atom name file ...'

################################################################################


################################ Processing data ###############################

print '\n\t Pairing index groups with atom names...'

line_array_2 = []                                                               # declaring a line array
group_number = 0                                                                # declaring a variable to count total group number
atom_number = 0                                                                 # declaring a variable to count total atom number

for i in range( 1, len( file_array ) ) :                                        # for every .gro file entered

    temp_array = []                                                             # resetting the temporary array

    for j in range( 0, len( file_array[i] ) ) :                                 # for every line of individual .gro files
        temp_array.append( file_array[i][j].split() )                           # split each line and append it to the temp array

    line_array_2.append( temp_array )                                           # append the temp array to the line array
#    print line_array_2                                                         # verbose

for i in range( 0, len( index_group_array ) ) :                                 # for every .gro file in the index array

    filout2 = open( 'index_int_' + str(i+1) + '.ndx', 'w' )                     # preparing intermediary index output files
    print '\t     Writing intermediary index file '+ str(i+1) + '...'
#    print index_group_array[i]                                                 # verbose
    filout1.write( '@ ' + sys.argv[i+2] + '\n' )                                # outputting .gro file header in the atom name file

    for j in range (0, len( index_group_array[i] ) ) :                          # for every index group
        filout2.write( '[ Group_' + str(j+1) +' ]\n' )                          # write group names in the index output files
#        print  index_group_array[i][j]                                         # verbose
#        print  'len = ' + str( len( index_group_array[i][j] ) )                # verbose
        if len( index_group_array[i][j] ) == 1 :                                # if the group is comprised of just one index
#            print index_group_array[i][j]                                      # verbose
            for k in range( 0, len( line_array_2[i] ) ) :                       # for every line in the each .gro file
#                print line_array_2[i][k]                                       # verbose

#                print index_group_array[i][j][0]                                # verbose
#                print line_array_2[i][k][2]                                     # verbose
                if len( line_array_2[i][k] ) == 6 and index_group_array[i][j][0] == line_array_2[i][k][2] :
                                                                                # if we are not in the current .gro file header and the index in both the .gro file and the index array are the same
#                    print 'index of gro file = ' + str(i)                      # verbose
#                    print 'index line of txt file = ' + str(j)                 # verbose
#                    print 'index line of gro file = ' + str(k)                 # verbose
#                    print 'index of gro file = ' +  str( index_group_array[i][j] ) # verbose
#                    print 'index found ' + str( line_array_2[i][k][2] )        # verbose
#                    print 'atom name found ' + str( line_array_2[i][k][1] )    # verbose
                    filout1.write( line_array_2[i][k][1] + ' ' )                # outputs atom names in the atom name file
                    filout2.write( line_array_2[i][k][2] + ' ' )                # outputs indexes in the index file
                    atom_number = atom_number + 1                               # increment atom number variable

        else :                                                                  # if the group is comprised of more than one index
#            print index_group_array[i][j]                                      # verbose
            for k in range( 0, len( index_group_array[i][j] ) ) :               # for every index in the index groups of individual files
#                print  index_group_array[i][j][k]                              # verbose
                for l in range (0, len( line_array_2[i] ) ) :                   # for every line in the each .gro file
#                    print line_array_2[i][l]                                   # verbose
#                    print index_group_array[i][j]                              # verbose
                    if line_array_2[i][l] != line_array_2[i][0] and line_array_2[i][l] != line_array_2[i][1] : #and str( index_group_array[i][j][k] ) == str( line_array_2[i][l][2] ) :
                                                                                # if we are not in the current .gro file header and the index in both the .gro file and the index array are the same
#                        print 'index of gro file = ' + str(i)                  # verbose
#                        print 'index line of txt file = ' + str(j)             # verbose
#                        print 'index line of gro file = ' + str(l)             # verbose
#                        print 'index of txt file = ' + str( index_group_array[i][j][k] ) # verbose
                        print 'index found ' + str( line_array_2[i][l][2] )              # verbose
                        print 'atom name found ' + str( line_array_2[i][l][1] ) # verbose
#                        filout1.write( line_array_2[i][l][1] + ' ' )            # outputs atom names in the atom name file
#                        filout2.write( line_array_2[i][l][2] + ' ' )            # outputs indexes in the index file
                        atom_number = atom_number + 1                           # increment atom number variable

        group_number = group_number + 1                                         # increment group number
        filout2.write( '\n' )                                                   # go down a line in the index file
        filout1.write( '\n' )                                                   # jump a line after each index group

    filout1.write( '\n' )                                                       # jump a line after each .gro file
    filout2.close()                                                             # closing file

print '\t Pairing successful.'
print '\n\t ' + str( len( sys.argv )-2 ) + ' intermediary index files written successfully.'

filout1.close()                                                                 # closing file
print '\t Output file "atom_name_list.txt" written successfully.'

################################################################################


########################## Concatenating index files ###########################

print '\n\t Concatenating final indexes...'

os.system( ' touch make_ndx.log' )                                              # create a .log file
os.system( ' echo "\n>" > make_ndx.log' )

for i in range( 2 ,len( sys.argv ) ) :                                          # for every .gro file entered
#    print sys.argv [i]
    os.system( 'echo "q" | make_ndx -f ' + sys.argv [i] + ' -o index_default.ndx >> make_ndx.log 2>/dev/null' )
                                                                                # generates a regular index .ndx file for each gro file
    os.system( 'echo "q" | make_ndx -f ' + sys.argv [i] + ' -n index_int_' + str(i-1) + '.ndx index_default.ndx -o index_final_' + str(i-1) + '.ndx >> make_ndx.log 2>/dev/null' )
                                                                                # concatenates the regular and manually generated index .ndx file
    print '\t\t index ' + str(i-1) + '...'

print '\t Done.\n\t Cleaning up...'
os.system( 'rm -rf \#index_final_*.ndx.* index_default.ndx \#index_default.ndx.* ' ) #index_int_*.ndx
                                                                                # clean up
print '\n\t ' + str( len( sys.argv )-2 ) + ' final index files written successfully.'

################################################################################


################################ Ending program ################################

print '\n\t The program has processed:'
print '\t - atom name .txt files: \t\t\t\t\t1'
print '\t - final index .ndx files: \t\t\t\t\t' + str( len( sys.argv )-2 )
print '\t - log .log files: \t\t\t\t\t\t1'
print '\t - index groups: \t\t\t\t\t\t' + str( group_number )
print '\t - atoms: \t\t\t\t\t\t\t' + str( atom_number )
print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'

################################################################################

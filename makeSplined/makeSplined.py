#!/usr/bin/env python

import sys																			#needed to allow system commands
import numpy as np																	#needed to spline
import scipy as sp																	#needed to spline		
from scipy.interpolate import interp1d												#needed to spline


print '\nWelcome to the density splining program.'

################################################################################

#reading density file
density_file = raw_input( 'Please specify density .xvg file: ' )					#inputing density .xvg file

try :
	filin = open( density_file, 'r' )												#opening the file
except IOError :																	#error if file not found
	print 'Error 1: unable to open "' + density_file + '" file !'
	sys.exit(1)
else :
	lines = filin.readlines()														#saving all lines where they can be accessed like an array
	filin.close()																	#closing the file

################################################################################

#splining the graphs and writing new coordinates
filout = open( 'splined.out', 'w' )													#opening write-only file

j = 0																				#line number variable
while ( '#' in lines[j] ) :															#while we are not at the right section
	j = j + 1																		#incrementing variable
while ( '@' in lines[j] ) :															#while we are in the right header section
	filout.write( lines[j] )														#writing header
	j = j + 1

k = 0																				#column number variable
columns = len( lines[j].split() )													#number of columns in the file
column_list = [ [] for n in xrange( columns ) ]										#list that will carry lists (1 for each column)
for i in range( j, len( lines ) ) :													#for each relavant line
	split_line = lines[i].split()													#splitting it
	for k in range( 0, columns ) :													#for each column in the file
		column_list[k].append( float( split_line[k] ) )								#saving all coords in different lists

xnew = np.linspace( min( column_list[0] ), max( column_list[0] ), 300 )				#create new smooth x axis
#increase last number to make smoother, because we are adding more points

for i in range( 0, len( xnew ) ) :													#for every new x coord
	filout.write( '\t' + str( xnew[i] ) )											#writing new x coords
	for k in range( 1, columns ) :													#for every column
		f = interp1d( column_list[0], column_list[k], kind = 'cubic' )				#interpolating
		ynew = f(xnew[i]).item()													#getting item from object
		filout.write( '\t\t\t' + str( ynew ) )										#writing new y coords
	filout.write( '\n' )

################################################################################
# HOW TO PLOT IN PYTHON #
#########################

#import matplotlib.pyplot as plt														#needed to plot

#f = interp1d( x, y1 )																	#linear curve (example)
#plt.plot( x, y1, 'o', xnew, f(xnew), '-' ,xnew , f1(xnew), '--' )						#plotting xcoord, ycoord, design of curve
#plt.legend( [ 'data', 'linear', 'cubic' ] , loc = 'best' )								#legend
#plt.show()																				#showing

################################################################################

#ending program
filout.close()

print '\nCurves have been successfully splined for the "' + density_file + '" file !'
print 'Please check the "splined.out" file for more details !\n'
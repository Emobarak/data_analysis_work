#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.02
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Move all hydrogens next to the atoms they are bonded to and optionally reorder
atoms as specified on command line.
"""

import re
import os
import sys
import pybel

import openbabel as ob
from optparse import OptionParser


def getFNs(option, opt_str, value, parser):
    """Extract number of parameters from command line.

    """
    reX = re.compile(r'^[0-9]+$')
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if reX.match(arg) is None:
            break
        else:
            value.append(int(arg))
            del rargs[0]
    setattr(parser.values, option.dest, value)

# Parse command line options.
usage="[python3] %prog [[-P] -a atom_indexes] -i file1 -o file2"
description="Move all hydrogens next to the atoms they are bonded to and" \
            " optionally reorder atoms as specified on command line."
version="\n%prog Version 0.02\n\nRequires Python 3."
optParser = OptionParser(usage=usage,
                         version=version,
                         description=description)

optParser.set_defaults(inFN=None, outFN=None, atoms=None, noPin=False)
optParser.add_option('-i', type='str',
                     dest='inFN',
                     help="Input file name [default: %default]")
optParser.add_option('-o', type='str',
                     dest='outFN',
                     help="Output file name [default: %default]")
optParser.add_option('-a', action='callback', callback=getFNs,
                     dest='atoms',
                     help="List of atoms in new order [default: %default]")
optParser.add_option('-P', action='store_true',
                     dest='noPin',
                     help="Reorder only atoms as given with option -a"
                          " [default: %default]")

options, args = optParser.parse_args()


# Check if required parameters are given
if options.inFN is None or options.outFN is None:
    print(optParser.usage)
    sys.exit(1)


# Get input file name and format
fname = options.inFN
fformat = os.path.splitext(fname)[1][1:]

# Read input file
pybmol = next(pybel.readfile(fformat, fname))

mol = pybmol.OBMol


def walkMol(mol):
    """Walk trough all atoms of the molecule and get their indices in order.

    """
    atoms = []
    for atom in ob.OBMolAtomIter(mol):
        # Check if hydrogen
        if atom.IsHydrogen():
            continue
        # Append heavy atom index to list
        atoms.append(atom.GetIdx())
        # Iterate trough bonded neighbors of heavy atom and
        # put hydrogens in list
        for nbr in ob.OBAtomAtomIter(atom):
            if nbr.IsHydrogen():
                atoms.append(nbr.GetIdx())
    return atoms


if options.atoms is not None:
    atoms = options.atoms
    if mol.NumAtoms() != len(options.atoms):
        for i in range(1, mol.NumAtoms()+1):
            if i not in atoms:
                atoms.append(i)
    if not options.noPin:
        mol.RenumberAtoms(atoms)
        atoms = walkMol(mol)
else:
    atoms = walkMol(mol)

# Reorder atoms in molecule
mol.RenumberAtoms(atoms)

# Write out molecule
offormat = os.path.splitext(options.outFN)[1][1:]
pybmol.write(offormat, options.outFN, overwrite=True)

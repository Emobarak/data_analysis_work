set cnt 0
foreach {file}  [ glob  *_molecule.mae] {
    mol load mae $file
    set sel [atomselect $cnt "all" ]
    $sel writepdb $file.pdb
    incr cnt
}

exit 0

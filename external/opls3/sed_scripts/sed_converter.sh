#!/bin/bash


# How to use:
# ./sed_converter.sh  Thrombin_mapper_pv.txt array.txt
# It works only for systems with max 9999 atoms!

# ATOMS + 1
MAPPERFILE=$1
ARRAYFILE=$2



readarray DIST < $ARRAYFILE
NUMBER_ATOM=`echo "${DIST[@]}" | wc -l`
DIST=("" "${DIST[@]}" )


for i in {1..9} ; do
    printf "$i\n"
    printf "${DIST[$i]}"

    #printf "${DIST[$i-1]}\n"
    sed -i  "s/\s[$i]\s/`printf "${DIST[$i]}"`/g" $MAPPERFILE
done



for i in {1..9} ; do
    for j in {0..9} ; do
        printf "$i$j\n"
        printf "${DIST[$i$j]}\n"
        sed -i "s/\s[$i][$j]\s/`printf "${DIST[$i$j]}"`/g" $MAPPERFILE
    done
done



for i in {1..9} ; do
    for j in {0..9} ; do
        for k in {0..9} ; do
            printf "$i$j$k\n"
            printf "${DIST[$i$j$k]}\n"
            sed -i "s/\s[$i][$j][$k]\s/`printf "${DIST[$i$j$k]}"`/g" $MAPPERFILE
        done
    done
done



for i in {1..9} ; do
    for j in {0..9} ; do
        for k in {0..9} ; do
            for l in {0..9} ; do
                if [ $i$j$k$l -eq $NUMBER_ATOM ]
                then
                    exit 1
                fi
                printf "$i$j$k$l\n"
                printf "${DIST[$i$j$k$l]}\n"
                sed -i "s/\s[$i][$j][$k][$l]\s/`printf "${DIST[$i$j$k$l]}"`/g" $MAPPERFILE
            done
        done
    done
done

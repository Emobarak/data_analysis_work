#!/usr/bin/env python


# How to use:
# python mapper.py test.pdb test.txt


import sys
import numpy as np
import re
# Install whith conda :
# conda install -c omnia parmed
from parmed import load_file

# Convert numbers (0-9) to letters (a-l)
def int2str(numbers):
	dic = {
	"0":["a"],
	"1":["b"],
	"2":["c"],
	"3":["d"],
	"4":["e"],
	"5":["f"],
	"6":["g"],
	"7":["h"],
	"8":["i"],
	"9":["l"]
	}
	# Split number in list of digits (string):
	strings_list = [list(str(i)) for i in numbers]

	letters = []
	c=[]
	for i in strings_list:
	    temp=[]
	    for j in i:
	        if j in dic:
	            j=dic.get(j)[0]
	            temp.append(j)
	    letters.append(temp)
	letters = ["".join(i) for i in letters]

	return letters


# Convert letters (a-l) numbers (0-9)
def str2int(letters):
	dic = {
	"a":["0"],
	"b":["1"],
	"c":["2"],
	"d":["3"],
	"e":["4"],
	"f":["5"],
	"g":["6"],
	"h":["7"],
	"i":["8"],
	"l":["9"]
	}
	# Split number in list of digits (string):
	numbers_list = [list(str(i)) for i in letters]

	numbers = []
	for i in numbers_list:
	    temp=[]
	    for j in i:
	        if j in dic:
	            j=dic[j][0]
	            temp.append(j)
	    numbers.append(temp)

	numbers = [int("".join(i)) for i in numbers]

	return numbers


def dictionaryMaker(inputPDB,outputFile):

	# Load PDB/GROMACS FILE
	loadfile = load_file(inputPDB)

	# Declare the total number of atoms
	atoms_number = len(loadfile.atoms)

	# Split atom coordinates in x,y,z
	# Only 3 numbers after the comma are taken in consideration
	x = [ '{:.3f}'.format(i) for i in loadfile.coordinates[:,0] ]
	y = [ '{:.3f}'.format(i) for i in loadfile.coordinates[:,1] ]
	z = [ '{:.3f}'.format(i) for i in loadfile.coordinates[:,2] ]

	# Concatenate x,y,z corrdinates and convert in x_y_z string
	coordinates_list = []
	for i in range(atoms_number):
		coordinates_list.append(str(x[i])+"_"+str(y[i])+"_"+str(z[i]))

	# Create a numpy array
	coordinates_list = np.vstack(coordinates_list)
	print (coordinates_list)



	# Parmed places all the information of the same atom in a string
	# Regular expression are used to catch residue and atom names
	residues_list = []
	atoms_list    = []

	# Serial number of Atoms (From 1 to N )
	serial_list    = []

	for i in range (atoms_number):
		residues_list.append( str(loadfile.atoms[i]).split(";")[1].split(" In ")[1].split(" ")[0] )
		atoms_list.append( str(loadfile.atoms[i]).split(";")[0].split(" [")[0].split("<Atom ")[1])
		serial_list.append(int(i+1))

	# Create numpy arrays
	residues_list = np.array(residues_list)
	atoms_list = np.array(atoms_list)
	serial_list = np.array(serial_list)


	# Parmed places the "CHAIN" information inside the residue list.
	# Each residue in the "loadfile.residues" list has to be iterated
	# for all the atoms inside each residues

	chains_list = []
	residues_number = []

	for i in loadfile.residues:
		for j in i:
			chains_list.append(str(re.findall(r"chain.(.*?);",str(i))).strip("[]\"\'"))
			residues_number.append(int(str(i)[str(i).find("[")+1:str(i).find("]")]))

	# Create numpy arrays
	chains_list 	= np.array(chains_list)
	residues_number = np.array(residues_number)





	# Dictionary Creation: [x_y_z] : [ serial number , atom name , residue name, chain ]
	dictionary = {}

	for i in range(atoms_number):
		dictionary[str(coordinates_list[i])] = [serial_list[i],atoms_list[i] ,residues_list[i], residues_number[i], chains_list[i]]



	# Convert number to letters:
	serial_list      = int2str(serial_list)
	residues_number  = int2str (residues_number)

	np.savetxt (outputFile, np.c_[serial_list, atoms_list ,residues_list, residues_number ,chains_list, x, y, z ] , fmt="%6s%6s%6s%5s%6s%10s%10s%10s\t")

	return dictionary


if __name__ == "__main__":
    dictionaryMaker(sys.argv[1],sys.argv[2])

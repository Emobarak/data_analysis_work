import itertool
import glob
import numpy as np
import argparse


def uniquifyByRows(a):
     #Take a 2D array and return a 2D array without duplicates.
    data = np.ascontiguousarray(a).view(np.dtype((np.void, a.dtype.itemsize * a.shape[1])))
    _, idx = np.unique(data, return_index=True)

    unique_a = a[idx]
    return unique_a




if __name__ == '__main__':


# Import columns:
  c = []
  for file in sorted(glob.glob("*ffnonbonded.itp")):                                                             
      list_str   = np.genfromtxt(file,skip_header=2, dtype=np.str)[:,0] 
      list_nums = np.genfromtxt(file,skip_header=2, dtype=np.float64)[:,-2::]                    
      # Prepare files -> strip number from string C[0-9] -> C
      f = []
      for i in list_str:
        f.append ("".join([i for i in i if not i.isdigit()]))
      b = np.c_[f, list_nums]          
      c.append(b) 

  merged = np.array(list(itertools.chain(*c)))
  uniquifyByRows(merged)






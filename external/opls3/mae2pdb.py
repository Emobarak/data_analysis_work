import re
import sys
import os
import os.path
import pybel
import openbabel as ob
import numpy as np
import glob
import argparse
from sys import platform


def split_file(filepath):
    """Split a file based on a number of lines."""
    path, filename = os.path.split(filepath)
    # filename.split('.') would not work for filenames with more than one .
    basename, ext = os.path.splitext(filename)
    # open input file
    a = []
    with open(filepath, 'r') as f_in:
        for idx, line in enumerate(f_in):
            if idx != 0:
                #It is detecting the starting of the molecule file : "f_m_ct {"
                if line.startswith("f_m_ct {"):
                   a.append(int(idx))

    with open(filepath, 'r') as f_in:
        try:
            # open the first output file
            f_out = open(os.path.join(path, '{}_{}{}'.format("%03d"  %  1, "molecule", ext)), 'w')
            # loop over all lines in the input file, and number them
            for i, line in enumerate(f_in):
                # every time the current line number can be divided by the
                # wanted number of lines, close the output file and open a
                # new one
                for n in range(len(a)):
                    if  i == a[n]:
                        f_out.close()
                        f_out = open(os.path.join(path, '{}_{}{}'.format("%03d"  %  int(n+1), "molecule", ext)), 'w')
                f_out.write(line)

        finally:
            # close the last output file
            f_out.close()


def uniquify(seq):
    """Take a list and return list without duplicates.

    """
    seen = set()
    return [i for i in seq if str(i) not in seen and not seen.add(str(i))]

def list_difference(s1, s2):
    """Take set difference of two lists.

    Return a list of only those elements of the first list which are not in the
    second list.
    """
    s2 = set([str(i) for i in s2])
    return [j for j in s1 if str(j) not in s2]



def getBonds(mol):
    """Get list of bonds in the molecule.

    """
    bonds = []
    for bond in ob.OBMolBondIter(mol):
        a = bond.GetBeginAtomIdx()
        b = bond.GetEndAtomIdx()
        d = bond.GetLength()
        s = [a, b]
        s.sort()
        s.append(d)
        bonds.append(s)
    bonds.sort()

    return bonds


def getAngles(mol):
    """Get list of angles in the molecule.

    """
    angles = []
    for angle in ob.OBMolAngleIter(mol):
        angles.append([angle[1]+1, angle[0]+1, angle[2]+1])
    for angle in angles:
        if angle[0] > angle[2]:
            angle.reverse()
        a = mol.GetAngle(mol.GetAtom(angle[1]),
                         mol.GetAtom(angle[0]),
                         mol.GetAtom(angle[2]))
        angle.append(a)
    angles.sort(key=lambda a: [a[1], a[0], a[2]])

    return angles


def getTorsions(mol):
    """Get list of torsions in the molecule.

    """
    torsions = []
    for torsion in ob.OBMolTorsionIter(mol):
        torsions.append([torsion[0]+1, torsion[1]+1,
                         torsion[2]+1, torsion[3]+1])
    for i in torsions:
        if i[2] < i[1]:
            i.reverse()
        t = mol.GetTorsion(i[0], i[1], i[2], i[3])
        i.append(t)
    torsions.sort(key=lambda a: [a[1], a[2], a[0], a[3]])

    return torsions


def getPairs(torsions, angles, bonds):
    """Get list of 1-4 pairs in the molecule.

    """
    pairs14 = [sorted([i[0], i[3]]) for i in torsions]
    # Filter out duplicate pairs and those in 4 and 5 member rings
    excludel = [[j[0],j[2]] for j in angles]
    excludel.extend(bonds)
    pairs14 = uniquify(list_difference(pairs14, excludel))
    pairs14.sort()

    return pairs14




def AtomListFromPdb(file):
    atoms = np.genfromtxt(file, skip_header=1, skip_footer=1, dtype= np.str)[:,-1]
    return atoms



def printPairs(pairs14):
    print('\n[ pairs ]')
    print(';   ai     aj   fn     c0    c1    c2    c3')
    s = "{0:>6} {1:>6}    1"
    for i in pairs14:
        print(s.format(getAtom(i[0]), getAtom(i[1])))





# Parse command line options
parser = argparse.ArgumentParser(description="Construct functionalized nanoparticle.", prog='"mae2pdb converter')
parser.add_argument('--version', action='version', version='%(prog)s 0.01')
parser.add_argument("-i","--input", dest="mae", type=str,
                    help="Input filename (molecule)")

args = parser.parse_args()


if __name__ == '__main__':

    if not os.path.isfile(args.mae):
        print("Error: mae collection file does not exist: {0}".format(args.mae))
        parser.print_usage()
        sys.exit(2)


    split_file(args.mae)
    # Preparing the files for vmd
    for file in glob.glob ("*molecule.mae"):
        open(file, "r+").write("{\n s_m_m2io_version\n :::\n 2.0.0\n}\n\n" + open(file).read())

    if platform == "linux" or platform == "linux2":
        # Linux
        os.system ("vmd -dispdev text -e mae2pdb.tcl")

    elif platform == "darwin":
        # OS X
        print ("Hi! you have a macintosh, check if the VMD path is correct, if you don't get the PDB files!")
        os.system("/Applications/VMD\ *.app/Contents/vmd/./vmd_MACOSXX86 -dispdev text -e mae2pdb.tcl | echo exit ")


    dct = {
    
    "C"     :     [12.011,    6 ],
    "H"     :     [1.008,       1],
    "BR"  :     [79.904,   35],
    "I"       :     [126.905,   3],
    "N"     :     [14.007,     7],
    "O"     :     [15.999,     8],
    "P"     :     [30.974,   15],
    "CL"   :    [35.453,   17],
    "S"     :     [ 32.065,  16],
    "F"     :     [ 18.998,    9]

    }
    
    
    with open ("corrections.itp", "wb") as f_handle:
    
        for idx,file in enumerate(glob.glob("*_molecule*pdb")):
            list_atoms      = []
            list_MolWeight  = []
            list_AtomicNumber = []
        
            for  numb,atom in enumerate(AtomListFromPdb(file)):
                list_atoms.append(atom+str(numb+1))
                list_MolWeight.append(dct [atom][0])
                list_AtomicNumber.append(dct [atom][1])
            
            # Defining the section header:
            head = "\n\n ; name: %03d\n ;\tname\tat.num\tmass" %  int(idx+1)

            # Collecting the final data:
            data = np.c_[list_atoms, list_AtomicNumber, list_MolWeight]

            ##################################################

            # Get file format from extension and read molecule
            fformat = os.path.splitext(file)[1][1:]
            pymol = next(pybel.readfile(fformat, file))
            mol = pymol.OBMol
        
            # Get index lists
            bonds    = getBonds(mol)
            angles   = getAngles(mol)
            torsions = getTorsions(mol)
        
            # Get 1-4 pair list
            pairs14 = np.array(getPairs(torsions, angles, bonds))
            dataPairs14 = np.c_[pairs14, np.ones(len(pairs14))]

            # Defining the section header:
            headPairs14 = "\n\n[ pairs ]\n;\tai\taj\tfn"

            ##################################################

            #  Writing the final file
            np.savetxt( f_handle, data , delimiter="\t" ,header= head, fmt="%6s", comments=" " )
            np.savetxt( f_handle, dataPairs14 , delimiter="\t" ,header= headPairs14, fmt="%5i", comments=' ' )

    print("Congratulation, the conversion is complete!")


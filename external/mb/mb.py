#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Version 0.05
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Membrane builder. Generate grid of molecules for membrane from GRO files.

Usage:
./mb.py [-x N] [-y N] [-s N] -m list_of_numbers filenames
"""

import re
import sys
import random
import numpy as np

from optparse import OptionParser


class MembraneBuilder:
    def __init__(self):
        """Create and initialize Option Parser.

        """
        def getMolCounts(option, opt_str, value, parser):
            """Extract numbers from command line.

            """
            reX = re.compile(r'^[0-9]+$')
            value = []
            rargs = parser.rargs
            while rargs:
                arg = rargs[0]
                if reX.match(arg) is None:
                    break
                else:
                    value.append(int(arg))
                    del rargs[0]
            setattr(parser.values, option.dest, value)

        usage = '[python] %prog [-x N] [-y N] [-s N]' \
                ' -m list_of_numbers filenames'
        version = '%prog Version 0.05\n\nRequires NumPy.'
        description = 'Generate grid of molecules for membrane ' \
                      'from GRO files. With option "-m" give ' \
                      'how many molecules of each type to use.'
        optParser = OptionParser(usage=usage,
                                 version=version,
                                 description=description)
        optParser.set_defaults(x=8, y=8, squeeze=0.0, nmols=None)
        optParser.add_option('-x', type='int',
                             dest='x',
                             help='Number of molecules in x direction'
                             ' [default: %default].')
        optParser.add_option('-y', type='int',
                             dest='y',
                             help='Number of molecules in y direction'
                             ' [default: %default].')
        optParser.add_option('-s', type='float',
                             dest='squeeze',
                             help='Amount to shorten spacing between'
                             ' molecules in xy plane (nm) [default: %default]')
        optParser.add_option('-m', action='callback',
                             callback=getMolCounts,
                             dest='nmols',
                             help='List of number of molecules to use'
                             ' [default: None]')
        (self.opt, self.args) = optParser.parse_args()

        if (len(self.args) < 1
            or self.opt.nmols is None
            or len(self.args) != len(self.opt.nmols)):
            print(usage)
            sys.exit(1)

        for n in self.opt.nmols:
            if n%2 != 0:
                print("Error: Molecule counts have to be even numbers!")
                sys.exit(2)

        ne = self.opt.x * self.opt.y * 2
        ng = sum(self.opt.nmols)
        if ne != ng:
            s = "Error: {} molecules expected, but {} were given!"
            print(s.format(ne, ng))
            sys.exit(3)

        # Get max dimentions of molecules
        self.stepx, self.stepy, self.stepz = self.get_mol_dimensions()


    def begin(self):
        """The main function doing all the building.

        """
        self.idx = 0

        natoms = self.get_natoms()
        x = self.opt.x
        y = self.opt.y
        nmols = np.array(self.opt.nmols, dtype=int)
        ncomp = len(self.args)

        molMap1 = self.make_mol_map(x, y, ncomp, nmols/2)
        molMap2 = self.make_mol_map(x, y, ncomp, nmols/2)
        molMap = (molMap1, molMap2)

        fn = ""
        for i in self.args:
            fn += "{}_".format(i[:-4])
        fn += "{}".format(ncomp)
        for i in nmols:
            fn += "_{}".format(i)
        fn += "_m.gro"

        with open(fn, 'w') as outGro:
            outGro.writelines("{}\n".format(fn[:-4]))
            outGro.writelines("{}\n".format(natoms))

            for i, name in enumerate(self.args):
                mol = self.readGro(name)[2:-1]
                for j in self.put_in_membrane(molMap, i, mol):
                    outGro.writelines(j)

            s = " {0:9.5f} {1:9.5f} {2:9.5f}\n"
            outGro.writelines(s.format(self.stepx * x,
                                       self.stepy * y,
                                       self.stepz * 4))


    def get_natoms(self):
        """Get final atom count.

        """
        natoms = 0
        for i, name in enumerate(self.args):
            with open(name, 'r') as f:
                f.readline()
                n = int(f.readline().strip())
            natoms += n*self.opt.nmols[i]

        return natoms


    def make_mol_map(self, x, y, ncomp, nmols):
        """Make random bitmap which determines positions of molecule types.

        """
        lcomp = []
        for i in range(ncomp):
            lcomp.append(np.zeros(nmols[i], dtype=int) + i)
        stack = np.hstack(lcomp)
        molMap = np.random.permutation(stack).reshape((x,y))

        print(molMap)
        return molMap


    def get_mol_dimensions(self):
        """Calculate max molecule dimentions.

        """
        stepxy = []
        stepz = []
        rows = []
        for name in self.args:
            with open(name, 'r') as f:
                mol = f.readlines()[2:-1]
            for i in mol:
                row = [float(i[20:28]), float(i[28:36]), float(i[36:44])]
                rows.append(row)
            coo = np.matrix(rows)
            coo = coo.T
            stepxy.append(np.max(coo[0]) - np.min(coo[0]))
            stepxy.append(np.max(coo[1]) - np.min(coo[1]))
            stepz.append(np.max(coo[2])  - np.min(coo[2]))
        stepxy = max(stepxy)
        stepz  = max(stepz)
        stepxy -= self.opt.squeeze

        return stepxy, stepxy, stepz


    def center_membrane(self, mol):
        """Move the molecule so that the membrane is centered in the box.

        """
        translationMatrix = np.matrix([self.stepx / 2.0,
                                       self.stepy / 2.0,
                                       self.stepz * 2.5])
        s = "{0}{1:8.3f}{2:8.3f}{3:8.3f}{4}"
        for n, i in enumerate(mol):
            coo = i[1]
            coo += translationMatrix
            mol[n] = s.format(i[0], coo[0,0], coo[0,1], coo[0,2], i[2])

        return mol


    def put_in_membrane(self, molMap, ID, mol):
        """Put molecule in membrane at positions given by bitmap.

        """
        for i, line in enumerate(mol):
            coo = np.matrix([float(line[20:28]),
                             float(line[28:36]),
                             float(line[36:44])])
            mol[i] = [line[:20], coo, line[44:]]

        membr = []
        for i in self.put_on_grid(molMap[0], ID, mol):
            membr.append(self.center_membrane(i))
        mol = self.flipX(mol)
        for i in self.put_on_grid(molMap[1], ID, mol):
            membr.append(self.center_membrane(i))

        return membr


    def put_on_grid(self, molMap, ID, mol):
        """Move a molecule to designated grid point.

        Also rotate it by random angle around Z axis.
        """
        membr = []
        for i, v in enumerate(molMap):
            for j, z in enumerate(v):
                if z == ID:
                    self.rotateZrand(mol)
                    membr.append(self.translate(mol, i, j))

        return membr


    def readGro(self, groFN):
        """Read in GRO file for a molecule.

        """
        with open(groFN, 'r') as f:
            return f.readlines()


    def rotateZrand(self, mol):
        """Rotate a molecule by random angle around Z axis.

        """
        angle = 2 * np.pi * random.random()
        rotationMatrix = np.matrix([[np.cos(angle), -np.sin(angle), 0],
                                    [np.sin(angle),  np.cos(angle), 0],
                                    [            0,              0, 1]])
        for i in mol:
            i[1] *= rotationMatrix


    def translate(self, mol, x, y):
        """Translate a molecule to a given grid point.

        """
        self.idx += 1
        dx = x * self.stepx
        dy = y * self.stepy
        mol2 = []
        for i in mol:
            coo = i[1].copy()
            coo[0,0] += dx
            coo[0,1] += dy
            mol2.append(["{0:5d}{1}".format(self.idx, i[0][5:]), coo, i[2]])

        return mol2


    def flipX(self, mol):
        """Flip a molecule around X axis.

        Used for building another leaflet of the membrane.
        """
        rotationMatrix = np.matrix([[1,  0,  0],
                                    [0, -1,  0],
                                    [0,  0, -1]])
        mol2 = []
        for i in mol:
            coo = i[1].copy()
            coo[0,2] += self.stepz
            coo *= rotationMatrix
            mol2.append([i[0], coo, i[2]])

        return mol2



if __name__ == '__main__':
    a = MembraneBuilder()
    a.begin()
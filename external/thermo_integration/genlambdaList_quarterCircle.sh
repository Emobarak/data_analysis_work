#! /bin/csh

set nl = 24                                                                     # change the number of windows
#set pi = `echo  -1 | awk '{print atan2(0, -1)}'`

set i = 0

set listlambda = ""

while ($i < $nl)

	set lambda = `echo $nl $i | awk '{printf "%.6f ", 1.0+((cos(atan2(0, -1)/2/($1-1)*$2))-1)}'`
        set listlambda = "$listlambda $lambda"

	@ i++
end                                                                             # generates the lambdas based on a half circle

echo "$listlambda   "                                                           # shows the list of lambdas


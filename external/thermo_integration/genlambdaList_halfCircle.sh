#! /bin/csh

set nl = 36

set i = 0

set listlambda = ""

while ($i < $nl)
	set lambda = `echo $nl $i | awk '{print 0.5*(cos(atan2(0, -1)/($1-1)*$2)+1)}'`
        set listlambda = "$listlambda $lambda"
	@ i++
end

echo "fep-lambda = $listlambda"

#./genlambdaList_halfCircle.sh  | tail -1 | tr " " "\n"  | sort -gk1 | tr "\n" " "

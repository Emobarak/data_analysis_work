#!/bin/bash

for i in `seq 1 10`; do
    cd ./sys$i\_*/thermo_run
#    ls -lrt

    for j in `seq 5 30`; do
#        echo $j
        k=$(expr 1000 \* $j)
#        l=$(expr 1000 \* $[$j-1])
#        echo $l
        gmx bar -f */min.xvg -o bar_$j.xvg -oi barint_$j.xvg -b 5000 -e $k &> gmx_bar_$j.log
#        ls -lrt
        wait
    done

    cd ./../..

done
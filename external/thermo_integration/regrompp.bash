#! /bin/bash 

Nold=36
N=36
old=test8

for i in `seq 0 $((N-1))`; do
	\rm -r $i
	mkdir $i	
	sed "s/init_lambda_state     =  0/init_lambda_state     =  $i/" thermo_int_all.mdp > $i/thermo_int_all.mdp
	olddir=`awk -v Nold=$Nold -v N=$N -v i=$i 'BEGIN {print int(i*Nold/N)}'`
	cd $i 
	gmx grompp -f  thermo_int_all.mdp -c ../../system_1_md_4_5.gro -p ../../system_1.top  -o min.tpr -n ../../sys_1_ndx.ndx -maxwarn 1 -ref ../../system_1_md_4_5.gro
	\rm -rf \#* 
	cd ..
done

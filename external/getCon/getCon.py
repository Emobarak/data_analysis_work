#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.6
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Prints lists of atom indexes for Gromacs topology. Optionally compare these
lists to an existing topology.

Requires Python 3 and Open Babel Python bindings.

Supports all file formats which Open Babel supports, but the bond information
is perceived from atom distances, so the molecule should not be distorted for
bond perception to work correctly.

Usage:
./getCon.py -i molecule.pdb
./getCon.py -c -t molecule.itp -i molecule.pdb
"""

import re
import sys
import os.path

import pybel
import openbabel as ob

from optparse import OptionParser



def optP():
    """Parse command line options.

    """
    usage="[python3] %prog [-cnv] -i structure_file [-t molecule.itp]"
    description="Prints lists of atom indexes for Gromacs topology." \
                " Optionally compare these lists to an existing topology." \
                " Uses Open Babel Python bindings."
    version="\n%prog Version 0.06\n\nRequires Python 3 and" \
            " Open Babel Python bindings."
    optParser = OptionParser(usage=usage,
                             version=version,
                             description=description)
    optParser.set_defaults(itpFN=None, printAtomNames=False, printValues=False,
                           structureFN=None, checkBonding=False)
    optParser.add_option('-i', type='str',
                         dest='structureFN',
                         help="Structure file (in) [default: %default]")
    optParser.add_option('-c', action='store_true',
                         dest='checkBonding',
                         help="Compare topology bonded parameter index lists"
                         " with the provided structure [default: %default]")
    optParser.add_option('-n', action='store_true',
                         dest='printAtomNames',
                         help="Print atom names instead of indexes"
                         " [default: %default]")
    optParser.add_option('-t', type='str',
                         dest='itpFN',
                         help="Topology file (in) [default: %default]")
    optParser.add_option('-v', action='store_true',
                         dest='printValues',
                         help="Print also length and angle values"
                         " [default: %default]")

    options, args = optParser.parse_args()

    # To check bonding topology file is required
    if (options.checkBonding
        and options.itpFN is None):
        print("Error: To check bonding topology is required!")
        sys.exit(1)

    # Print usage info if structure file is not given
    if (options.structureFN is None
        or not os.path.isfile(options.structureFN)):
        print(__doc__)
        sys.exit(1)

    return options


def uniquify(seq):
    """Take a list and return list without duplicates.

    """
    seen = set()
    return [i for i in seq if str(i) not in seen and not seen.add(str(i))]


def list_difference(s1, s2):
    """Take set difference of two lists.

    Return a list of only those elements of the first list which are not in the
    second list.
    """
    s2 = set([str(i) for i in s2])
    return [j for j in s1 if str(j) not in s2]


def getAtomTypes(itpFN):
    """Read atom types from topology.

    """
    # Regular expressions used
    reXa = re.compile(r'^[^;]*\[\s+atoms\s+\]')
    reXc = re.compile(r'^\s*[;]+')
    reXe = re.compile(r'^\s*$')
    reXs = re.compile(r'^[^;]*\[\s+[a-z]*\s+\]')

    # Read topology and get atom types
    atypes = {}
    with open(itpFN, 'r') as f:
        # Find [ atoms ]
        line = f.readline()
        while reXa.match(line) is None:
            line = f.readline()
        # Read atoms
        line = f.readline()
        while reXs.match(line) is None:
            if (reXc.match(line) is not None
                or reXe.match(line) is not None):
                line = f.readline()
                continue
            anr, atype = line.split()[:2]
            atypes[int(anr)] = atype
            line = f.readline()

    return atypes


def getBonds(mol):
    """Get list of bonds in the molecule.

    """
    bonds = []
    for bond in ob.OBMolBondIter(mol):
        a = bond.GetBeginAtomIdx()
        b = bond.GetEndAtomIdx()
        d = bond.GetLength()
        s = [a, b]
        s.sort()
        s.append(d)
        bonds.append(s)
    bonds.sort()

    return bonds


def getAngles(mol):
    """Get list of angles in the molecule.

    """
    angles = []
    for angle in ob.OBMolAngleIter(mol):
        angles.append([angle[1]+1, angle[0]+1, angle[2]+1])
    for angle in angles:
        if angle[0] > angle[2]:
            angle.reverse()
        a = mol.GetAngle(mol.GetAtom(angle[1]),
                         mol.GetAtom(angle[0]),
                         mol.GetAtom(angle[2]))
        angle.append(a)
    angles.sort(key=lambda a: [a[1], a[0], a[2]])

    return angles


def getTorsions(mol):
    """Get list of torsions in the molecule.

    """
    torsions = []
    for torsion in ob.OBMolTorsionIter(mol):
        torsions.append([torsion[0]+1, torsion[1]+1,
                         torsion[2]+1, torsion[3]+1])
    for i in torsions:
        if i[2] < i[1]:
            i.reverse()
        t = mol.GetTorsion(i[0], i[1], i[2], i[3])
        i.append(t)
    torsions.sort(key=lambda a: [a[1], a[2], a[0], a[3]])

    return torsions


def getPairs(torsions, angles, bonds):
    """Get list of 1-4 pairs in the molecule.

    """
    pairs14 = [sorted([i[0], i[3]]) for i in torsions]
    # Filter out duplicate pairs and those in 4 and 5 member rings
    excludel = [[j[0],j[2]] for j in angles]
    excludel.extend(bonds)
    pairs14 = uniquify(list_difference(pairs14, excludel))
    pairs14.sort()

    return pairs14


def getITPLists(f):
    """Get bonded lists from topology.

    """
    reXb = re.compile(r'^[^;]*\[\s+bonds\s+\]')
    reXp = re.compile(r'^[^;]*\[\s+pairs\s+\]')
    reXa = re.compile(r'^[^;]*\[\s+angles\s+\]')
    reXd = re.compile(r'^[^;]*\[\s+dihedrals\s+\]')
    reXc = re.compile(r'^\s*[;]+')
    reXe = re.compile(r'^\s*$')
    reXs = re.compile(r'^[^;]*\[\s+[a-z]*\s+\]')

    bonds     = []
    pairs14   = []
    angles    = []
    torsions  = []
    impropers = []

    # Bonds
    f.seek(0)
    line = f.readline()
    while (reXb.match(line) is None
           and line != ''):
        line = f.readline()

    line = f.readline()
    while (reXs.match(line) is None
           and line != ''):
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        a, b = line.split()[:2]
        bonds.append(sorted([int(a), int(b)]))
        line = f.readline()
    bonds.sort()

    # 1-4 pairs
    f.seek(0)
    line = f.readline()
    while (reXp.match(line) is None
           and line != ''):
        line = f.readline()

    line = f.readline()
    while (reXs.match(line) is None
           and line != ''):
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        a, b = line.split()[:2]
        pairs14.append(sorted([int(a), int(b)]))
        line = f.readline()
    pairs14.sort()

    # Angles
    f.seek(0)
    line = f.readline()
    while (reXa.match(line) is None
           and line != ''):
        line = f.readline()

    line = f.readline()
    while (reXs.match(line) is None
           and line != ''):
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        a, b, c = line.split()[:3]
        angles.append([int(a), int(b), int(c)])
        line = f.readline()
    for i in angles:
        if i[2] < i[0]:
            i.reverse()
    angles.sort(key=lambda a: [a[1], a[0], a[2]])

    # Dihedrals
    f.seek(0)
    line = f.readline()
    while (reXd.match(line) is None
           and line != ''):
        line = f.readline()

    line = f.readline()
    while (reXs.match(line) is None
           and line != ''):
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        a, b, c, d = line.split()[:4]
        torsions.append([int(a), int(b), int(c), int(d)])
        line = f.readline()
    for i in torsions:
        if i[2] < i[1]:
            i.reverse()
    torsions.sort(key=lambda a: [a[1], a[2], a[0], a[3]])

    # Improper dihedrals
    line = f.readline()
    while (reXd.match(line) is None
           and line != ''):
        line = f.readline()

    line = f.readline()
    while (reXs.match(line) is None
           and line != ''):
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        a, b, c, d = line.split()[:4]
        impropers.append([int(a), int(b), int(c), int(d)])
        line = f.readline()
    impropers.sort()

    return bonds, pairs14, angles, torsions, impropers


def getAtom(i):
    if not options.printAtomNames:
        return i
    else:
        if options.itpFN is None:
            atom = mol.GetAtom(i)
            return atom.GetResidue().GetAtomID(atom).strip()
        else:
            return atypes[i]


def printLists(options, bonds, pairs14, angles, torsions):
    print('\n[ bonds ]')
    print(';   ai     aj   fn     c0    c1    c2    c3')
    s = "{0:>6} {1:>6}    1"
    if options.printValues:
        s += " {2:8.3f}"
    for i in bonds:
        print(s.format(getAtom(i[0]), getAtom(i[1]), i[2]))

    print('\n[ pairs ]')
    print(';   ai     aj   fn     c0    c1    c2    c3')
    s = "{0:>6} {1:>6}    1"
    for i in pairs14:
        print(s.format(getAtom(i[0]), getAtom(i[1])))

    print('\n[ angles ]')
    print(';   ai     aj     ak   fn     c0    c1    c2    c3')
    s = "{0:>6} {1:>6} {2:>6}    1"
    if options.printValues:
        s += " {3:8.3f}"
    for i in angles:
        print(s.format(getAtom(i[0]), getAtom(i[1]), getAtom(i[2]), i[3]))

    print('\n[ dihedrals ]')
    print(';   ai     aj     ak     al   fn    c0    c1    c2    c3    c4    c5')
    s = "{0:>6} {1:>6} {2:>6} {3:>6}    3"
    if options.printValues:
        s += " {4:8.3f}"
    for i in torsions:
        print(s.format(getAtom(i[0]), getAtom(i[1]),
                       getAtom(i[2]), getAtom(i[3]),
                       i[4]))

    # Print summary
    print("\nBonds: " + str(len(bonds)))
    print("Pairs 1-4: " + str(len(pairs14)))
    print("Angles: " + str(len(angles)))
    print("Torsions: " + str(len(torsions)))


def getDiff(lmol, litp):
    """Compare given bonded lists.

    """
    isDifferent = False
    if len(lmol) > len(litp):
        for i in range(len(lmol) - len(litp)):
            litp.append("    ")
    elif len(lmol) < len(litp):
        for i in range(len(litp) - len(lmol)):
            lmol.append("    ")
    s  = ""
    ss = ""
    for i in range(len(litp[0])):
        s += "{0[" + str(i) + "]:>6} "
        ss += " " * 7
    s.rstrip()
    ss = "    " + "Topology" + ss  + "Molecule\n"
    for i, j in zip(lmol, litp):
        if i != j:
            isDifferent = True
            ss += s.format(j)
            ss += " " * 4
            ss += s.format(i)
            ss += "\n"

    return isDifferent, ss


if __name__ == "__main__":
    # Parse command line options
    options = optP()

    # Get file format from extension and read molecule
    fformat = os.path.splitext(options.structureFN)[1][1:]
    pymol = next(pybel.readfile(fformat, options.structureFN))

    mol = pymol.OBMol

    # Get index lists
    bonds    = getBonds(mol)
    angles   = getAngles(mol)
    torsions = getTorsions(mol)

    # Get 1-4 pair list
    pairs14 = getPairs(torsions, angles, bonds)

    if options.checkBonding:
        # Leave only atom indexes in the lists
        for i in bonds, angles, torsions:
            for j in i:
                del j[-1]
        # Read topology
        with open(options.itpFN, 'r') as f:
            tbonds, tpairs14, tangles, ttorsions, timpropers = getITPLists(f)
        # Compare the lists
        diff = False
        ln = ["bonds", "pairs14", "angles", "dihedrals"]
        for n, ll in zip(ln, ((bonds,    tbonds),
                              (pairs14,  tpairs14),
                              (angles,   tangles),
                              (torsions, ttorsions))):
            isDifferent, ss = getDiff(ll[0], ll[1])
            print(n)
            if isDifferent:
                print(ss)
            if isDifferent:
                diff = True
                print("Warning: Some differences found!")
                print("Check the topology and provided structure!")
        if not diff:
            print("Bonded lists are the same in topology and in molecule.")
        sys.exit(0)

    # Get atom types
    if options.itpFN is not None:
        atypes = getAtomTypes(options.itpFN)

    # Print the lists
    printLists(options, bonds, pairs14, angles, torsions)
#!/usr/bin/env python

"""
Call from terminal:
./lipids_around_dye.py inputfile.gro outputfile.dat

inputfile.gro
    the structure file of the membrane
outputfile.dat
    contains a list of residues of lipids around the dye

Program searches the lipid residues of the membrane that are
around the dye lipid between the given inner and outer radius

(spherical volume so may also contain lipids from the other
leaflet, so when using the output with make_ndx make sure
there is a index file for one leaflet to use)

--------------------------------------------------------------
   WHAT NEEDS TO BE CHECKED BEFORE RUNNING THE SCRIPT:
--------------------------------------------------------------

1) calculate separately 'around' and 'not around' the dye
   depending which one is in order_parameter_around_dye.sh
   -> change condition for u.select_atoms to either 'around'
      or 'not around'

   -> will select phosphorus atoms within a given radius of
      the dye residue
   -> the phosphorus will represent the lipid it is part of

--------------------------------------------------------------
"""

import sys                                                                      # import needed modules
import MDAnalysis
import re

MDAnalysis.core.flags['use_periodic_selections'] = True                         # to use pbc in finding the atoms
MDAnalysis.core.flags['use_KDTree_routines'] = False

script_name = sys.argv[0]                                                       # read in the filenames
infile = sys.argv[1]
outfile = sys.argv[2]

u = MDAnalysis.Universe(infile)                                                 # load structure

print                                                                           # write to terminal
print ' Program lipids_around_dye.py '                                          # write to terminal


resnum_dye = raw_input(' Residue index of the dye  > ')                         # residue index of the dye as a string
print ' Give the inner and outer radius around the dye in Angstrom '            # write to terminal
inner_r = raw_input(' Inner radius  > ')                                        # inner radius
outer_r = raw_input(' Outer radius   > ')                                       # outer radius

phosphorus_outer = u.select_atoms('name P8 and around '+str(outer_r)+' resid '+str(resnum_dye))# AtomGroup of lipids within (or not within) the outer radius
phosphorus_inner = u.select_atoms('name P8 and around '+str(inner_r)+' resid '+str(resnum_dye))# AtomGroup of lipids within the inner radius

lipids_list_outer = list(phosphorus_outer.residues)                             # list of residues within the outer radius
lipids_list_inner = list(phosphorus_inner.residues)                             # list of residues within the inner radius

for value in lipids_list_inner:                                                 # compare inner and outer radius atoms and remove
    if value in lipids_list_outer:                                              # from outer lipids list to get the lipids inside
        lipids_list_outer.remove(value)                                         # these ring segments

output1 = open(outfile,'w')
for line in lipids_list_outer:                                                  # write all residues to an output file
    line_string = str(line)                                                     # split the line with < or space or > to get a list
    splitted_line = re.split('<| |>', line_string)                              # like ['', 'Residue', 'DOPC,', '25', '']
    output1.write(splitted_line[3] + '\n')                                      # take only the number so splitted_line[3]

output1.close                                                                   # close output

print ' Found ' +str(len(lipids_list_outer))+ ' lipids.'                        # write to terminal
print ' Wrote the residue numbers to ' +outfile+ ' !'                           # write to terminal
print



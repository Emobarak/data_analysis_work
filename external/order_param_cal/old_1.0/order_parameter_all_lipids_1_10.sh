#!/bin/bash

# Script for calculating the order parameters for all lipids in systems 1-10

# Ld systems order parameter calculation separately for saturated and
# unsaturated carbons (DOPC double bonds)
# -> results combined with process_unsaturated.py

# Lo systems order parameter calculation only for saturated carbons (SMPC
# only single bonds)

# ----------------------------------------------------------------------------
#          WHAT NEEDS TO BE CHECKED BEFORE RUNNING THE SCRIPT:
# ----------------------------------------------------------------------------

# 1) create correct structure of the folders:
#
#    order_parameter_all_lipids.sh
#
#    order_parameter_all_lipids/       (output folder)
#
#    system_"$i"/
#        system_"$i"_run.tpr
#        system_"$i"_run_lastframe.gro
#        system_"$i"_center_traj.xtc
#        sn1_all_lipids.ndx
#        sn1_all_lipids_unsat.ndx
#        sn2_all_lipids.ndx
#        sn2_all_lipids_unsat.ndx

# 2) check the system numbers in ld_array and lo_array

# 3) for gmx order modify options -b and -e to match the system trajectory
#    -> fixed systems -b 0 and -e 200000
#    -> original ones -b 200000

# ----------------------------------------------------------------------------

ld_array=( 1 2 5 6 9 )                                                          # systems without cholesterol (ld)
lo_array=( 3 4 7 8 10 )                                                         # systems with cholesterol (lo)

for i in "${ld_array[@]}"
do
    # calculate the order parameter for sn1 saturated and unsaturated
    gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -n system_"$i"/sn1_all_lipids.ndx -od order_parameter_all_lipids/"$i"_deuter_sn1_all_lipids.xvg -d z -b 200000

    gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -n system_"$i"/sn1_all_lipids_unsat.ndx -od order_parameter_all_lipids/"$i"_deuter_sn1_all_lipids_unsat.xvg -unsat -d z -b 200000

    # calculate the order parameter for sn2 saturated and unsaturated
    gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -n system_"$i"/sn2_all_lipids.ndx -od order_parameter_all_lipids/"$i"_deuter_sn2_all_lipids.xvg -d z -b 200000

    gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -n system_"$i"/sn2_all_lipids_unsat.ndx -od order_parameter_all_lipids/"$i"_deuter_sn2_all_lipids_unsat.xvg -unsat -d z -b 200000
done


for i in "${lo_array[@]}"
do
    # calculate the order parameter for sn1
    gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -n system_"$i"/sn1_all_lipids.ndx -od order_parameter_all_lipids/"$i"_deuter_sn1_all_lipids.xvg -d z -b 200000

    # calculate the order parameter for sn2
    gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -n system_"$i"/sn2_all_lipids.ndx -od order_parameter_all_lipids/"$i"_deuter_sn2_all_lipids.xvg -d z -b 200000
done


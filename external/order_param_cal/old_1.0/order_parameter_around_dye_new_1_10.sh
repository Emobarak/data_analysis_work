#!/bin/bash
rm -rf lipids_around.log                                                                           # remove old .log file
touch lipids_around.log                                                                            # create new .log file

# Script for calculating the order parameters with errors for membrane lipids
# around or not around a specific radius of the dye

# NEW: calculates only the average order parameter of all dyes in one system

# What the script does:
#   - for each dye in each system it finds the lipids around (or not around)
#     a given radius of the dye and calculates the order parameter for those lipids
#   - the trajectory is sliced and the lipid search and order parameter calculation
#     done separately for each slice
#   - the resulting order parameters for each carbon are average values over all slices

# ------------------------------------------------------------------------------------------
#                 WHAT NEEDS TO BE CHECKED BEFORE RUNNING THE SCRIPT:
# ------------------------------------------------------------------------------------------

# 1) create correct structure of the folders:
#
#    order_parameter_around_dye.sh
#    lipids_around_dye.py
#    process_unsaturated.py
#
#    order_parameter_around_dye/
#        sys_"$i"/                     (output folder for each system i)
#
#    system_"$i"/
#        system_"$i"_run.tpr
#        system_"$i"_run_lastframe.gro
#        system_"$i"/sys*_rep*_tot.xtc
#        sn1_upper_lipids.ndx
#        sn2_upper_lipids.ndx
#        sn1_lower_lipids.ndx
#        sn2_lower_lipids.ndx
#        (+ _unsat.ndx files if needed)

# 2) calculate separately for cases 'around' and 'not around' the dye
#    around dye:      change x=_
#                     also lipids_around_dye.py condition needs to be 'around'
#    not around dye:  change x=_not_
#                     also lipids_around_dye.py condition needs to be 'not around'


# 3) check the system numbers in ld_array and lo_array

# 4) check dye_residues_i lists according to dye residue numbers in each system
#    -> for this also modify the if-statements in the first for loop to match system numbers
#    -> also modify the for loop values for leaflet like 'upper1' and 'upper2' if more dyes
#    -> in the membrane than just two
#    -> for systems 1-10 should be same for all systems

# 5) check the values ld_amount_groups and lo_amount_groups to match the numbers of
#    groups in the index files for sn1, sn2 and unsaturated

# 6) change the radii around which to find the lipids (inner_radii, outer_radii)
#    -> also update last_index to match the last index of radii lists

# 7) change the dump times (begintime, endtime) according to the systems

# 8) for more precise results (and thus longer computing time) change the value of
#    timeslice to get more slices of the trajectory

# ------------------------------------------------------------------------------------------

ld_array=( 1 2 5 6 9 )                                                                             # systems without cholesterol
lo_array=( 3 4 7 8 10 )                                                                            # systems with cholesterol

dye_residues=( 125 126 127 128 )                                                                   # residues of the dyes in each system


ld_amount_groups=( 18 18 4 )                                                                       # how many groups is there in the index file of the lipid chains
lo_amount_groups=( 14 16 )                                                                         # ( atoms_sn1 atoms_sn2 unsaturated ) for ld and lo membranes

inner_radii=( 0 )                                                                                  # inner radii for lipid search around the dye in Angstrom ( 0 8 16 )
outer_radii=( 8 )                                                                                  # outer radii for lipid search around the dye in Angstrom ( 8 16 24 )

last_index=0                                                                                       # change to match the last index of radii arrays !

x=_                                                                                                # change to '_' or '_not_' depending if calculating for around or not around the radius
                                                                                                   # if x changed change also the condition for lipid search in lipids_around_dye.py

echo "**** Lipids $x around dye ****" >> lipids_around.log                                         # write to .log file

firsttime=200000                                                                                   # beginning time of last 200 ns of trajectory
endtime=389500                                                                                     # ending time of trajectory -10000 !
timeslice=10000                                                                                    # how often updates the lipids around the dye

# LD SYSTEMS                                                                                       # FOR LOOP ld systems begins

for i in "${ld_array[@]}"; do

    echo "  " >> lipids_around.log                                                                 # write to .log file
    echo "system $i"  >> lipids_around.log                                                         # write to .log file
    echo "  " >> lipids_around.log                                                                 # write to .log file

    for r in $(seq 0 $last_index); do                                                              # FOR LOOP all radii begins

        inner_r=${inner_radii[$r]}                                                                 # inner radius for lipid search around the dye
        outer_r=${outer_radii[$r]}                                                                 # outer radius for lipid search around the dye

        echo " " >> lipids_around.log                                                              # write to .log file
        echo "radii from $inner_r to $outer_r"  >> lipids_around.log                               # write to .log file

        #remove old
        rm -rf ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn*"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg
        # create new output file sn1, output of all dyes
        touch ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg
        # create new output file sn2, output of all dyes
        touch ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg


        for dye in upper1 upper2 lower1 lower2; do                                                 # FOR LOOP all four dyes (named with upper and lower) begins

            if [ "$dye" == "upper1" ]; then
                leaflet="upper"                                                                    # upper or lower leaflet to use in index files
                k=0                                                                                # index number for "upper" dye for later use
            fi
            if [ "$dye" == "upper2" ]; then
                leaflet="upper"                                                                    # upper or lower leaflet to use in index files
                k=1                                                                                # index number for "lower" dye for later use
            fi
            if [ "$dye" == "lower1" ]; then
                leaflet="lower"                                                                    # upper or lower leaflet to use in index files
                k=2                                                                                # index number for "upper" dye for later use
            fi
            if [ "$dye" == "lower2" ]; then
                leaflet="lower"                                                                    # upper or lower leaflet to use in index files
                k=3                                                                                # index number for "lower" dye for later use
            fi

            touch system_"$i"/residues_dye_"$dye"_"$time".dat                                             # create temporary lipid residue output file

            echo " " >> lipids_around.log                                                          # write to .log file
            echo "** $dye dye" >> lipids_around.log                                                # write to .log file

            dye_resid=${dye_residues[$k]}                                                          # to get the correct lipid residue number from the array

            time=$firsttime                                                                        # starting time for the while loop

            while_index=0                                                                          # how many times while loop has gone through


            while [[ $time -le $endtime ]]; do                                                     # loop over slices of trajectory

                echo "time $time" >> lipids_around.log                                             # write to .log file

                # dump the .gro file of current time in while loop
                echo 0 0 | gmx trjconv -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -o system_"$i"/system_dump_"$dye"_"$time".gro -dump $time

                # get only the timeslice long trajectory of the slice
                echo 0 0 | gmx trjconv -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -o system_"$i"/system_slice_"$dye"_"$time".xtc -b $time -e $(( $time + $timeslice ))

                # call the program lipids_around_dye.py to find the lipids around the dye
                ./lipids_around_dye.py system_"$i"/system_dump_"$dye"_"$time".gro system_"$i"/residues_dye_"$dye"_"$time".dat << EOF
$dye_resid
$inner_r
$outer_r
EOF
                residue_array=()                                                                   # read the output file that contains the residue numbers
                index=0
                for line in `cat system_"$i"/residues_dye_"$dye"_"$time".dat`; do                                 # now line is the residue number
                    residue_array[$index]=$line                                                    # save the line to array
                    let "index += 1"
                done
                echo "found $index lipids" >> lipids_around.log                                    # write to .log file
                residues='ri'
                for value in "${residue_array[@]}"; do                                             # make a string to contain the residues for make_ndx like ri 214|215|218
                    residues+=$value
                    residues+='|ri'
                done

                nr_groups_sn1=${ld_amount_groups[0]}                                               # read in numbers of groups for sn1
                nr_groups_sn2=${ld_amount_groups[1]}                                               # read in numbers of groups for sn2
                nr_groups_unsat=${ld_amount_groups[2]}                                             # read in numbers of groups for unsaturated

                # make the index groups for saturated carbons sn1
                gmx make_ndx -f system_"$i"/system_dump_"$dye"_"$time".gro -n system_"$i"/sn1_"$leaflet"_lipids.ndx -o system_"$i"/sn1_around_dye_slice_"$dye"_"$time".ndx << EOF
$residues
name $nr_groups_sn1 around_dye
0&$nr_groups_sn1
1&$nr_groups_sn1
2&$nr_groups_sn1
3&$nr_groups_sn1
4&$nr_groups_sn1
5&$nr_groups_sn1
6&$nr_groups_sn1
7&$nr_groups_sn1
8&$nr_groups_sn1
9&$nr_groups_sn1
10&$nr_groups_sn1
11&$nr_groups_sn1
12&$nr_groups_sn1
13&$nr_groups_sn1
14&$nr_groups_sn1
15&$nr_groups_sn1
16&$nr_groups_sn1
17&$nr_groups_sn1
del 0-$nr_groups_sn1

q
EOF
                # make the index groups for unsaturated carbons (double bonds) sn1
                gmx make_ndx -f system_"$i"/system_dump_"$dye"_"$time".gro -n system_"$i"/sn1_"$leaflet"_lipids_unsat.ndx -o system_"$i"/sn1_around_dye_slice_"$dye"_"$time"_unsat.ndx << EOF
$residues
name $nr_groups_unsat around_dye
0&$nr_groups_unsat
1&$nr_groups_unsat
2&$nr_groups_unsat
3&$nr_groups_unsat
del 0-$nr_groups_unsat

q
EOF
                # make the index groups for saturated carbons sn2
                gmx make_ndx -f system_"$i"/system_dump_"$dye"_"$time".gro -n system_"$i"/sn2_"$leaflet"_lipids.ndx -o system_"$i"/sn2_around_dye_slice_"$dye"_"$time".ndx << EOF
$residues
name $nr_groups_sn2 around_dye
0&$nr_groups_sn2
1&$nr_groups_sn2
2&$nr_groups_sn2
3&$nr_groups_sn2
4&$nr_groups_sn2
5&$nr_groups_sn2
6&$nr_groups_sn2
7&$nr_groups_sn2
8&$nr_groups_sn2
9&$nr_groups_sn2
10&$nr_groups_sn2
11&$nr_groups_sn2
12&$nr_groups_sn2
13&$nr_groups_sn2
14&$nr_groups_sn2
15&$nr_groups_sn2
16&$nr_groups_sn2
17&$nr_groups_sn2
del 0-$nr_groups_sn2

q
EOF
                # make the index groups for unsaturated carbons (double bonds) sn2
                gmx make_ndx -f system_"$i"/system_dump_"$dye"_"$time".gro -n system_"$i"/sn2_"$leaflet"_lipids_unsat.ndx -o system_"$i"/sn2_around_dye_slice_"$dye"_"$time"_unsat.ndx << EOF
$residues
name $nr_groups_unsat around_dye
0&$nr_groups_unsat
1&$nr_groups_unsat
2&$nr_groups_unsat
3&$nr_groups_unsat
del 0-$nr_groups_unsat

q
EOF
                # calculate the order parameter for sn1 saturated and unsaturated
                gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/system_slice_"$dye"_"$time".xtc -n system_"$i"/sn1_around_dye_slice_"$dye"_"$time".ndx -od ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time".xvg -d z

                gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/system_slice_"$dye"_"$time".xtc -n system_"$i"/sn1_around_dye_slice_"$dye"_"$time"_unsat.ndx -od ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time"_unsat.xvg -d z -unsat

                # combine the files for saturated and unsaturated sn1
                ./process_unsaturated.py order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time".xvg order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time"_unsat.xvg order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time"_final.xvg

               # calculate the order parameter for sn2 saturated and unsaturated
                gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/system_slice_"$dye"_"$time".xtc -n system_"$i"/sn2_around_dye_slice_"$dye"_"$time".ndx -od ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time".xvg -d z

                gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/system_slice_"$dye"_"$time".xtc -n system_"$i"/sn2_around_dye_slice_"$dye"_"$time"_unsat.ndx -od ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time"_unsat.xvg -d z -unsat

                # combine the files for saturated and unsaturated sn2
                ./process_unsaturated.py order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time".xvg order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time"_unsat.xvg order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time"_final.xvg


                # write a line to the output file that contains the time and the order parameter value for each carbon in sn1
                outputline="$time  "
                awk '!/#|@/{print $2}' order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time"_final.xvg > order_parameter_around_dye/sys_"$i"/temp.xvg
                index=0
                for line in `cat order_parameter_around_dye/sys_"$i"/temp.xvg`; do
                    outputline+=" $line "
                done
                echo $outputline >> ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg


                # write a line to the output file that contains the time and the order parameter value for each carbon in sn2
                outputline="$time  "
                awk '!/#|@/{print $2}' order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time"_final.xvg > order_parameter_around_dye/sys_"$i"/temp2.xvg
                index=0
                for line in `cat order_parameter_around_dye/sys_"$i"/temp2.xvg`; do
                    outputline+=" $line "
                done
                echo $outputline >> ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg

                let "time += timeslice"                                                            # add timeslice of time to current time for next dump
                let "while_index += 1"                                                             # update number of while loops done

                rm -rf ./order_parameter_around_dye/sys_"$i"/temp*                                 # remove temporary .xvg files
                #rm -rf system_"$i"/system_dump.gro                                                 # remove dumped .gro file
                #rm -rf system_"$i"/system_slice.xtc                                                # remove trajectory of slice
                #rm -rf system_"$i"/sn*_around_dye_slice*                                           # remove .ndx file of slice
                #rm -rf system_"$i"/residues_dye.dat                                                # remove residue list file
                #rm -rf order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn*_slice*                  # remove .xvg of the slice

                rm -rf system_"$i"/\#*                                                             # clean up
                rm -rf order_parameter_around_dye/sys_"$i"/\#*                                     # clean up
                rm -rf \#order*                                                                    # clean up
                rm -rf order.xvg                                                                   # clean up

            done                                                                                   # WHILE LOOP ends

        done                                                                                       # leaflet groups do loop ends



        # remove old average files
        rm -rf ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg
        rm -rf ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg

        # calculate the average of order parameter for sn1

        # gmx analyze takes the output.xvg and calculates average for each carbon
        gmx analyze -f  ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg | grep 'SS[1-9]' > ./order_parameter_around_dye/sys_"$i"/output1.xvg
        # some modification to get only number of carbon, average value and standard error of the mean
        awk '{ gsub("SS",""); print $1 "  " $2 "  "  $4 }' ./order_parameter_around_dye/sys_"$i"/output1.xvg > ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg


        # calculate the average of order parameter for sn2

        # gmx analyze takes the output.xvg and calculates average for each carbon
        gmx analyze -f  ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg | grep 'SS[1-9]' > ./order_parameter_around_dye/sys_"$i"/output2.xvg
        # some modification to get only number of carbon, average value and standard error of the mean
        awk '{ gsub("SS",""); print $1 "  " $2 "  "  $4 }' ./order_parameter_around_dye/sys_"$i"/output2.xvg > ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg

        # remove temporary output files
        rm -rf ./order_parameter_around_dye/sys_"$i"/output*.xvg

    done                                                                                           # all radii for loop ends

done                                                                                               # lo systems for loop ends



# LO SYSTEMS still modify

for i in "${lo_array[@]}"; do                                                                      # FOR LOOP lo systems with chol begins

    echo "  " >> lipids_around.log                                                                 # write to .log file
    echo "system $i"  >> lipids_around.log                                                         # write to .log file
    echo "  " >> lipids_around.log                                                                 # write to .log file

    for r in $(seq 0 $last_index); do                                                              # FOR LOOP all radii begins

        inner_r=${inner_radii[$r]}                                                                 # inner radius for lipid search around the dye
        outer_r=${outer_radii[$r]}                                                                 # outer radius for lipid search around the dye

            echo " " >> lipids_around.log                                                          # write to .log file
            echo "radii from $inner_r to $outer_r"  >> lipids_around.log                           # write to .log file

            #remove old
            rm -rf ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn*"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg
            # create new output file sn1
            touch ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg
            # create new output file sn2
            touch ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg

            for dye in upper1 upper2 lower1 lower2; do                                             # FOR LOOP all four dyes (named with upper and lower) begins ]

                if [ "$dye" == "upper1" ]; then
                    leaflet="upper"                                                                # upper or lower leaflet to use in index files
                    k=0                                                                            # index number for "upper" dye for later use
                fi
                if [ "$dye" == "upper2" ]; then
                    leaflet="upper"                                                                # upper or lower leaflet to use in index files
                    k=1                                                                            # index number for "lower" dye for later use
                fi
                if [ "$dye" == "lower1" ]; then
                    leaflet="lower"                                                                # upper or lower leaflet to use in index files
                    k=2                                                                            # index number for "upper" dye for later use
                fi
                if [ "$dye" == "lower2" ]; then
                    leaflet="lower"                                                                # upper or lower leaflet to use in index files
                    k=3                                                                            # index number for "lower" dye for later use
                fi

            touch system_"$i"/residues_dye_"$dye"_"$time".dat                                                     # create temporary lipid residue output file

            echo " " >> lipids_around.log                                                          # write to .log file
            echo "** $dye dye" >> lipids_around.log                                                 # write to .log file

            dye_resid=${dye_residues[$k]}                                                          # to get the correct lipid residue number from the array

            time=$firsttime                                                                        # starting time for the while loop

            while_index=0                                                                          # how many times while loop has gone through


            while [[ $time -le $endtime ]]; do                                                     # loop over 10ns slices of trajectory

                echo "time $time" >> lipids_around.log

                # dump the .gro file of current time in while loop
                echo 0 0 | gmx trjconv -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -o system_"$i"/system_dump_"$dye"_"$time".gro -dump $time

                # get only the timeslice long trajectory of the slice
                echo 0 0 | gmx trjconv -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/sys*_rep*_tot.xtc -o system_"$i"/system_slice_"$dye"_"$time".xtc -b $time -e $(( $time + 10000 ))

                # call the program lipids_around_dye.py to find the lipids around the dye
                ./lipids_around_dye.py system_"$i"/system_dump_"$dye"_"$time".gro system_"$i"/residues_dye_"$dye"_"$time".dat << EOF
$dye_resid
$inner_r
$outer_r
EOF
                residue_array=()                                                                   # read the output file that contains the residue numbers
                index=0
                for line in `cat system_"$i"/residues_dye_"$dye"_"$time".dat`; do                                 # now line is the residue number
                    residue_array[$index]=$line                                                    # save the line to array
                    let "index += 1"
                done
                echo "found $index lipids" >> lipids_around.log                                    # write to .log file
                residues='ri'
                for value in "${residue_array[@]}"; do                                             # make a string to contain the residues for make_ndx like ri 214|215|218
                    residues+=$value
                    residues+='|ri'
                done

                nr_groups_sn1=${lo_amount_groups[0]}                                               # read in numbers of groups for sn1
                nr_groups_sn2=${lo_amount_groups[1]}                                               # read in numbers of groups for sn2

                # make the index groups for saturated carbons sn1
                gmx make_ndx -f system_"$i"/system_dump_"$dye"_"$time".gro -n system_"$i"/sn1_"$leaflet"_lipids.ndx -o system_"$i"/sn1_around_dye_slice_"$dye"_"$time".ndx << EOF
$residues
name $nr_groups_sn1 around_dye
0&$nr_groups_sn1
1&$nr_groups_sn1
2&$nr_groups_sn1
3&$nr_groups_sn1
4&$nr_groups_sn1
5&$nr_groups_sn1
6&$nr_groups_sn1
7&$nr_groups_sn1
8&$nr_groups_sn1
9&$nr_groups_sn1
10&$nr_groups_sn1
11&$nr_groups_sn1
12&$nr_groups_sn1
13&$nr_groups_sn1
del 0-$nr_groups_sn1

q
EOF
                # make the index groups for saturated carbons sn2
                gmx make_ndx -f system_"$i"/system_dump_"$dye"_"$time".gro -n system_"$i"/sn2_"$leaflet"_lipids.ndx -o system_"$i"/sn2_around_dye_slice_"$dye"_"$time".ndx << EOF
$residues
name $nr_groups_sn2 around_dye
0&$nr_groups_sn2
1&$nr_groups_sn2
2&$nr_groups_sn2
3&$nr_groups_sn2
4&$nr_groups_sn2
5&$nr_groups_sn2
6&$nr_groups_sn2
7&$nr_groups_sn2
8&$nr_groups_sn2
9&$nr_groups_sn2
10&$nr_groups_sn2
11&$nr_groups_sn2
12&$nr_groups_sn2
13&$nr_groups_sn2
14&$nr_groups_sn2
15&$nr_groups_sn2
del 0-$nr_groups_sn2

q
EOF
                # calculate the order parameter for sn1
                gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/system_slice_"$dye"_"$time".xtc -n system_"$i"/sn1_around_dye_slice_"$dye"_"$time".ndx -od ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time".xvg -d z

                # calculate the order parameter for sn2
                gmx order -s system_"$i"/sys*_rep*_400ns.tpr -f system_"$i"/system_slice_"$dye"_"$time".xtc -n system_"$i"/sn2_around_dye_slice_"$dye"_"$time".ndx -od ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time".xvg -d z

                # write to the output file that contains the time and the order parameter value for each carbon in sn1
                outputline="$time  "
                awk '!/#|@/{print $2}' order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1_slice_"$dye"_"$time".xvg > order_parameter_around_dye/sys_"$i"/temp.xvg
                index=0
                for line in `cat order_parameter_around_dye/sys_"$i"/temp.xvg`; do
                    outputline+=" $line "
                done
                echo $outputline >> ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg


                # write to the output file that contais the time and the order parameter value for each carbon in sn2
                outputline="$time  "
                awk '!/#|@/{print $2}' order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2_slice_"$dye"_"$time".xvg > order_parameter_around_dye/sys_"$i"/temp2.xvg
                index=0
                for line in `cat order_parameter_around_dye/sys_"$i"/temp2.xvg`; do
                    outputline+=" $line "
                done
                echo $outputline >> ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg


                let "time += timeslice"                                                            # add 10 ns to current time for next dump
                let "while_index += 1"                                                             # update number of while loops done

                rm -rf ./order_parameter_around_dye/sys_"$i"/temp*                                 # remove temporary .xvg files
                #rm -rf system_"$i"/system_dump.gro                                                 # remove dumped .gro file
                #rm -rf system_"$i"/system_slice.xtc                                                # remove trajectory of slice
                #rm -rf system_"$i"/sn*_around_dye_slice*                                           # remove .ndx file of slice
                #rm -rf system_"$i"/residues_dye.dat                                                # remove residue list file
                #rm -rf order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn*_slice*                  # remove .xvg of the slice

                rm -rf system_"$i"/\#*                                                             # clean up
                rm -rf order_parameter_around_dye/sys_"$i"/\#*                                     # clean up
                rm -rf \#order*                                                                    # clean up
                rm -rf order.xvg                                                                   # clean up


            done                                                                                   # WHILE LOOP ends

        done                                                                                       # leaflet groups do loop ends

        # remove old average files
        rm -rf ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg
        rm -rf ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg

        # calculate the average of order parameter for sn1

        # gmx analyze takes the output.xvg and calculates average for each carbon
        gmx analyze -f  ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg | grep 'SS[1-9]' > ./order_parameter_around_dye/sys_"$i"/output1.xvg
        # some modification to get only number of carbon, average value and standard error of the mean
        awk '{ gsub("SS",""); print $1 "  " $2 "  "  $4 }' ./order_parameter_around_dye/sys_"$i"/output1.xvg > ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn1"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg


        # calculate the average of order parameter for sn2

        # gmx analyze takes the output.xvg and calculates average for each carbon
        gmx analyze -f  ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_output.xvg | grep 'SS[1-9]' > ./order_parameter_around_dye/sys_"$i"/output2.xvg
        # some modification to get only number of carbon, average value and standard error of the mean
        awk '{ gsub("SS",""); print $1 "  " $2 "  "  $4 }' ./order_parameter_around_dye/sys_"$i"/output2.xvg > ./order_parameter_around_dye/sys_"$i"/"$i"_deuter_sn2"$x"around_dye_"$inner_r"_"$outer_r"_ave_error.xvg

        # remove temporary output files
        #rm -rf ./order_parameter_around_dye/sys_"$i"/output*.xvg                                # remove temporary output file


    done                                                                                            # all radii for loop ends

done                                                                                                # ld systems for loop ends






























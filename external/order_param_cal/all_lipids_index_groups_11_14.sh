#!/bin/bash

# Script for finding the lipids in the sn1 and sn2 tails
# for order parameter analysis for systems 1-10

ld_array=( 13 )                                                              # systems without cholesterol (ld)
lo_array=( 14 )                                                              # systems with cholesterol (lo)


# LD SYSTEMS (double bonds in lipids -> unsaturated .ndx)

for i in "${ld_array[@]}"                                                       # FOR LOOP ld systems begins
do

    # SYSTEM $i sn1 saturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn1_all_lipids.ndx << EOF
aC35 & 3
aC36 & 3
aC37 & 3
aC38 & 3
aC39 & 3
aC40 & 3
aC41 & 3
aC42 & 3
aC43 & 3
aC44 & 3
aC45 & 3
aC46 & 3
aC47 & 3
aC48 & 3
aC49 & 3
aC50 & 3
aC51 & 3
aC52 & 3
del 0-14

q
EOF


    # SYSTEM $i sn1 unsaturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn1_all_lipids_unsat.ndx << EOF
aC42 & 3
aC43 & 3
aC44 & 3
aC45 & 3
del 0-14

q
EOF


    # SYSTEM $i sn2 saturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn2_all_lipids.ndx << EOF
aC15 & 3
aC17 & 3
aC18 & 3
aC19 & 3
aC20 & 3
aC21 & 3
aC22 & 3
aC23 & 3
aC24 & 3
aC25 & 3
aC26 & 3
aC27 & 3
aC28 & 3
aC29 & 3
aC30 & 3
aC31 & 3
aC32 & 3
aC33 & 3
del 0-14

q
EOF

    # SYSTEM $i sn2 unsaturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn2_all_lipids_unsat.ndx << EOF
aC23 & 3
aC24 & 3
aC25 & 3
aC26 & 3
del 0-14

q
EOF

done



# LO SYSTEMS (no double bonds in lipids)

for i in "${lo_array[@]}"                                                                           # FOR LOOP lo systems begins
do

    # SYSTEM $i sn1
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn1_all_lipids.ndx << EOF
aC18 & 4
aC19 & 4
aC20 & 4
aC21 & 4
aC22 & 4
aC23 & 4
aC24 & 4
aC25 & 4
aC26 & 4
aC27 & 4
aC28 & 4
aC29 & 4
aC30 & 4
aC31 & 4
del 0-16

q
EOF

    # SYSTEM $i sn2
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn2_all_lipids.ndx << EOF
aC32 & 4
aC36 & 4
aC37 & 4
aC38 & 4
aC39 & 4
aC40 & 4
aC41 & 4
aC42 & 4
aC43 & 4
aC44 & 4
aC45 & 4
aC46 & 4
aC47 & 4
aC48 & 4
aC49 & 4
aC50 & 4
del 0-16

q
EOF

done



#!/usr/bin/env python

"""
Call from terminal: 
./process_unsaturated.py input_sat.xvg input_unsat.xvg outputfile.xvg

input_sat.xvg
    order parameter .xvg file with all carbons of the chain

input_unsat.xvg 
    order parameter .xvg file with only the values for unsaturated 
    carbons got with gmx order and -unsat option

outputfile.xvg
    output .xvg with unsaturated values 

Program outputs a final .xvg file where the order parameters for carbon 
atoms of the double bond are replaced with correct ones from 
gmx order -unsat calculations

-----------------------------------------------------------------------
    WHAT NEEDS TO BE CHECKED BEFORE RUNNING THE SCRIPT:
-----------------------------------------------------------------------

1) change the indexes in saturated_carbons array to match the indexes 
   of unsaturated carbons in the lipid tail
   - calculation of indexes begins from 1 from beginning of the tail 
     in the headgroup side of the chain

-----------------------------------------------------------------------  
"""

import numpy as np                                                                                 # import needed modules
import sys

script_name = sys.argv[0]                                                                          # read in the filenames
infile_sat = sys.argv[1]
infile_unsat = sys.argv[2]
outfile = sys.argv[3]

print '**********'                                                                                 # write to terminal
print '  Program ' +script_name                                                                    # write to terminal
print

input_sat = open(infile_sat,'r')                                                                   # open file for reading
input_unsat = open(infile_unsat,'r')                                                               # open file for reading

saturated_carbons = [9, 10]                                                                        # indexes of unsaturated carbons when calculated from 
                                                                                                   # the beginning of the tail starting from headgroup

nr_lineskips_sat = 0
headerlines = '# ' +infile_sat+' and ' +infile_unsat+ ' combined by ' +script_name+ '\n# \n'       # initializing the headerlines to print to output

for line in input_sat:                                                                             # go through saturated input
    if line.startswith('#'):                                                                       # skip all comment lines
        nr_lineskips_sat += 1
    if line.startswith('@'):                                                                       # add @ comment lines to header
        nr_lineskips_sat += 1
        headerlines += line

input_sat.close

data_sat = np.loadtxt(infile_sat, skiprows=nr_lineskips_sat)                                       # load data of saturated carbons

nr_lineskips_unsat = 0
for line2 in input_unsat:                                                                          # go through unsaturated input
    if line2.startswith('#'):                                                                      # skip all comment lines
        nr_lineskips_unsat += 1
    if line2.startswith('@'):                                                                      # skip all commnet lines
        nr_lineskips_unsat += 1

input_unsat.close

data_unsat = np.loadtxt(infile_unsat, skiprows=nr_lineskips_unsat)                                 # load data of unsaturated carbons

index = 0
for carbon_index in saturated_carbons:                                                             # replace the order parameter value 
    # -2 in below for also taking into account the shifting when order parameter not calculated for 1st or last carbon
    data_sat[carbon_index-2,1] = data_unsat[index,1]                                               # index-1 because python starts indexing from 0
    index += 1                                                                                   
    print '  Replaced carbon ' +str(carbon_index)+ ' in ' +infile_sat                              # write to terminal
    print ' with unsaturated one from ' +infile_unsat                                              # write to terminal
    
# shift also the carbon numbers by 1 when order parameter not calculated for 1st or last carbon in gmx order
for i in range(0,14):
    data_sat[i,0] = i+2  

np.savetxt(outfile, data_sat, fmt='%10f', header=headerlines, comments='' )                        # write to final .xvg output file

print                                                                                         
print '  combined ' +infile_sat+ ' and ' +infile_unsat                                             # write to terminal
print '  to ' +outfile                                                                             # write to terminal
print '**********'                                                                                 # write to terminal


     

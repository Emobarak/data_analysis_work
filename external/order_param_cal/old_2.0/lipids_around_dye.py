#!/usr/bin/env python

"""


Call from terminal: 
./lipids_around_dye.py inputfile.gro outputfile.dat
  > residue number of dye
  > inner radius
  > upper radius 
  > system number
  > either '_' or '_not_' depending whether to find lipids around or not around the dye

inputfile.gro
    the structure file of the membrane
outputfile.dat 
    contains a list of residues of lipids around the dye

Program searches the lipid residues of the membrane that are 
around the dye between the given inner and outer radius

--------------------------------------------------------------
   WHAT NEEDS TO BE CHECKED BEFORE RUNNING THE SCRIPT:
--------------------------------------------------------------

--------------------------------------------------------------
"""

import sys                                                                                         # import needed modules
import MDAnalysis 
import re  
import numpy as np
import math

MDAnalysis.core.flags['use_periodic_selections'] = True                                            # to use pbc in finding the atoms
MDAnalysis.core.flags['use_KDTree_routines'] = False

script_name = sys.argv[0]                                                                          # read in the filenames
infile = sys.argv[1]
outfile = sys.argv[2]

u = MDAnalysis.Universe(infile)                                                                    # load structure

print                                                                                              # write to terminal
print ' Program lipids_around_dye.py '                                                             # write to terminal


resnum_dye = raw_input(' Residue index of the dye  > ')                                            # residue index of the dye as a string
print ' Give the inner and outer radius around the dye in Angstrom '                               # write to terminal
inner_r = raw_input(' Inner radius  > ')                                                           # inner radius
outer_r = raw_input(' Outer radius   > ')                                                          # outer radius
sys_num = raw_input(' System number   > ')                                                         # number of the system for deciding COM calculation
x = raw_input(' "_" if around, "_not_" if not around   > ')                                        # value of x as in bash script

# -------------- OLD --------------
#phosphorus_outer = u.select_atoms('name P8 and around '+str(outer_r)+' resid '+str(resnum_dye))    # AtomGroup of lipids within the outer radius
#phosphorus_inner = u.select_atoms('name P8 and around '+str(inner_r)+' resid '+str(resnum_dye))    # AtomGroup of lipids within the inner radius

#lipids_list_outer = list(phosphorus_outer.residues)                                                # list of residues within the outer radius
#lipids_list_inner = list(phosphorus_inner.residues)                                                # list of residues within the inner radius

#for value in lipids_list_inner:                                                                    # compare inner and outer radius atoms and remove 
#    if value in lipids_list_outer:                                                                 # from outer lipids list to get the lipids inside 
#        lipids_list_outer.remove(value)                                                            # these ring segments

#lipids_list = lipids_list_outer
# ---------------------------------


# -------------- NEW --------------
# checks for each lipid if its COM (heavy atoms) is within given radius from dye on xy-plane 

sys_num = int(sys_num)

# extract the box vectors from the gro file
with open(infile) as file:
    last_line = file.readlines()[-1]
vectors = []
for s in last_line.split():
    vectors.append(float(s)*10.0)
print(vectors)
     
lipids_list = []                                                                                   # lipids around the dye

ld_systems = [1,2,5,6,9,11,13]
lo_systems = [3,4,7,8,10,12,14]


# calculate center of mass of the lipid containing the dye
# - different atom selection for all different dyes 
if sys_num in [1,3]:
    dye_atoms = u.select_atoms('resid '+str(resnum_dye)+' and name N29 C30 C31 O311 O312 O313 O314 P31 C32 C33 O34 C34 C35 C36 C37 C38 C39 C40 C41 C42 C43 C44 C45 C46 C47 C48 N33 C49 O49 C50 C51 C52 C53 C54 C55 C56 C57 C58 C59 C60') 

if sys_num in [2,4]:
    dye_atoms = u.select_atoms('resid '+str(resnum_dye)+' and name O40 C40 C41 C42 C43 C44 C45 C46 C47 C48 C49 C50 C51 C52 C53 C54 C55')
        
if sys_num in [6,8]:
    dye_atoms = u.select_atoms('resid '+str(resnum_dye)+' and name O50 C50 C51 C52 C53 C54 C55 C56 C57 C58 C59 C60 C61 C62 C63 C64 C65')

if sys_num in [5,7]:
    dye_atoms = u.select_atoms('resid '+str(resnum_dye)+' and name N42 C43 C44 O441 P44 O442 O443 O444 O47 C45 C46 C47 C48 C49 C50 C51 C52 C53 C54 C55 C56 C57 C58 C59 C60 C61 N46 C62 O62 C63 C64 C65 C66 C67 C68 C69 C70 C71 C72 C73 ')

if sys_num in [9,10,11,12,13,14]:
    dye_atoms = u.select_atoms('resid '+str(resnum_dye)+' and name N49 C50 C51 O51 P52 O521 O522 O523 C53 C54 C55 C56 C57 C58 C59 C60 C61 C62 C63 C64 C65 C66 C67 C68 C69 O55 N70 O71 C71 C72 C73 C74 C75 C76 C77 C78 C79 C80 C81 C82')

print(len(dye_atoms))
dye_com = dye_atoms.center_of_mass()                                                                # center of mass of the dye, [x y z] array 

    

zlimit_up = dye_com[2]+1.0
zlimit_down = dye_com[2]-1.0

# get the residue numbers of the membrane lipids for the upcoming for loops
if sys_num in ld_systems:
    memb_lipids = u.select_atoms('resname DOPC and prop z < '+str(zlimit_up)+' and prop z > '+str(zlimit_down) )
    lipid_list = list(memb_lipids.residues)
if sys_num in lo_systems:
    memb_lipids = u.select_atoms('resname SMPC and prop z < '+str(zlimit_up)+' and prop z > '+str(zlimit_down) ) 
    lipid_list = list(memb_lipids.residues)


memb_lipids_list = []
for line in lipid_list:                                                                          
    line_string = str(line)                                                                        
    splitted_line = re.split('<| |>', line_string)                                                
    memb_lipids_list.append(int(splitted_line[3]))                                                      
print(memb_lipids_list)

# LD systems (DOPC only)
if sys_num in ld_systems:
    all_lipids = np.linspace(memb_lipids_list[0], memb_lipids_list[-1], len(memb_lipids_list))
    for resnum_lipid in range(memb_lipids_list[0],memb_lipids_list[-1]):
        lipid_atoms = u.select_atoms('resid '+str(resnum_lipid)+' and name CA CB CC CD CE N O7 P8 O9 O10 O11 CF C13 O14 C15 C16 C17 C18 C19 C20 C21 C22 C23 C24 C25 C26 C27 C28 C29 C30 C31 C32 C33 O33 O35 C34 C35 C36 C37 C38 C39 C40 C41 C42 C43 C44 C45 C46 C47 C48 C49 C50') 
           
        lipid_com = lipid_atoms.center_of_mass()
        #print(' ')
        #print(lipid_com)
        # check if the dye is on the edge of box and change the COM of lipids due to PBC
        if dye_com[0] <= float(outer_r):                                     # dye close to x_min
            if lipid_com[0] > vectors[0] - (float(outer_r)-dye_com[0]):
                lipid_com[0] = lipid_com[0] - vectors[0]
                
        if dye_com[0] >= vectors[0]-float(outer_r):                          # dye close to x_max
            if lipid_com[0] < float(outer_r)-abs(vectors[0]-dye_com[0]):
                lipid_com[0] = lipid_com[0] + vectors[0] 
                
        if dye_com[1] <= float(outer_r):                                     # dye close to y_min
            if lipid_com[1] > vectors[1] - (float(outer_r)-dye_com[1]):
                lipid_com[1] = lipid_com[1] - vectors[1] 
                
        if dye_com[1] >= vectors[0]-float(outer_r):                          # dye close to y_max
            if lipid_com[1] < float(outer_r)-abs(vectors[1]-dye_com[1]):
                lipid_com[1] = lipid_com[1] + vectors[1] 
        #print(lipid_com)
        #print(' ')
        dx = dye_com[0]-lipid_com[0]
        dy = dye_com[1]-lipid_com[1]
        dz = abs(dye_com[2]-lipid_com[2])
        d = math.sqrt(math.pow(dx, 2)+math.pow(dy, 2))                                              # distance between COM dye and COM lipid
        if d < float(outer_r) and d >= float(inner_r) and dz <= 10.0:                               # check if within given radii
            lipids_list.append(resnum_lipid)                                                        # add to list of lipids around dye
            all_lipids = all_lipids[all_lipids!=resnum_lipid]                                       # remove from list of lipids not around dye
            print(resnum_lipid)
            print(lipid_com)
            print(dye_com)
            print(d)        


# LO systems (SMPC, CHO)
if sys_num in lo_systems:
    all_lipids = np.linspace(memb_lipids_list[0], memb_lipids_list[-1], len(memb_lipids_list))
    for resnum_lipid in range(memb_lipids_list[0],memb_lipids_list[-1]):
        lipid_atoms = u.select_atoms('resid '+str(resnum_lipid)+' and name CA CB CC CD CE N O7 P8 O9 O10 O11 CF C13 C14 OSM C17 C18 C19 C20 C21 C22 C23 C24 C25 C26 C27 C28 C29 C30 C31 N C32 033 C36 C37 C38 C39 C40 C41 C42 C43 C44 C45 C46 C47 C48 C49 C50') 
        
        lipid_com = lipid_atoms.center_of_mass()
        #print(' ')
        #print(lipid_com)
        # check if the dye is on the edge of box and change the COM of lipids due to PBC
        if dye_com[0] <= float(outer_r):                                     # dye close to x_min
            if lipid_com[0] > vectors[0] - (float(outer_r)-dye_com[0]):
                lipid_com[0] = lipid_com[0] - vectors[0]
                
        if dye_com[0] >= vectors[0]-float(outer_r):                          # dye close to x_max
            if lipid_com[0] < float(outer_r)-abs(vectors[0]-dye_com[0]):
                lipid_com[0] = lipid_com[0] + vectors[0] 
                
        if dye_com[1] <= float(outer_r):                                     # dye close to y_min
            if lipid_com[1] > vectors[1] - (float(outer_r)-dye_com[1]):
                lipid_com[1] = lipid_com[1] - vectors[1] 
                
        if dye_com[1] >= vectors[0]-float(outer_r):                          # dye close to y_max
            if lipid_com[1] < float(outer_r)-abs(vectors[1]-dye_com[1]):
                lipid_com[1] = lipid_com[1] + vectors[1] 
        #print(lipid_com)
        #print(' ')
        dx = dye_com[0]-lipid_com[0]
        dy = dye_com[1]-lipid_com[1]
        dz = abs(dye_com[2]-lipid_com[2])
        d = math.sqrt(math.pow(dx, 2)+math.pow(dy, 2))                                              # distance between COM dye and COM lipid
        if d < float(outer_r) and d >= float(inner_r) and dz <= 10.0:                               # check if within given radii
            lipids_list.append(resnum_lipid)                                                        # add to list of lipids around dye
            all_lipids = all_lipids[all_lipids!=resnum_lipid]                                       # remove from list of lipids not around dye
            print(resnum_lipid)
            print(lipid_com)
            print(dye_com)
            print(d)

    
if x == '_not_':
    lipids_list = all_lipids.astype(int)                                                            # to get all the lipids that are not around the dye
    
    
    
# ---------------------------------   
    

# write all residues to an output file
output1 = open(outfile,'w')
for line in lipids_list:                                                                           # write all residues to an output file
#    line_string = str(line)                                                                        # split the line with < or space or > to get a list 
#    splitted_line = re.split('<| |>', line_string)                                                 # like ['', 'Residue', 'DOPC,', '25', '']
#    output1.write(splitted_line[3] + '\n')                                                         # take only the number so splitted_line[3]
     output1.write(str(line) + '\n')    
 
output1.close      

print ' Found ' +str(len(lipids_list))+ ' lipids.'                                           # write to terminal
print ' Wrote the residue numbers to ' +outfile+ ' !'                                              # write to terminal
print



#!/bin/bash

# Script for finding the lipids in the sn1 and sn2 tails
# for order parameter analysis for systems 1-10

ld_array=( 1 2 5 6 9 )                                                                             # systems without cholesterol (ld)
lo_array=( 3 4 7 8 10 )                                                                            # systems with cholesterol (lo)




# LD SYSTEMS (double bonds in lipids -> unsaturated .ndx)

for i in "${ld_array[@]}"                                                                          # FOR LOOP ld systems begins
do

    # SYSTEM $i sn1 saturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn1_all_lipids.ndx << EOF
aC35 & 2
aC36 & 2
aC37 & 2
aC38 & 2
aC39 & 2
aC40 & 2
aC41 & 2
aC42 & 2
aC43 & 2
aC44 & 2
aC45 & 2
aC46 & 2
aC47 & 2
aC48 & 2
aC49 & 2
aC50 & 2
aC51 & 2
aC52 & 2
del 0-14

q
EOF


    # SYSTEM $i sn1 unsaturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn1_all_lipids_unsat.ndx << EOF
aC42 & 2
aC43 & 2
aC44 & 2
aC45 & 2
del 0-14

q
EOF


    # SYSTEM $i sn2 saturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn2_all_lipids.ndx << EOF
aC15 & 2
aC17 & 2
aC18 & 2
aC19 & 2
aC20 & 2
aC21 & 2
aC22 & 2
aC23 & 2
aC24 & 2
aC25 & 2
aC26 & 2
aC27 & 2
aC28 & 2
aC29 & 2
aC30 & 2
aC31 & 2
aC32 & 2
aC33 & 2
del 0-14

q
EOF

    # SYSTEM $i sn2 unsaturated
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn2_all_lipids_unsat.ndx << EOF
aC23 & 2
aC24 & 2
aC25 & 2
aC26 & 2
del 0-14

q
EOF

done



# LO SYSTEMS (no double bonds in lipids)

for i in "${lo_array[@]}"                                                                           # FOR LOOP lo systems begins
do

    # SYSTEM $i sn1
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn1_all_lipids.ndx << EOF
aC18 & 3
aC19 & 3
aC20 & 3
aC21 & 3
aC22 & 3
aC23 & 3
aC24 & 3
aC25 & 3
aC26 & 3
aC27 & 3
aC28 & 3
aC29 & 3
aC30 & 3
aC31 & 3
del 0-16

q
EOF

    # SYSTEM $i sn2
    gmx make_ndx -f system_"$i"/sys"$i"_rep*_400ns.gro -n system_"$i"/index.ndx -o system_"$i"/sn2_all_lipids.ndx << EOF
aC32 & 3
aC36 & 3
aC37 & 3
aC38 & 3
aC39 & 3
aC40 & 3
aC41 & 3
aC42 & 3
aC43 & 3
aC44 & 3
aC45 & 3
aC46 & 3
aC47 & 3
aC48 & 3
aC49 & 3
aC50 & 3
del 0-16

q
EOF

done



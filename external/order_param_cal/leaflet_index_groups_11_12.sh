#!/bin/bash

# Script for finding the lipids in upper and
# lower leaflet for systems 1-10

ld_array=( 11 )                                                                             # systems without cholesterol (ld)
lo_array=( 12 )                                                                            # systems with cholesterol (lo)


for leaflet in upper lower                                                                         # FOR LOOP leaflets begins
do

    # LD SYSTEMS

    for i in "${ld_array[@]}"                                                                      # FOR LOOP ld systems begins
    do

        if [ "$leaflet" == "upper" ]; then
            residues="ri 1-143"
        fi

        if [ "$leaflet" == "lower" ]; then
            residues="ri 144-286"
        fi

        # SYSTEM $i $leaflet sn1 saturated
        gmx make_ndx -f system_"$i"/sys*_rep*_400ns.gro -n system_"$i"/sn1_all_lipids.ndx -o system_"$i"/sn1_"$leaflet"_lipids.ndx << EOF
$residues
0&18
1&18
2&18
3&18
4&18
5&18
6&18
7&18
8&18
9&18
10&18
11&18
12&18
13&18
14&18
15&18
16&18
17&18
del 0-18

q
EOF
        # SYSTEM $i $leaflet sn1 unsaturated
        gmx make_ndx -f system_"$i"/sys*_rep*_400ns.gro -n system_"$i"/sn1_all_lipids_unsat.ndx -o system_"$i"/sn1_"$leaflet"_lipids_unsat.ndx << EOF
$residues
0&4
1&4
2&4
3&4
del 0-4

q
EOF
        # SYSTEM $i $leaflet sn2 saturated
        gmx make_ndx -f system_"$i"/sys*_rep*_400ns.gro -n system_"$i"/sn2_all_lipids.ndx -o system_"$i"/sn2_"$leaflet"_lipids.ndx << EOF
$residues
0&18
1&18
2&18
3&18
4&18
5&18
6&18
7&18
8&18
9&18
10&18
11&18
12&18
13&18
14&18
15&18
16&18
17&18
del 0-18

q
EOF
        # SYSTEM $i $leaflet sn2 unsaturated
        gmx make_ndx -f system_"$i"/sys*_rep*_400ns.gro -n system_"$i"/sn2_all_lipids_unsat.ndx -o system_"$i"/sn2_"$leaflet"_lipids_unsat.ndx << EOF
$residues
0&4
1&4
2&4
3&4
del 0-4

q
EOF

    done                                                                                           # FOR LOOP ld systems ends


    # LO SYSTEMS

    for i in "${lo_array[@]}"                                                                      # FOR LOOP lo systems begins
    do
       # tähän lo systeemit kun tietää mitkä residuet on SMPC (kun cho ei oteta mukaan!)
        if [ "$leaflet" == "upper" ]; then
            residues="ri 1-94"
        fi

        if [ "$leaflet" == "lower" ]; then
            residues="ri 95-188"
        fi


        # sys $i $leaflet sn1
        gmx make_ndx -f system_"$i"/sys*_rep*_400ns.gro -n system_"$i"/sn1_all_lipids.ndx -o system_"$i"/sn1_"$leaflet"_lipids.ndx << EOF
$residues
0&14
1&14
2&14
3&14
4&14
5&14
6&14
7&14
8&14
9&14
10&14
11&14
12&14
13&14
del 0-14

q
EOF

        # sys $i $leaflet sn2
        gmx make_ndx -f system_"$i"/sys*_rep*_400ns.gro -n system_"$i"/sn2_all_lipids.ndx -o system_"$i"/sn2_"$leaflet"_lipids.ndx << EOF
$residues
0&16
1&16
2&16
3&16
4&16
5&16
6&16
7&16
8&16
9&16
10&16
11&16
12&16
13&16
14&16
15&16
del 0-16

q
EOF
    done                                                                                           # FOR LOOP lo systems ends


done                                                                                               # FOR LOOP leaflets ends

rm -rf system_*/\#*                                                                             # clean up



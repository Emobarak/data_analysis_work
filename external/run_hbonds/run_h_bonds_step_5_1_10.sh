#!/bin/bash
rm -rf hbonds_5.log                                                             # remove old .log file
touch hbonds_5.log                                                              # create a new .log file

# 1 = lipid (DOPC or SMPC)
# 2 = dye (dye attached to lipid)
# 5 = water
# 10 = cholesterol (for systems 3, 4, 7, 8, 10)

# for system 1, 2, 3, 4, 9, 10
#      532_tail 532_head    kk114
# 11 = O9       O9          N3
# 12 = NH1      NH1         O14
# 13 = SOOO1    SOOO1       N22
# 14 = SOOO2    SOOO2       SOOO1
# 15 = NH2      NH2         SOOO2
# 16 = CON      CON         CON
# 17 = OOOPO    OOOPO       OPOOO
# 18 = CHOH     CHOH        CHOH
# 19 = NHCO     CH3CH3CH3N  NHCO
# 20 = Group 1 (dye 1)
# 21 = Group 2 (dye 2)
# 22 = Group 3 (dye 3 + sm/cpe)
# 23 = Group 4 (sm/cpe)
# 24 = CPE/SM no dye

# for systems 5, 6, 7, 8
#      647_tail 647_head
# 11 = N1       N5
# 12 = N2       N16
# 13 = CON      CON
# 14 = OOOPO    OOOPO
# 15 = NHCO     CH3CH3CH3N
# 16 = CHOH     CHOH
# 17 = Group 1 (dye 1)
# 18 = Group 2 (dye 2)
# 19 = Group 3 (dye 3 + sm/cpe)
# 20 = Group 4 (sm/cpe)
# 24 = CPE/SM no dye

counter=0                                                                       # counter variable
atto532_kk_array=( 1 2 3 4 9 10 )                                               # array for systems 1 2 3 4 9 10
atto532_kk_group_array_1=( 11 12 13 14 15 16 17 18 19 20 21 22 23 )             # arrays for the groups in systems 1 2 3 4 9 10
atto532_kk_group_array_2=( 11 12 13 14 15 16 17 18 19 20 21 22 23 1 5 )

atto614_array=( 5 6 7 8 )                                                       # arrays for systems 5 6 7 8
atto614_group_array_1=( 11 12 13 14 15 16 17 18 19 20 )                         # arrays for the groups in systems 1 2 3 4 9 10
atto614_group_array_2=( 11 12 13 14 15 16 17 18 19 20 1 5 )

# in order to compare with the empty membranes
ld_array=( 1 2 5 6 9 )                                                          # arrays for the systems with an ld membrane (no chol)
ld_group_array_1=( 24 1 5 )
ld_group_array_2=( 24 1 5 )
lo_array=( 3 4 7 8 10 )                                                         # arrays for the systems with an lo membrane (chol)
lo_group_array_1=( 24 1 5 10 )
lo_group_array_2=( 24 1 5 10 )

for i in "${atto532_kk_array[@]}"; do                                           # for all atto532 and kk systems
    for j in "${atto532_kk_group_array_1[@]}"; do                               # array 1 loop

        for k in "${atto532_kk_group_array_2[@]}"; do                           # array 2 loop
            if [ "$j" -ge "$k" ]; then                                          # condition to avoid repeats
                echo "system = $i" >> hbonds_5.log
                echo $j $k | gmx hbond -f system_$i/system_"$i"_traj.xtc -s system_$i/system_"$i"_md_4_1.tpr -n system_$i/system_"$i"_hbonds_final.ndx -num ./step_5_hbonds/$i-$j-$k-hbonds.xvg -b 200000 >> hbonds_5.log
                let "counter += 1"                                              # increments counter
                echo "counter = $counter " >> hbonds_5.log
                echo $'\n' >> hbonds_5.log
            fi
        done

    if [ $i == 3 ] || [ $i == 4 ] || [ $i == 10 ]; then                         # if the system has cholesterol (missing water/chol and lipid/chol)
        echo "system = $i" >> hbonds_5.log
        echo $j 10 | gmx hbond -f system_$i/system_"$i"_traj.xtc -s system_$i/system_"$i"_md_4_1.tpr -n system_$i/system_"$i"_hbonds_final.ndx -num ./step_5_hbonds/$i-$j-chol-hbonds.xvg -b 200000 >> hbonds_5.log
        let "counter += 1"                                                      # increments counter
        echo "counter = $counter " >> hbonds_5.log
        echo $'\n' >> hbonds_5.log
    fi

    done
done

for i in "${atto614_array[@]}"; do                                              # for all atto614 systems
    for j in "${atto614_group_array_1[@]}"; do

        for k in "${atto614_group_array_2[@]}"; do
            if [ "$j" -ge "$k" ]; then                                          # condition to avoid repeats
                echo "system = $i" >> hbonds_5.log
                echo $j $k | gmx hbond -f system_$i/system_"$i"_traj.xtc -s system_$i/system_"$i"_md_4_1.tpr -n system_$i/system_"$i"_hbonds_final.ndx -num ./step_5_hbonds/$i-$j-$k-hbonds.xvg -b 200000 >> hbonds_5.log
                let "counter += 1"                                              # increments counter
                echo "counter = $counter " >> hbonds_5.log
                echo $'\n' >> hbonds_5.log
            fi
        done

    if [ $i == 7 ] || [ $i == 8 ]; then                                         # for systems with cholesterol
        echo "system = $i" >> hbonds_5.log
        echo $j 10 | gmx hbond -f system_$i/system_"$i"_traj.xtc -s system_$i/system_"$i"_md_4_1.tpr -n system_$i/system_"$i"_hbonds_final.ndx -num ./step_5_hbonds/$i-$j-chol-hbonds.xvg -b 200000 >> hbonds_5.log
        let "counter += 1"                                                      # increments counter
        echo "counter = $counter " >> hbonds_5.log
        echo $'\n' >> hbonds_5.log
    fi

    done
done

for i in "${ld_array[@]}"; do                                                   # for all ld systems
    for j in "${ld_group_array_1[@]}"; do                                       # for missing group / combinations involving only the membrane
        for k in "${ld_group_array_2[@]}"; do
            if [ "$j" -ge "$k" ]; then
                echo "system = $i" >> hbonds_5.log
                echo $j $k | gmx hbond -f system_$i/system_"$i"_traj.xtc -s system_$i/system_"$i"_md_4_1.tpr -n system_$i/system_"$i"_hbonds_final.ndx -num ./step_5_hbonds/$i-$j-$k-hbonds.xvg -b 200000 >> hbonds_5.log
                let "counter += 1"                                              # increments counter
                echo "counter = $counter " >> hbonds_5.log
                echo $'\n' >> hbonds_5.log
            fi
        done
    done
done

for i in "${lo_array[@]}"; do                                                   # for all lo systems
    for j in "${lo_group_array_1[@]}"; do                                       # for missing group / combinations involving only the membrane
        for k in "${lo_group_array_2[@]}"; do
            if [ "$j" -ge "$k" ]; then
                echo "system = $i" >> hbonds_5.log
                echo $j $k | gmx hbond -f system_$i/system_"$i"_traj.xtc -s system_$i/system_"$i"_md_4_1.tpr -n system_$i/system_"$i"_hbonds_final.ndx -num ./step_5_hbonds/$i-$j-$k-hbonds.xvg -b 200000 >> hbonds_5.log
                let "counter += 1"                                              # increments counter
                echo "counter = $counter " >> hbonds_5.log
                echo $'\n' >> hbonds_5.log
            fi
        done
    done
done

rm -rf step_5_hbonds/\#*hbonds.xvg.*
# clean up
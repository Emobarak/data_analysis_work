#!/bin/bash -l
rm -rf hbonds.log                                                               # remove old .log file
touch hbonds.log                                                                # create a new .log file

counter = 0

echo SM_pure >> hbonds.log
echo 1 1 | gmx hbond -f SM_membrane/equilibration_100ns.xtc -s SM_membrane/equilibration_100ns.tpr -n SM_membrane/index.ndx -num SM_membrane/sm_sm_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# SM / SM - chol
echo CPE_pure >> hbonds.log
echo 1 1 | gmx hbond -f CPE_membrane/equilibration_100ns.xtc -s CPE_membrane/equilibration_100ns.tpr -n CPE_membrane/index.ndx -num CPE_membrane/cpe_cpe_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# CPE / CPE - chol
echo SM_chol >> hbonds.log
echo 1 1 | gmx hbond -f SM_chol_membrane/equilibration_100ns.xtc -s SM_chol_membrane/equilibration_100ns.tpr -n SM_chol_membrane/index.ndx -num SM_chol_membrane/sm_sm_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# SM / SM + chol
echo CPE_chol >> hbonds.log
echo 2 2 | gmx hbond -f CPE_chol_membrane/equilibration_100ns.xtc -s CPE_chol_membrane/equilibration_100ns.tpr -n CPE_chol_membrane/index.ndx -num CPE_chol_membrane/cpe_cpe_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# CPE / CPE + chol

echo SM_chol >> hbonds.log
echo 1 2 | gmx hbond -f SM_chol_membrane/equilibration_100ns.xtc -s SM_chol_membrane/equilibration_100ns.tpr -n SM_chol_membrane/index.ndx -num SM_chol_membrane/sm_chol_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# SM / Chol
echo CPE_chol >> hbonds.log
echo 2 1 | gmx hbond -f CPE_chol_membrane/equilibration_100ns.xtc -s CPE_chol_membrane/equilibration_100ns.tpr -n CPE_chol_membrane/index.ndx -num CPE_chol_membrane/cpe_chol_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# CPE / Chol

echo SM_pure >> hbonds.log
echo 1 4 | gmx hbond -f SM_membrane/equilibration_100ns.xtc -s SM_membrane/equilibration_100ns.tpr -n SM_membrane/index.ndx -num SM_membrane/sm_wat_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# SM / wat - chol
echo CPE_pure >> hbonds.log
echo 1 4 | gmx hbond -f CPE_membrane/equilibration_100ns.xtc -s CPE_membrane/equilibration_100ns.tpr -n CPE_membrane/index.ndx -num CPE_membrane/cpe_wat_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# CPE / wat - chol
echo SM_chol >> hbonds.log
echo 1 5 | gmx hbond -f SM_chol_membrane/equilibration_100ns.xtc -s SM_chol_membrane/equilibration_100ns.tpr -n SM_chol_membrane/index.ndx -num SM_chol_membrane/sm_wat_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# SM / wat + chol
echo CPE_chol >> hbonds.log
echo 2 5 | gmx hbond -f CPE_chol_membrane/equilibration_100ns.xtc -s CPE_chol_membrane/equilibration_100ns.tpr -n CPE_chol_membrane/index.ndx -num CPE_chol_membrane/cpe_wat_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# CPE / wat + chol

echo SM_chol >> hbonds.log
echo 2 5 | gmx hbond -f SM_chol_membrane/equilibration_100ns.xtc -s SM_chol_membrane/equilibration_100ns.tpr -n SM_chol_membrane/index.ndx -num SM_chol_membrane/chol_wat_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# chol / wat
echo CPE_chol >> hbonds.log
echo 1 5 | gmx hbond -f CPE_chol_membrane/equilibration_100ns.xtc -s CPE_chol_membrane/equilibration_100ns.tpr -n CPE_chol_membrane/index.ndx -num CPE_chol_membrane/chol_wat_hbond.xvg -b 50000 >> hbonds.log
let "counter += 1"                                                          # increments counter
echo "counter = $counter " >> hbonds.log
echo $'\n' >> hbonds.log
# chol / wat

rm -rf SM_chol_membrane/\#*.xvg.*
rm -rf CPE_chol_membrane/\#*.xvg.*
rm -rf SM_membrane/\#*.xvg.*
rm -rf CPE_membrane/\#*.xvg.*
#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\n\t Help Menu:'
    print '\t ##########'
    print '\t This program takes will parse every contact.xvg file present in the directory'
    print '\t and will average the number of contacts over the length of the trajectory.'
    print '\t If that value is below 0.02, the corresponding .ps file will be deleted.'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


################################# Reading files ################################

print '\n\t Starting program...'                                                # verbose

filout = open( 'average_contacts.log', 'w' )                                    # preparing log file
file_number = len([ name for name in os.listdir('.') if os.path.isfile(name) ]) # counts the number of file in the current directory
line_array = []                                                                 # initialising the file array

print '\n\t Parsing files...'                                                   # verbose

for i in range( 0, file_number ) :                                              # for every file in the directory

    if os.path.isfile( 'residue_list_2.txt.ndx_'+str(i)+'.xvg' ) :              # if the .xvg files exist

        contacts = 0                                                            # set the contacts variable to 0
        average = 0                                                             # set the average variable to 0
        average = float(average)                                                # converts the variable to a float value

        filin = open( 'residue_list_2.txt.ndx_'+str(i)+'.xvg' )                 # open the .xvg file
        lines = filin.readlines()                                               # save every line of the file
        filin.close()                                                           # close the file

        for j in range( 19, len( lines ) ) :                                    # for every line except the header
            split_line = lines[j].split()                                       # split it
            contacts = float(contacts) + float(split_line[1])                   # sum up the total number of contacts

        average = contacts / ( len( lines ) - 18 )                              # devide it by the total number of frames to calculate the average

        if average < 0.02 :                                                     # if the average contact is lower than 0.02
            if os.path.isfile( 'residue_list_2.txt.ndx_'+str(i)+'.ps' ) :       # if the corresponding .ps file still exists
                os.remove('residue_list_2.txt.ndx_'+str(i)+'.ps')               # delete the file
                print '\t Deleting file...'                                     # verbose
        else :                                                                  # or else
            filout.write( 'Residue ' + str(i) + ' average contacts = ' + str(average) + '\n' )
                                                                                # print out the average contact in a log file

filout.close()                                                                  # closing log file

print '\n\t Assembling .pdf file...'                                            # verbose
os.system("montage -tile 2x3 -geometry 1000  -rotate 90 residue_list_*.txt.ndx_*.ps  contacts_over_time.pdf")
                                                                                # assemble the .pdf file
print '\n\t Done!'                                                              # verbose

################################ Ending program ################################

print '\n\t Log file successfully written!'
print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'

################################################################################
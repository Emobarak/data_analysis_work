#!/bin/bash
rm -rf ${BASH_ARGV[0]}.log                                                      # remove old .log file
touch ${BASH_ARGV[0]}.log                                                       # create a new .log file

# Takes as argument the residue list .txt file

# for system 2
#   0 System              : 633782 atoms
#   1 Protein             : 30312 atoms
#   2 Protein-H           : 15240 atoms
#   3 C-alpha             :  1888 atoms
#   4 Backbone            :  5664 atoms
#   5 MainChain           :  7556 atoms
#   6 MainChain+Cb        :  9370 atoms
#   7 MainChain+H         :  9396 atoms
#   8 SideChain           : 20916 atoms
#   9 SideChain-H         :  7684 atoms
#  10 Prot-Masses         : 30312 atoms
#  11 non-Protein         : 603470 atoms
#  12 Other               : 148420 atoms
#  13 4YB                 :  1080 atoms
#  14 VMB                 :   400 atoms
#  15 0MA                 :   880 atoms
#  16 POPC                : 40870 atoms
#  17 SMPC                : 22860 atoms
#  18 CHO                 : 40330 atoms
#  19 GLUC                :  9240 atoms
#  20 POPS                : 16510 atoms
#  21 PE                  : 16250 atoms
#  22 NA                  :   567 atoms
#  23 CL                  :   409 atoms
#  24 Water               : 454074 atoms
#  25 SOL                 : 454074 atoms
#  26 non-Water           : 179708 atoms
#  27 Ion                 :   976 atoms
#  28 Water_and_ions      : 455050 atoms
#  29 Sugars              :  2360 atoms
#  30 Membrane            : 146060 atoms
#  31 Monomer_1           : 13011 atoms
#  32 Monomer_2           : 13011 atoms
#  33 MD2_1               :  2145 atoms
#  34 MD2_2               :  2145 atoms
#  35 Monomer_MD2_1       : 15156 atoms
#  36 Monomer_MD2_2       : 15156 atoms
#  37 TLR4                : 32672 atoms

group_nr=38                                                                     # variable defining group numbers

echo q | make_ndx -f system_4_equilibrated_fixed.gro -n system_4.ndx -o ${BASH_ARGV[0]}.ndx
                                                                                # prepares new index file from the pre-prepared index file

while IFS='=' read -r col1 col2 || [[ -n "$col1"  ]]; do                        # reads a file inputed in argument and creates the index file
    make_ndx -f system_4_equilibrated_fixed.gro -n ${BASH_ARGV[0]}.ndx -o ${BASH_ARGV[0]}.ndx <<EOF
    1 & r $col1
    name $group_nr $col1$col2
    q
EOF
    group_nr=$((group_nr+1))                                                    # incrememting the group number
    rm -rf \#${BASH_ARGV[0]}.ndx*                                               # clean up backup files
done < "$1" >> ${BASH_ARGV[0]}.log                                              # outputing everything in a log file
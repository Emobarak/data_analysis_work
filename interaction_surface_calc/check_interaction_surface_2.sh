#!/bin/bash
rm -rf ${BASH_ARGV[0]}.log                                                      # remove old .log file
touch ${BASH_ARGV[0]}.log                                                       # create a new .log file

# takes as argument the .xtc trajectory, then .tpr file and then index outputed by the mk_res_ndx.sh bash file

# for system 2
#   0 System              : 633782 atoms
#   1 Protein             : 30312 atoms
#   2 Protein-H           : 15240 atoms
#   3 C-alpha             :  1888 atoms
#   4 Backbone            :  5664 atoms
#   5 MainChain           :  7556 atoms
#   6 MainChain+Cb        :  9370 atoms
#   7 MainChain+H         :  9396 atoms
#   8 SideChain           : 20916 atoms
#   9 SideChain-H         :  7684 atoms
#  10 Prot-Masses         : 30312 atoms
#  11 non-Protein         : 603470 atoms
#  12 Other               : 148420 atoms
#  13 4YB                 :  1080 atoms
#  14 VMB                 :   400 atoms
#  15 0MA                 :   880 atoms
#  16 POPC                : 40870 atoms
#  17 SMPC                : 22860 atoms
#  18 CHO                 : 40330 atoms
#  19 GLUC                :  9240 atoms
#  20 POPS                : 16510 atoms
#  21 PE                  : 16250 atoms
#  22 NA                  :   567 atoms
#  23 CL                  :   409 atoms
#  24 Water               : 454074 atoms
#  25 SOL                 : 454074 atoms
#  26 non-Water           : 179708 atoms
#  27 Ion                 :   976 atoms
#  28 Water_and_ions      : 455050 atoms
#  29 Sugars              :  2360 atoms
#  30 Membrane            : 146060 atoms
#  31 Monomer_1           : 13011 atoms
#  32 Monomer_2           : 13011 atoms
#  33 MD2_1               :  2145 atoms
#  34 MD2_2               :  2145 atoms
#  35 Monomer_MD2_1       : 15156 atoms
#  36 Monomer_MD2_2       : 15156 atoms
#  37 TLR4                : 32672 atoms
#  38 residue_1
# 846 residue_808


#residue_array=( 1 2 3 4 9 10 )                                                 # potential group array for systems 1 2 3 4 9 10

# input 1 = ${BASH_ARGV[2]} = .xtc
# input 2 = ${BASH_ARGV[1]} = .tpr
# input 3 = ${BASH_ARGV[0]} = .ndx (outputed from mk_res_ndx.sh)
# output = 1 .xvg file for every residue index group


for i in {38..846}; do                                                          # for all residues, start the g_mindist function
    g_mindist -f ${BASH_ARGV[2]} -s ${BASH_ARGV[1]} -n ${BASH_ARGV[0]} -od ./pic2/mindist.xvg -on ./pic2/${BASH_ARGV[0]}_$i.xvg -group <<EOF
    35
    $i
EOF
    rm pic2/\#mindist*                                                               # clean up
    #rm mindist*

    gracebat ./pic2/${BASH_ARGV[0]}_$i.xvg -print ./pic2/${BASH_ARGV[0]}_$i.ps
done >> ${BASH_ARGV[0]}.log                                                     # make the log



# THINK ABOUT REMOVING THE CONTACT REPEATS = -group flag
# REMOVE THE PLOTS WITH NO VALUES
# CALCULATE THE AVERAGE OVER THE WHOLE TRAJECTORY (ADD UP CONTACTS + DIVIDE THEM BY THE TRAJECTORY NUMBER OF FRAMES)
#   This value would be 0 or something
#   Make a plot on VMD using the beta value (look it up in .pdb), which would be the average number
#   Make a .pdb file of the last snapshot of the trajectory, and replace beta value column with the average number of contacts (0 or not 0)
#   In VMD, we could colour the protein in a way that we will see the points coloured where beta value is not zero, hence there is interaction
#   OR EVEN BETTER = do it for the whole trajectory = no average, just 0 or not zero
#   OR EVEN BETTERERER = do a colour scale with VMD
# NORMALISE THEM?
# evince file.pdf to view it
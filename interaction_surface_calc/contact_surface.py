#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\n\t Help Menu:'
    print '\t ##########'
    print '\t This program takes will take a list.txt file filled with residues'
    print '\t forming a surface, will parse every contact.xvg file present in'
    print '\t the inputed list, and will sum up the number of contacts for each'
    print '\t time step, for the whole simulation.'
    print '\t The output is one contact.xvg file, with the number of contacts'
    print '\t over time for the whole surface, during the whole simulation.'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


############################### Starting program ###############################

print '\n\t ************************************************************************'
print '\t ************************** contact_surface.py **************************'
print '\t ************************************************************************'

################################################################################


################################# Reading files ################################

if len( sys.argv ) > 1 :                                                        # if at least one file has been entered as argument

    arg_array = []                                                              # create an array that stores the arguments

    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()                                                          # start the help section

    if len( sys.argv ) > 2 :                                                    # if more than one argument has been entered
        print '\n ERROR(2): More than one argument has been entered, please only enter one residue_list .txt file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if not sys.argv[1].endswith('.txt') :                                       # checks if first argument entered is a .txt file
        print '\n ERROR(3): Argument ' + sys.argv[i] + ' is not a .txt file, please enter a text .txt file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a txt file

else :
    print '\n ERROR(1): No argument entered, please enter a text .txt file.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\n\t Starting program...'                                                # verbose

try :                                                                           # error handeling
    filin = open( sys.argv[1] )
except IOError :
    print '\n ERROR(4): Unable to open "' + sys.argv[1] + '" file, please check file name or file location.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exits if file is missing or the name is entered wrong
else :
    print '\n\t Reading file "' + sys.argv[i] + '"...'                          # verbose
    lines = filin.readlines()                                                   # saving every line of every file in the file array
    filin.close()                                                               # close the file

    r_index_array = []                                                          # create an array that stores all residue indexes in the files

    for i in range( 1, len( lines ) ) :                                         # for every line except the header
        if lines[i] == '\n' :                                                   # if the line is empty
            print '\n ERROR(5): Line ' + str(i+1) + ' in file ' + sys.argv[1] + 'is empty, please remove it.\n'
            sys.exit(1)                                                         # error and exit
        r_index = str( int(re.sub('[^0-9]','', lines[i])) + 11 )                # remove all extra charactere and correct the index
        r_index_array.append( r_index )                                         # save it in the array

    print '\t\t file "' + sys.argv[1] + '" read successfully.'                  # verbose

################################################################################

print '\n\t Parsing contact .xvg files...'                                      # verbose

filin = open( 'residue_list_1.txt.ndx_38.xvg' )                                 # open one .xvg file
lines = filin.readlines()                                                       # save every line of the file
filin.close()

contacts_sum_array = [0] * ( len( lines )-19 )                                  # prepare a sum_contact array of 1856 cases
timestep_array = [0] * ( len( lines )-19 )                                      # prepare a timestamp array

for i in range( 0, len(r_index_array) ) :                                       # for every residue in the list
    filin = open( 'residue_list_1.txt.ndx_' + r_index_array[i] + '.xvg' )       # open the proper .xvg file
    print '\t\t residue_list_1.txt.ndx_' + r_index_array[i] + '.xvg'            # verbose
    lines = filin.readlines()                                                   # save every line of the file
    filin.close()                                                               # close the file

    for j in range( 19, len( lines ) ) :                                        # for every line except the header
        split_line = lines[j].split()                                           # split it
        contacts_sum_array[j-19] = int(contacts_sum_array[j-19]) + int(split_line[1]) # sum up the total number of contacts
        timestep_array[j-19] = split_line[0]                                    # save the appropriate timestep

print '\t successfully summed up residue contacts on all files.'                # verbose

print '\n\t Writing output file...'                                             # verbose

out_filename = os.path.splitext(sys.argv[1])[0]                                 # strip the .txt extension from the filename
filout = open( out_filename + '_sum.xvg', 'w' )                                 # preparing output file

filout.write('@    title "Sum of all Contacts < 0.6 nm"\n')                     # write the header
filout.write('@    xaxis  label "Time (ps)"\n')
filout.write('@    yaxis  label "Total number"\n')
filout.write('@TYPE xy\n')
filout.write('@ view 0.15, 0.15, 0.75, 0.85\n')
filout.write('@ legend on\n')
filout.write('@ legend box on\n')
filout.write('@ legend loctype view\n')
filout.write('@ legend 0.78, 0.8\n')
filout.write('@ legend length 2\n')
filout.write('@ s0 legend "' + out_filename + '"\n')

for j in range( 19, len( lines ) ) :                                            # write the timestamp and summed contacts line by line
    filout.write( timestep_array[j-19] + '        ' + str(contacts_sum_array[j-19]) + '\n' )

filout.close()                                                                  # closing output file

print '\t\t Output file successfully written.'                                  # verbose
print '\n\t Done!'                                                              # verbose

################################ Ending program ################################

print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'

################################################################################
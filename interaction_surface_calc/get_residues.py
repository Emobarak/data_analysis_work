#!/usr/bin/env python
import sys                                                                      # system library
import re                                                                       # regular expressions library
import os                                                                       # bash commands library

# TLR4 Monomer 1 = 27, 835
# TLR4 Monomer 2 = 836, 1644

############################## Defining functions ##############################

def help_fun () :                                                               # starts help section
    print '\n\t Help Menu:'
    print '\t ##########'
    print '\t This program takes an argument a .gro file. It then takes ranges of'
    print '\t residues inputed by the user and outputs them into an easy to read .txt'
    print '\t file that will be used by other programs'
    print '\n\t ************************************************************************'
    print '\t ******************* Thank you for using this program! ******************'
    print '\t ************************************************************************\n'
    sys.exit(1)                                                                 # program exits

################################################################################


################################# Reading files ################################

if len( sys.argv ) > 1 :                                                        # if at least one file has been entered as argument

    arg_array = []                                                              # create an array that stores the arguments

    for i in range( 1 ,len( sys.argv ) ) :                                      # for every argument entered
        if sys.argv[i] == '-h' :                                                # if the '-h' flag is used
            help_fun()                                                          # start the help section

    if len( sys.argv ) > 2 :                                                    # if more than one argument has been entered
        print '\n ERROR(2): More than one argument has been entered, please only enter one coordinate .gro file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits

    if not sys.argv[1].endswith('.gro') :                                       # checks if first argument entered is a .txt file
        print '\n ERROR(3): Argument ' + sys.argv[i] + ' is not a .gro file, please enter a coordinate .gro file.'
        print ' Use the "-h" flag to get more help.\n'
        sys.exit(1)                                                             # program exits if the first file is not a txt file

else :
    print '\n ERROR(1): No argument entered, please enter a coordinate .gro file.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exit if no file name entered

print '\n\t Starting program...'

try :                                                                           # error handeling
    filin = open( sys.argv[1] )
except IOError :
    print '\n ERROR(4): Unable to open "' + sys.argv[1] + '" file, please check file name or file location.'
    print ' Use the "-h" flag to get more help.\n'
    sys.exit(1)                                                                 # program exits if file is missing or the name is entered wrong
else :
    print '\t Reading file "' + sys.argv[i] + '"...'
    lines = filin.readlines()                                                   # saving every line of every file in the file array
    filin.close()                                                               # close the file

    line_array = []                                                             # create an array that stores all lines of the files

    for i in range( 0, len( lines ) ) :                                         # for every line
        split_line = lines[i].split()                                           # split it
        line_array.append( split_line )                                         # save it in the array

    print '\t\t file "' + sys.argv[1] + '" read successfully.'

################################################################################


############################ Preparing output file2 ############################

fragments = input ('\n\t Please input the number of fragments in the .gro file: ')
print('\t\t Processing ' + str( fragments ) + ' fragment(s)!')                  # takes the number of fragments as user input

if fragments < 1 :                                                              # error if the number 0 is inputed
    print '\n ERROR(5): Number of fragments can not be 0. Please input 1 or more.'
    sys.exit(1)


for i in range ( 0, fragments ) :                                               # for every fragment, getting residue ranges from input
    range_1 = input ('\n\t Please input the first residue index for fragment ' + str(i+1) + ': ')
    range_2 = input ('\t Please input the last residue index for fragment ' + str(i+1) + ': ')

    filout = open( 'residue_list_'+str(i+1)+'.txt', 'w' )                       # preparing output files for each fragment
    print '\n\t Preparing output residue name file "residue_list_'+str(i+1)+'.txt"...'

    residue_array = []                                                          # create an array that stores all the residues of the file

    for j in range( 0, 99999 ) :                                                # for the whole file
        r_index = re.sub('[^0-9]','', line_array[j][0])                         # take the current residue index

        if ( int(r_index) >= int(range_1) ) :                                   # if it's included in the range chosen
            if ( int(r_index) <= int(range_2) ) :
                if line_array[j][0] not in residue_array :                      # if a new residue name appears
                    residue_array.append( line_array[j][0] )                    # save it in the array

    for j in range ( 0, len( residue_array ) ) :                                # outputs residue numbers in the residue txt file
        filout.write( re.sub( "[^0-9]", "", residue_array[j] ) + '=' + residue_array[j].lstrip('0123456789 ') + '\n' )

    print '\n\t Finished outputing file "residue_list_'+str(i+1)+'.txt"!'

    filout.close()                                                              # closes file

#################################################################################


################################ Ending program ################################

print '\n\t All files successfully written!'
print '\n\t ************************************************************************'
print '\t ******************* Thank you for using this program! ******************'
print '\t ************************************************************************\n'

################################################################################
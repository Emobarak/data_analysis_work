#!/usr/bin/env python
import sys                                                                      # system library

current_section = 'header'                                                      # variable that sets the current section

# open file #####################################################################
if len( sys.argv ) < 2 :                                                        # if no file has been entered
    print '\n\t ERROR(1): please enter one molecule_list .txt file as argument.\n'
    sys.exit(1)                                                                 # program exit

try :                                                                           # error handeling
    filin = open( sys.argv[1] )                                                 # try to open the file
except IOError :
    print '\n\t ERROR(2): Unable to open "' + sys.argv[1] + '" file, please check file name or file location.\n'
    sys.exit(1)                                                                 # program exits if file is missing or the name is entered wrong
else :                                                                          # if it's not missing
    print '\n\t Reading file "' + sys.argv[1] + '"...'                          # verbose
    lines = filin.readlines()                                                   # saving every line of every file in the file array
    filin.close()                                                               # close the file
    print '\t\t file "' + sys.argv[1] + '" read successfully.'                  # verbose


# read the .txt file ############################################################
filout = open( 'aminoacids_converted.rtp', 'w' )                                # open output file

print '\n\t Writing file "aminoacids_converted.rtp"...'                         # verbose

for i in range ( 0, 26 ) :                                                      # for every header line
    filout.write(lines[i])                                                      # write it as is

for i in range( 26, len(lines) ) :                                              # for every line read from the file

    split_line = lines[i].split()                                               # split every line

    if( lines[i] == '\n' ) :                                                    # if the line is empty
        filout.write(lines[i])                                                  # write it

    elif( lines[i][0] == '[' ) :                                                # if the line is a header
        current_section = split_line[1]                                         # set what the current section is
        filout.write(lines[i])                                                  # write it as is

    elif( split_line[0] == ';' ) :                                              # if the line is commented
        if len(lines[i]) > 2 and not 'converted' in lines[i] :                  # if the line is long and not a converted file
            filout.write(lines[i])                                              # write it as is

        elif len(lines[i]) > 2 and 'converted' in lines[i] :                    # if the line is long and not a converted file
            if current_section == 'atoms' :                                     # if we are in the atoms section
                filout.write('%6s' % split_line[1])                             # write atom 1
                filout.write('%12s' % split_line[2])                            #       atom type
                filout.write('%9s' % split_line[3])                             #       partial charge
                filout.write('%6s' % split_line[4] + '\n')                      #       charge group

            if current_section == 'bonds' :                                     # if we are in the bonds section
                filout.write('%6s' % split_line[1])                             # write atom 1
                filout.write('%6s' % split_line[2])                             #       atom 2
                filout.write('%6s' % 1)                                         #       function
                filout.write('%12s' % split_line[3])                            #       value 1
                filout.write('%10s' % split_line[4] + '\n')                     #       value 2

            if current_section == 'angles' :                                    # if we are in the angles section
                filout.write('%6s' % split_line[1])                             # write atom 1
                filout.write('%6s' % split_line[2])                             #       atom 2
                filout.write('%6s' % split_line[3])                             #       atom 3
                filout.write('%6s' % 1)                                         #       function
                filout.write('%12s' % split_line[4])                            #       value 1
                filout.write('%12s' % split_line[5] + '\n')                     #       value 2

            if current_section == 'dihedrals' :                                 # if we are in the dihedrals section
                filout.write('%6s' % split_line[1])                             # write atom 1
                filout.write('%6s' % split_line[2])                             #       atom 2
                filout.write('%6s' % split_line[3])                             #       atom 3
                filout.write('%6s' % split_line[4])                             #       atom 4
                filout.write('%6s' % 5)                                         #       function
                filout.write('%12s' % split_line[5])                            #       value 1
                filout.write('%12s' % split_line[6])                            #       value 2
                filout.write('%12s' % split_line[7])                            #       value 3
                filout.write('%12s' % split_line[8] + '\n')                     #       value 4

            if current_section == 'impropers' :                                 # if we are in the impropers section
                filout.write('%6s' % split_line[1])                             # write atom 1
                filout.write('%6s' % split_line[2])                             #       atom 2
                filout.write('%6s' % split_line[3])                             #       atom 3
                filout.write('%6s' % split_line[4])                             #       atom 4
                filout.write('%10s' % split_line[5] + '\n')                     #       value 1

        elif len(lines[i]) == 2 :                                               # if it's just a semi-colon
            filout.write(lines[i])                                              # write it as is

    else :                                                                      # if the line is a normal line
        if current_section == 'atoms' :                                         # if we are in the atoms section
            # do not touch
            filout.write('%6s' % split_line[0])                                 # write atom 1
            filout.write('%12s' % split_line[1])                                #       atom type
            filout.write('%9s' % split_line[2])                                 #       partial charge
            filout.write('%6s' % split_line[3] + '\n')                          #       function

        if current_section == 'bonds' :                                         # if we are in the bonds section
            fixed_line_1 = float(split_line[3]) / 10                            # fix column 4
            fixed_line_2 = float(split_line[2]) * 418.4                         # fix colum  5
            filout.write('%6s' % split_line[0])                                 # write atom 1
            filout.write('%6s' % split_line[1])                                 #       atom 2
            filout.write('%6s' % 1)                                             #       function
            filout.write('%12s' % str(fixed_line_1))                            #       converted value 1
            filout.write('%10s' % str(fixed_line_2) + '\n')                     #       converted value 2

        if current_section == 'angles' :                                        # if we are in the angles section
            fixed_line_1 = float(split_line[4])                                 # fix column 4
            fixed_line_2 = float(split_line[3]) * 4.184                         # fix colum  5
            filout.write('%6s' % split_line[0])                                 # write atom 1
            filout.write('%6s' % split_line[1])                                 #       atom 2
            filout.write('%6s' % split_line[2])                                 #       atom 3
            filout.write('%6s' % 1)                                             #       function
            filout.write('%12s' % str(fixed_line_1))                            #       converted value 1
            filout.write('%12s' % str(fixed_line_2) + '\n')                     #       converted value 2

        if current_section == 'dihedrals' :                                     # if we are in the dihedrals section
            fixed_line_1 = float(split_line[4]) * 4.184                         # fix column 6
            fixed_line_2 = float(split_line[5]) * 4.184                         # fix column 7
            fixed_line_3 = float(split_line[6]) * 4.184                         # fix column 8
            fixed_line_4 = float(split_line[7]) * 4.184                         # fix column 9
            filout.write('%6s' % split_line[0])                                 # write atom 1
            filout.write('%6s' % split_line[1])                                 #       atom 2
            filout.write('%6s' % split_line[2])                                 #       atom 3
            filout.write('%6s' % split_line[3])                                 #       atom 4
            filout.write('%6s' % 5)                                             #       function
            filout.write('%12s' % str(fixed_line_1))                            #       converted value 1
            filout.write('%12s' % str(fixed_line_2))                            #       converted value 2
            filout.write('%12s' % str(fixed_line_3))                            #       converted value 3
            filout.write('%12s' % str(fixed_line_4) + '\n')                     #       converted value 4

        if current_section == 'impropers' :                                     # if we are in the impropers section
            fixed_line = float(split_line[4]) * 4.184                           # fix column 7
            filout.write('%6s' % split_line[0])                                 # write atom 1
            filout.write('%6s' % split_line[1])                                 #       atom 2
            filout.write('%6s' % split_line[2])                                 #       atom 3
            filout.write('%6s' % split_line[3])                                 #       atom 4
            filout.write('%6s' % 2)                                             #       function
            filout.write('%12s' % 180.0)                                        #       angle
            filout.write('%12s' % str(fixed_line) + '\n')                       #       converted value 1

filout.close()                                                                  # close the file
print '\t\t "aminoacids_converted.rtp" written successfully!\n'                 # verbose

################################################################################